package com.aptech.chitieu;
import java.text.SimpleDateFormat;
import java.util.*;
import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import com.aptech.chitieu.adapter.CtChitieuAdapter;
import com.aptech.chitieu.adapter.CtDanhMucAdapter;
import com.aptech.chitieu.dao.ConnectionUtil;
import com.aptech.chitieu.dao.CtChiTieuDataSource;
import com.aptech.chitieu.dao.CtDanhMucDataSource;
import com.aptech.chitieu.dao.DatabaseHelperChiTieu;
import com.aptech.chitieu.entity.CtChiTieu;
import com.aptech.chitieu.entity.CtDanhMuc;
import com.aptech.chitieu.entity.DanhMuc;
import com.aptech.chitieu.util.DateUtils;
import com.aptech.chitieu2.dao.DanhMucDao;
import android.R.layout;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View.OnClickListener;
import android.webkit.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.*;
import static com.aptech.chitieu.util.DigestUtil.*;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
/**
 * Title : Main Class<br/>
 * Description : The fisrt run program here<br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Jul 16, 2015 10:26:32 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Jul 16, 2015 10:26:32 PM
 */
public class MainActivity extends Activity {
	// public static List<CtChiTieu> listThuChi =new ArrayList<CtChiTieu>();
	private float	                             tongThuChi	        =0f;
	private float	                             tongThu	          =0f;
	private float	                             tongChi	          =0f;
	// Control
	private ListView	                         ct_lv_thuChi	      =null;
	private RadioButton	                       ct_rd_thu	        =null;
	private RadioButton	                       ct_rd_chi	        =null;
	private TextView	                         ct_txt_tenChiTieu	=null;
	private TextView	                         ct_txt_tienChiTieu	=null;
	private EditText	                         ct_txt_ngayTao	    =null;
	// Control View
	private TextView	                         ct_txt_TongChi	    =null;
	private TextView	                         ct_txt_TongThu	    =null;
	private TextView	                         ct_txt_TongThuChi	=null;
	private Spinner	                           ct_sp_tenDanhMuc	  =null;
	private WebView	                           webView	          =null;
	private DatePickerDialog.OnDateSetListener	date	            =null;
	// Adapter
	private Calendar	                         calendar	          =Calendar
	                                                                  .getInstance();
	// Làm việc với Database
	private List<CtChiTieu>	                   listThuChi	        =new ArrayList<CtChiTieu>();
	private List<CtDanhMuc>	                   lstCtDM	          =new ArrayList<CtDanhMuc>();
	private ArrayAdapter<CtDanhMuc>	           ctDanhMucAdapter	  =null;
	private CtChitieuAdapter	                 ctChiTieuAdapter;
	private CtDanhMucDataSource	               ctDanhMucDataSource;
	private CtChiTieuDataSource	               ctChiTieuDataSource;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initComponent();
		loadTabs();
//		changeByRadio();
		doCreateFakeDataChiTieu();
		loadTroGiupWebView();
		// doCreateFakeDataChiTieu2();
		displayListView();
		date=new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year,
			    int monthOfYear, int dayOfMonth) {
				calendar.set(Calendar.YEAR,year);
				calendar.set(Calendar.MONTH,monthOfYear);
				calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
				updateLabel();
			}
		};
		ct_txt_ngayTao
		    .setOnClickListener(new OnClickListener() {
			    @Override
			    public void onClick(View v) {
				    new DatePickerDialog(MainActivity.this,date,
				        calendar.get(Calendar.YEAR),calendar
				            .get(Calendar.MONTH),calendar
				            .get(Calendar.DAY_OF_MONTH)).show();
			    }
		    });
		// Update cot gia tri date
		updateLabel();
		// su kien thay doi menu theo radio button : Thu | Chi
		changeByRadio();
		// An icon
		ct_lv_thuChi
		    .setOnItemClickListener(new OnItemClickListener() {
			    @Override
			    public void onItemClick(AdapterView<?> parent,
			        View view, int position, long id) {
				    CtChiTieu item=ctChiTieuAdapter
				        .getItem(position);
				    ct_txt_tenChiTieu.setText(item.getTenChiTieu());
				    ct_txt_tienChiTieu.setText(item
				        .getTienChiTieu()+"");
				    calendar.setTime(item.getNgayTao());
				    updateLabel();
				    // ct_txt_ngayTao.setText(DateUtils.yyyyMMdd(date)+"");
				    if(item.getIdDanhMuc().getTrangThai()==0) {
					    ct_rd_thu.setSelected(true);
				    } else {
					    ct_rd_chi.setSelected(true);
				    }
			    }
		    });
		// ct_sp_tenDanhMuc
		// .setOnItemClickListener(new OnItemClickListener() {
		// @Override
		// public void onItemClick(AdapterView<?> parent,
		// View view, int position, long id) {
		// // Toast.makeText(this,"Vao day",
		// // Toast.LENGTH_SHORT).show();
		// // Log.e("danhmucclick",view.toString());
		// // CtDanhMuc ctDm=ctDanhMucAdapter
		// // .getItem(ct_sp_tenDanhMuc
		// // .getSelectedItemPosition());
		// // // ct_txt_tenChiTieu=(TextView)
		// // // findViewById(R.id.ct_txt_tenChiTieu);
		// // // ct_txt_tenChiTieu.setText(item.getTenDanhMuc()
		// // // +" "+listThuChi.size());
		// // Toast.makeText(MainActivity.this,
		// // ctDm.getTenDanhMuc(),Toast.LENGTH_SHORT)
		// // .show();
		// }
		// });
	}
	// /**
	// * Su kien List view click
	// *
	// * @param view
	// */
	// public void ct_lv_thuChiOnCLick(View view) {
	// int position=ct_lv_thuChi.getSelectedItemPosition();
	// CtChiTieu item=ctChiTieuAdapter.getItem(position);
	// Toast.makeText(MainActivity.this,"vaoday",
	// Toast.LENGTH_SHORT).show();
	// }
	private void updateLabel() {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd",
		    Locale.US);
		ct_txt_ngayTao.setText(sdf.format(calendar.getTime()));
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main,menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id=item.getItemId();
		if(id==R.id.action_settings) { return true; }
		return super.onOptionsItemSelected(item);
	}
	public void ct_rd_chiOnClicked(View view) {
		changeByRadio();
	}
	public void ct_rd_thuOnClicked(View view) {
		changeByRadio();
	}
	// 08-04 07:43:54.772: E/Chitieu :(1049): CtChiTieu [idChiTieu=92,
	// tenChiTieu=Thu 91, idDanhMuc=An sinh xã hội, tienChiTieu=50000.0,
	// trangThai=0, ghiChu=An sinh xã hội, ngayTao=Sat Jul 18 00:00:00 GMT+07:00
	// 2015, ngaySua=null]
	/**
	 * initialization component
	 */
	public void initComponent() {
		// initialization component source
		ct_sp_tenDanhMuc=(Spinner) findViewById(R.id.ct_sp_tenDanhMuc);
		ct_rd_thu=(RadioButton) findViewById(R.id.ct_rd_thu);
		ct_rd_chi=(RadioButton) findViewById(R.id.ct_rd_chi);
		ct_lv_thuChi=(ListView) findViewById(R.id.ct_lv_thuChi);
		ct_txt_tenChiTieu=(TextView) findViewById(R.id.ct_txt_tenChiTieu);
		ct_txt_tienChiTieu=(TextView) findViewById(R.id.ct_txt_tienChiTieu);
		// Hien thi ket qua tong thu, chi, tong chu chi
		ct_txt_TongThu=(TextView) findViewById(R.id.ct_txt_TongThu);
		ct_txt_TongChi=(TextView) findViewById(R.id.ct_txt_TongChi);
		ct_txt_TongThuChi=(TextView) findViewById(R.id.ct_txt_TongThuChi);
		// ------
		webView=(WebView) findViewById(R.id.ct_tg_wv_trogiup);
		ct_txt_ngayTao=(EditText) findViewById(R.id.ct_txt_ngayTao);
		// initialization data source
		ctDanhMucDataSource=new CtDanhMucDataSource(this);
		ctChiTieuDataSource=new CtChiTieuDataSource(this);
		DanhMucDao danhMucDao=new DanhMucDao(this);
//		List<DanhMuc> lstDanhMuc=danhMucDao.findAll();
		// List<DanhMuc> lstDanhMuc=danhMucDao.findBy(DanhMuc.DanhMucTrangThai,"1");
		// Collection<Integer> allPKs=danhMucDao.getAllPKs();
//		DanhMuc dm=lstDanhMuc.get(lstDanhMuc.size()-1);
//		Log.e("Value test : ",dm.getDanhMucId()+"");
//		dm.setDanhMucId(dm.getDanhMucId()+1);
		
		DanhMuc danhMuc=danhMucDao.findByPK(27);
		Log.e("Value test : ",danhMuc.getDanhMucTen()+"");
//		danhMucDao.insert(danhMuc);
		danhMuc.setDanhMucTen(danhMuc.getDanhMucTen() + " Update");
		danhMucDao.update(danhMuc);
		danhMuc=danhMucDao.findByPK(27);
		Log.e("Value test : ",danhMuc.getDanhMucTen()+"");
//		Integer insert=danhMucDao.findByPK(27);
		try {
			Log.e("Value test : ",danhMuc.getDanhMucTen()+"");
		} catch(Exception ex) {
			Log.e("getConnectionSQLLite",ex.getMessage()+"");
		}
	}
	/**
	 * Su kien thay doi theo radio button thu chi
	 */
	private void changeByRadio() {
		lstCtDM.removeAll(lstCtDM);
		if(ct_rd_thu.isChecked()) {
			lstCtDM=ctDanhMucDataSource.getAllByStatus(0);
			ct_txt_tenChiTieu
			    .setText(getStringRes(R.string.ct_ct_thu)+" "
			        +listThuChi.size());
		} else {
			lstCtDM=ctDanhMucDataSource.getAllByStatus(1);
			ct_txt_tenChiTieu
			    .setText(getStringRes(R.string.ct_ct_chi)+" "
			        +listThuChi.size());
		}
		ctDanhMucAdapter=new CtDanhMucAdapter(
		    MainActivity.this,layout.simple_spinner_item,
		    (ArrayList<CtDanhMuc>) lstCtDM);
		// ctDanhMucAdapter=new CtDanhMucAdapter(
		// MainActivity.this,R.layout.item_sp_danhmuc,
		// (ArrayList<CtDanhMuc>) lstCtDM);
		ct_sp_tenDanhMuc.setAdapter(ctDanhMucAdapter);
	}
	/**
	 * Su kien click nut them
	 * 
	 * @param v
	 */
	public void btnClickBtThem(View v) {
		// TimePicker ct_t_gioTao=(TimePicker) findViewById(R.id.ct_t_gioTao);
		int status=1;
		if(ct_rd_thu.isChecked()) status=0;
		CtDanhMuc ctDm=ctDanhMucAdapter
		    .getItem(ct_sp_tenDanhMuc.getSelectedItemPosition());
		ctDm.setTrangThai(status);
		// Add data
		CtChiTieu chiTieu=new CtChiTieu(listThuChi.size()+1,
		    ctDm,ct_txt_tenChiTieu.getText().toString(),
		    Float.parseFloat(ct_txt_tienChiTieu.getText()
		        .toString()),2);
		chiTieu.setNgayTao(DateUtils
		    .stringToDate(ct_txt_ngayTao.getText().toString()));
		listThuChi.add(chiTieu);
		ctChiTieuDataSource.create(chiTieu);
		// ctChiTieuAdapter.notifyDataSetChanged();
		displayListView();
		changeByRadio();
	}
	public void displayListView() {
		listThuChi.removeAll(listThuChi);
		// Collections.sort(listThuChi);
		listThuChi=ctChiTieuDataSource.getAll();
		if(listThuChi!=null&&listThuChi.size()>0) {
			tongChi=tongThu=tongThuChi=0f;
			for(CtChiTieu item:listThuChi) {
				if(item.getIdDanhMuc()!=null) {
					if(item.getIdDanhMuc().getTrangThai()==0)
						tongThu+=item.getTienChiTieu();
					else tongChi+=item.getTienChiTieu();
					tongThuChi=tongThu-tongChi;
				}
			}
		}
		// Hien thi ket qua tong thu, chi, tong chu chi
		ct_txt_TongThu.setText(getStringRes(R.string.ct_ct_thu)
		    +": "+float2String2f(tongThu));
		ct_txt_TongChi.setText(getStringRes(R.string.ct_ct_chi)
		    +": "+float2String2f(tongChi));
		ct_txt_TongThuChi
		    .setText(getStringRes(R.string.ct_ct_thu_chi)+": "
		        +float2String2f(tongThuChi));
		// Load danh sach thu chi
		// ct_lv_thuChi=(ListView) findViewById(R.id.ct_lv_thuChi);
		ctChiTieuAdapter=new CtChitieuAdapter(
		    MainActivity.this,R.layout.item_lv_thuchi,
		    (ArrayList<CtChiTieu>) listThuChi);
		ct_lv_thuChi.setAdapter(ctChiTieuAdapter);
		openChart();
	}
	/**
	 * Hien thi tro giup webview
	 */
	@SuppressLint("SetJavaScriptEnabled")
	private void loadTroGiupWebView() {
		// webView.loadUrl("http://developer.android.com");
		class JsObject {
			@JavascriptInterface
			public String toString() {
				return "injectedObject";
			}
		}
		webView.addJavascriptInterface(new JsObject(),
		    "injectedObject");
		// webView.loadUrl("http://developer.android.com");
		webView.loadUrl("http://weavesilk.com");
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new MyWebViewCient());
	}
	public String getStringRes(int id) {
		return getApplication().getResources().getString(id);
	}
	public int getColorRes(int id) {
		return getApplication().getResources().getColor(id);
	}
	public Drawable getIconRes(int id) {
		return getApplication().getResources().getDrawable(id);
	}
	/**
	 * Lay gia tri Drawable tu System
	 * 
	 * @param id
	 *          String system
	 * @return String tu he thong
	 */
	public String getStringSys(int id) {
		return Resources.getSystem().getString(id);
	}
	/**
	 * Lay gia tri Drawable tu System
	 * 
	 * @param id
	 *          drawable id
	 * @return Drawable
	 */
	public Drawable getDrawalbeSys(int id) {
		return Resources.getSystem().getDrawable(id);
	}
	/**
	 * Lay gia tri Drawable tu Resource
	 * 
	 * @param id
	 *          drawable id
	 * @return Drawable
	 */
	public Drawable getDrawalbeRs(int id) {
		return Resources.getSystem().getDrawable(id);
	}
	/**
	 * Lay gia tri mang tu Resource
	 * 
	 * @param id
	 *          array id
	 * @return String[]
	 */
	public String[] getStringArrayRes(int id) {
		return getApplication().getResources().getStringArray(
		    id);
	}
	/**
	 * Tao giu lieu test
	 */
	private void doCreateFakeDataChiTieu() {
		listThuChi=ctChiTieuDataSource.getAll();
		if(listThuChi.size()<=0) {
			for(int i=1;i<20;i++) {
				// int Có 0: Thu | 1 : Chi : 2 : Xoa
				CtChiTieu chiTieu=new CtChiTieu(
				    listThuChi.size()+1,new CtDanhMuc(i,null,1),
				    "Chitieu"+i,i*123f,2);
				chiTieu.setNgayTao(DateUtils.getNowDate());
				ctChiTieuDataSource.create(chiTieu);
			}
			listThuChi=ctChiTieuDataSource.getAll();
		}
	}
	// Click to del
	public void ct_lv_col_iconDelThuChiOnClick(View view) {
		CtChiTieu item=ctChiTieuAdapter.getItem(ct_lv_thuChi
		    .getSelectedItemPosition());
		Toast.makeText(MainActivity.this,"Del"+item.toString(),
		    Toast.LENGTH_SHORT).show();
		ctChiTieuDataSource.delete(item);
		displayListView();
	}
	/*
	 * Load cac tab vao trong tabhost
	 */
	public void loadTabs() {
		// Lấy Tabhost id ra trước (cái này của built - in android
		final TabHost tabhost=(TabHost) findViewById(android.R.id.tabhost);
		// gọi lệnh setup
		tabhost.setup();
		TabHost.TabSpec spec;
		// Tạo tab1
		spec=tabhost.newTabSpec("t1");
		spec.setContent(R.id.tab1);
		TextView v1=new TextView(this);
		v1.setText(getStringRes(R.string.ct_ct_thu_chi));
		spec.setIndicator(getStringRes(R.string.ct_ct_thu_chi));
		tabhost.addTab(spec);
		// Tạo tab2
		spec=tabhost.newTabSpec("t2");
		spec.setContent(R.id.tab2);
		spec.setIndicator(getStringRes(R.string.ct_tk_taikhoan));
		tabhost.addTab(spec);
		// Tạo tab3
		spec=tabhost.newTabSpec("t3");
		spec.setContent(R.id.tab3);
		spec.setIndicator(getStringRes(R.string.ct_dl_dulieu));
		tabhost.addTab(spec);
		// Tạo tab3
		spec=tabhost.newTabSpec("t4");
		spec.setContent(R.id.tab4);
		spec.setIndicator(getStringRes(R.string.ct_pt_phantich));
		tabhost.addTab(spec);
		// Tạo tab3
		spec=tabhost.newTabSpec("t5");
		spec.setContent(R.id.tab5);
		spec.setIndicator(getStringRes(R.string.ct_tg_trogiup));
		tabhost.addTab(spec);
		// Thiết lập tab mặc định được chọn ban đầu là tab 0
		for(int i=0;i<tabhost.getTabWidget().getChildCount();i++) {
			tabhost
			    .getTabWidget()
			    .getChildAt(i)
			    .setBackgroundColor(
			        getColorRes(R.color.WhiteSmoke));
			tabhost.getTabWidget().getChildAt(i)
			    .getLayoutParams().height=60;
		}
		tabhost.setCurrentTab(0);
		tabhost.getTabWidget()
		    .getChildAt(tabhost.getCurrentTab())
		    .setBackgroundColor(getColorRes(R.color.LightBlue));
		// Ví dụ tab1 chưa nhập thông tin xong mà lại qua tab 2 thì báo...
		tabhost
		    .setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			    public void onTabChanged(String arg0) {
				    for(int i=0;i<tabhost.getTabWidget()
				        .getChildCount();i++) {
					    tabhost.getTabWidget().getChildAt(i)
					        .getLayoutParams().height=60;
					    tabhost
					        .getTabWidget()
					        .getChildAt(i)
					        .setBackgroundColor(
					            getColorRes(R.color.WhiteSmoke));
				    }
				    tabhost
				        .getTabWidget()
				        .getChildAt(tabhost.getCurrentTab())
				        .setBackgroundColor(
				            getColorRes(R.color.LightBlue));
			    }
		    });
	}
	class MyWebViewCient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view,
		    String url) {
			if(Uri.parse(url).getHost().equals("weavesilk.com"))
			  return false;
			Intent intent=new Intent(Intent.ACTION_VIEW,
			    Uri.parse(url));
			startActivity(intent);
			return true;
		}
	}
	// ==========================Draw pie chart========================
	private View	mPieChart;
	protected void onCreate2(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button btnPieChart=(Button) findViewById(R.id.btnPieChart);
		OnClickListener clickEvent=new OnClickListener() {
			@Override
			public void onClick(View v) {
				openChart();
			}
		};
		btnPieChart.setOnClickListener(clickEvent);
	}
	private void openChart() {
		String[] categories=new String[]{
		    getStringRes(R.string.ct_ct_thu),
		    getStringRes(R.string.ct_ct_chi),
		    getStringRes(R.string.ct_ct_thu_chi)};
		// double[] proportion={0.4,0.3,0.3};
		// double[] proportion={tongThu,tongChi};
		float perscentThu=tongThu/tongThuChi*100;
		float perscentChi=tongChi/tongThuChi*100;
		// double[] proportion={perscentThu,perscentChi};
		double[] proportion={tongThu,tongChi};
		int[] color={Color.BLUE,Color.RED,Color.YELLOW};
		CategorySeries expenseSeries=new CategorySeries(
		    getStringRes(R.string.ct_ct_thu_chi));
		// Them ten va gia tri cho tung khoan chi
		for(int i=0;i<proportion.length;i++) {
			expenseSeries.add(categories[i],proportion[i]);
		}
		DefaultRenderer defaultRenderer=new DefaultRenderer();
		for(int i=0;i<proportion.length;i++) {
			SimpleSeriesRenderer seriesRenderer=new SimpleSeriesRenderer();
			seriesRenderer.setColor(color[i]);
			seriesRenderer.setDisplayChartValues(true);
			seriesRenderer.setChartValuesTextSize(40);
			// Them mau cho background
			defaultRenderer.setBackgroundColor(Color.GRAY);
			defaultRenderer.setApplyBackgroundColor(true);
			// Tao ra tung slice cua pie chart
			defaultRenderer.addSeriesRenderer(seriesRenderer);
		}
		defaultRenderer
		    .setChartTitle(getStringRes(R.string.ct_ct_thu_chi));
		defaultRenderer.setChartTitleTextSize(60);
		defaultRenderer.setLabelsTextSize(30);
		defaultRenderer.setLegendTextSize(30);
		defaultRenderer.setDisplayValues(true);
		defaultRenderer.setZoomButtonsVisible(false);
		LinearLayout chartContainer=(LinearLayout) findViewById(R.id.chart);
		// remove view before painting the chart
		chartContainer.removeAllViews();
		// Ve pie chart
		mPieChart=ChartFactory.getPieChartView(
		    getBaseContext(),expenseSeries,defaultRenderer);
		// Add view to the linearlayout
		chartContainer.addView(mPieChart);
	}
	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.main,menu);
	// return true;
	// }
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// // Handle action bar item clicks here. The action bar will
	// // automatically handle clicks on the Home/Up button, so long
	// // as you specify a parent activity in AndroidManifest.xml.
	// int id=item.getItemId();
	// if(id==R.id.action_settings) { return true; }
	// return super.onOptionsItemSelected(item);
	// }
}
