package com.aptech.chitieu;

import java.text.SimpleDateFormat;
import java.util.*;

import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import com.aptech.chitieu.entity.ChiTieu;
import com.aptech.chitieu.entity.DanhMuc;
import com.aptech.chitieu.entity.TaiKhoan;
import com.aptech.chitieu2.adapter.ChitieuAdapter;
import com.aptech.chitieu2.adapter.DanhMucAdapter2;
import com.aptech.chitieu2.adapter.TaiKhoanAdapter;
import com.aptech.chitieu2.adapter.TaiKhoanSpinnerAdapter;
import com.aptech.chitieu2.dao.ChiTieuDao;
import com.aptech.chitieu2.dao.DanhMucDao;
import com.aptech.chitieu2.dao.HinhAnhDao;
import com.aptech.chitieu2.dao.SuKienDao;
import com.aptech.chitieu2.dao.TaiKhoanDao;
import com.aptech.chitieu2.dao.TienDao;
import android.R.layout;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View.OnClickListener;
import android.webkit.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import static com.aptech.chitieu.util.DigestUtil.*;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Title : Main Class<br/>
 * Description : The fisrt run program here<br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Jul 16, 2015 10:26:32 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Jul 16, 2015 10:26:32 PM
 */
public class MainActivity03 extends Activity {
	// public static List<ChiTieu> listThuChi =new ArrayList<ChiTieu>();
	private float tongThuChi = 0f;
	private float tongThu = 0f;
	private float tongChi = 0f;
	// Control
	private TextView ct_txt_chiTieuId = null;
	private ListView ct_lv_thuChi = null;
	private RadioButton ct_dl_rd_thu = null;
	private RadioButton ct_rd_chi = null;
	private TextView ct_txt_tenChiTieu = null;
	private TextView ct_txt_tienChiTieu = null;
	private EditText ct_txt_ngayTao = null;
	// Control View
	private TextView ct_txt_TongChi = null;
	private TextView ct_txt_TongThu = null;
	private TextView ct_txt_TongThuChi = null;
	private Spinner ct_sp_tenDanhMuc = null;
	private Spinner ct_hd_spTaiKhoan = null;
	private Spinner ct_pt_spTaiKhoan = null;

	private WebView webView = null;
	private DatePickerDialog.OnDateSetListener date = null;
	private ChiTieu chiTieuCurrent = null;
	// tab tai khoan
	ListView ct_tk_lv_thuChi = null;
	ListView ct_dl_lv_thuChi = null;
	// Adapter
	private Calendar calendar = Calendar.getInstance();
	// Làm việc với Database
	private List<ChiTieu> hdLstChiTieu = new ArrayList<ChiTieu>();

	private List<TaiKhoan> lstTaiKhoan = new ArrayList<TaiKhoan>();
	private List<DanhMuc> lstDanhMuc = new ArrayList<DanhMuc>();
	private ArrayAdapter<DanhMuc> danhMucAdapter = null;
	private ChitieuAdapter chiTieuAdapter;
	private TaiKhoanAdapter taiKhoanAdapter;
	private TaiKhoanSpinnerAdapter hdTaiKhoanSpinnerAdapter = null;
	private DanhMucAdapter2 danhMucAdapter2;
	private DanhMucDao danhMucDao;
	private ChiTieuDao chiTieuDao;
	private HinhAnhDao hinhAnhDao;
	private TaiKhoanDao taiKhoanDao;
	private SuKienDao suKienDao;
	private TienDao tienDao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initComponent();
		loadTabs();
		changeByRadio();
		doCreateFakeDataChiTieu();
		// loadTroGiupWebView();
		// doCreateFakeDataChiTieu2();
		displayListView();
		date = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				calendar.set(Calendar.YEAR, year);
				calendar.set(Calendar.MONTH, monthOfYear);
				calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				updateLabel();
			}
		};
		// ct_txt_ngayTao.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// new DatePickerDialog(MainActivity03.this, date, calendar
		// .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
		// calendar.get(Calendar.DAY_OF_MONTH)).show();
		// }
		// });
		// Update cot gia tri date
		// updateLabel();
		// su kien thay doi menu theo radio button : Thu | Chi
		// changeByRadio();
		// An icon

		ct_lv_thuChi.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Log.e("ct_lv_thuChi.setOnItemSelectedListener", "OK");
				ChiTieu chiTieu = chiTieuAdapter.getItem(position);
				Intent intent = new Intent(getApplicationContext(),
						ChiTieuMain.class);
				intent.putExtra("tk", getTaiKhoanSpinnerAdapter());
				intent.putExtra("chiTieu", chiTieu);
				startActivity(intent);
			}
		});
		ct_tk_lv_thuChi = (ListView) findViewById(R.id.ct_dl_lv_thuChi);
		ct_tk_lv_thuChi.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				TaiKhoan taiKhoan = taiKhoanAdapter.getItem(position);
				Log.e("ct_tk_lv_thuChi.setOnItemSelectedListener",
						taiKhoan.getTaiKhoanTen());
				Intent intent = new Intent(getApplicationContext(),
						TaiKhoanMain.class);
				intent.putExtra("taiKhoan", taiKhoan);
				startActivity(intent);
			}
		});
		ct_dl_lv_thuChi = (ListView) findViewById(R.id.ct_dl_lv_thuChi);
		ct_dl_lv_thuChi.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				DanhMuc danhMuc = danhMucAdapter2.getItem(position);
				Log.e("ct_dl_lv_thuChi.setOnItemSelectedListener",
						danhMuc.getDanhMucTen());
				Intent intent = new Intent(getApplicationContext(),
						DanhMuc.class);
				intent.putExtra("danhMuc", danhMuc);
				startActivity(intent);
			}
		});
		ct_tk_lv_thuChi = (ListView) findViewById(R.id.ct_tk_lv_thuChi);
		ct_tk_lv_thuChi.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				TaiKhoan taiKhoan = taiKhoanAdapter.getItem(position);
				Log.e("ct_tk_lv_thuChi.setOnItemSelectedListener",
						taiKhoan.getTaiKhoanTen());
				Intent intent = new Intent(getApplicationContext(),
						TaiKhoanMain.class);
				intent.putExtra("taiKhoan", taiKhoan);
				startActivity(intent);
			}
		});

		ct_hd_spTaiKhoan
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						int position = ct_hd_spTaiKhoan
								.getSelectedItemPosition();
						TaiKhoan tk = hdTaiKhoanSpinnerAdapter
								.getItem(position);
						if (chiTieuDao != null) {
							chiTieuDao = new ChiTieuDao(getApplicationContext());
						}

						hdLstChiTieu = chiTieuDao.findOne(ChiTieu.TaiKhoanId,
								tk.getId());
						chiTieuAdapter = new ChitieuAdapter(
								MainActivity03.this, R.layout.item_lv_thuchi,
								(ArrayList<ChiTieu>) hdLstChiTieu);
						chiTieuAdapter.notifyDataSetChanged();
						ct_lv_thuChi.setAdapter(chiTieuAdapter);
						tinhtong();
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}

				});
		ct_pt_spTaiKhoan
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						int position = ct_pt_spTaiKhoan
								.getSelectedItemPosition();
						TaiKhoan tk = hdTaiKhoanSpinnerAdapter
								.getItem(position);
						if (chiTieuDao != null) {
							chiTieuDao = new ChiTieuDao(getApplicationContext());
						}

						hdLstChiTieu = chiTieuDao.findOne(ChiTieu.TaiKhoanId,
								tk.getId());
						chiTieuAdapter = new ChitieuAdapter(
								MainActivity03.this, R.layout.item_lv_thuchi,
								(ArrayList<ChiTieu>) hdLstChiTieu);
						chiTieuAdapter.notifyDataSetChanged();
						ct_lv_thuChi.setAdapter(chiTieuAdapter);
						tinhtong();
						openChart();
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {

					}

				});

	}

	private void updateLabel() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		ct_txt_ngayTao.setText(sdf.format(calendar.getTime()));
	}

	public void ct_dl_rd_chiOnClicked(View view) {
		changeByRadio();
	}

	public void ct_dl_rd_thuOnClicked(View view) {
		changeByRadio();
	}

	/**
	 * initialization component
	 */
	public void initComponent() {
		Log.i("initComponent", "initComponent start..");
		// -------------initialization component source-------------
		ct_sp_tenDanhMuc = (Spinner) findViewById(R.id.ct_sp_tenDanhMuc);
		ct_hd_spTaiKhoan = (Spinner) findViewById(R.id.ct_hd_spTaiKhoan);
		ct_pt_spTaiKhoan = (Spinner) findViewById(R.id.ct_pt_spTaiKhoan);
		ct_txt_chiTieuId = (TextView) findViewById(R.id.ct_txt_chiTieuId);
		ct_dl_rd_thu = (RadioButton) findViewById(R.id.ct_dl_rd_thu);
		ct_rd_chi = (RadioButton) findViewById(R.id.ct_dl_rd_chi);
		ct_lv_thuChi = (ListView) findViewById(R.id.ct_lv_thuChi);
		ct_txt_tenChiTieu = (TextView) findViewById(R.id.ct_txt_tenChiTieu);
		ct_txt_tienChiTieu = (TextView) findViewById(R.id.ct_txt_tienChiTieu);
		// Hien thi ket qua tong thu, chi, tong chu chi-----------------
		ct_txt_TongThu = (TextView) findViewById(R.id.ct_txt_TongThu);
		ct_txt_TongChi = (TextView) findViewById(R.id.ct_txt_TongChi);
		ct_txt_TongThuChi = (TextView) findViewById(R.id.ct_txt_TongThuChi);
		// ============================Tab tai khoan--------------------
		ct_tk_lv_thuChi = (ListView) findViewById(R.id.ct_tk_lv_thuChi);
		// ============================Tab du lieu----------------------
		ct_dl_lv_thuChi = (ListView) findViewById(R.id.ct_dl_lv_thuChi);
		// ------
		webView = (WebView) findViewById(R.id.ct_tg_wv_trogiup);
		ct_txt_ngayTao = (EditText) findViewById(R.id.ct_txt_ngayTao);
		// ---------------------------init dao--------------------------
		danhMucDao = new DanhMucDao(this);
		chiTieuDao = new ChiTieuDao(this);
		hinhAnhDao = new HinhAnhDao(this);
		taiKhoanDao = new TaiKhoanDao(this);
		suKienDao = new SuKienDao(this);
		tienDao = new TienDao(this);

		List<ChiTieu> del = chiTieuDao.findBy(ChiTieu.DanhMucId, 0);
		for (ChiTieu chiTieu : del) {
			chiTieuDao.delete(chiTieu);
		}
		lstDanhMuc = danhMucDao.findAll();
		hdLstChiTieu = chiTieuDao.findAll();
		lstDanhMuc = danhMucDao.findAll();
		lstTaiKhoan = taiKhoanDao.findAll();
		// -----------------------init adapter--------------------------

		Log.i("initComponent", "initComponent end..");
	}

	/**
	 * Su kien thay doi theo radio button thu chi
	 */
	private void changeByRadio() {
		Log.i("changeByRadio", "changeByRadio start..");
		lstDanhMuc.removeAll(lstDanhMuc);
		if (ct_dl_rd_thu.isChecked())
			lstDanhMuc = danhMucDao.findBy(DanhMuc.DanhMucTrangThai, 0);
		else
			lstDanhMuc = danhMucDao.findBy(DanhMuc.DanhMucTrangThai, 1);

		// ct_txt_tienChiTieu
		// .setText(getStringRes(R.string.ct_ct_tienChiTieu_50000));

		// =============== hien thi tab danhMucAdapter2
		danhMucAdapter2 = new DanhMucAdapter2(MainActivity03.this,
				R.layout.item_lv_danhmuc, (ArrayList<DanhMuc>) lstDanhMuc);
		ct_dl_lv_thuChi.setAdapter(danhMucAdapter2);

		// danhMucAdapter = new com.aptech.chitieu2.adapter.DanhMucAdapter(
		// MainActivity03.this, layout.simple_spinner_item,
		// (ArrayList<DanhMuc>) lstDanhMuc);
		// DanhMucAdapter=new DanhMucAdapter(
		// MainActivity.this,R.layout.item_sp_danhmuc,
		// (ArrayList<DanhMuc>) lstCtDM);
		// ct_sp_tenDanhMuc.setAdapter(danhMucAdapter);
		Log.i("changeByRadio", "changeByRadio end..");
	}

	public void mnActionSettingsClick(View view) {
		Toast.makeText(MainActivity03.this, "mnActionSettingsClick",
				Toast.LENGTH_SHORT).show();
	}

	/**
	 * Su kien click nut them
	 * 
	 * @param v
	 */
	public void btnClickBtThem(View v) {
		Intent intent = new Intent(getApplicationContext(), ChiTieuMain.class);
		intent.putExtra("tk", getTaiKhoanSpinnerAdapter());
		startActivity(intent);
	}

	/**
	 * Su kien click nut them
	 * 
	 * @param v
	 */
	public void ct_tk_bt_luuClick(View v) {
		Intent intent = new Intent(getApplicationContext(), TaiKhoanMain.class);
		Log.e("ct_tk_bt_luuClick", "");
		intent.putExtra("taiKhoan", taiKhoanDao.getEntityClass());
		startActivity(intent);
	}

	/**
	 * Su kien click nut them
	 * 
	 * @param v
	 */
	public void ct_dl_bt_luuClick(View v) {
		Intent intent = new Intent(getApplicationContext(), DanhMucMain.class);
		// intent.putExtra("tk", getTaiKhoanSpinnerAdapter());
		startActivity(intent);
	}

	/**
	 * get {@link TaiKhoan} object had been item selected
	 * 
	 * @return {@link TaiKhoan} object
	 */
	private TaiKhoan getTaiKhoanSpinnerAdapter() {
		return hdTaiKhoanSpinnerAdapter.getItem(ct_hd_spTaiKhoan
				.getSelectedItemPosition());
	}

	/**
	 * get {@link TaiKhoan} object had been item selected
	 * 
	 * @return {@link TaiKhoan} object
	 */
	private TaiKhoan getTaiKhoanPhanTichSpinnerAdapter() {
		return hdTaiKhoanSpinnerAdapter.getItem(ct_pt_spTaiKhoan
				.getSelectedItemPosition());
	}

	/**
	 * Hien thi displayListView
	 */
	public void displayListView() {
		hdLstChiTieu.removeAll(hdLstChiTieu);
		// Collections.sort(listThuChi);
		hdLstChiTieu = chiTieuDao.findAll();
		tinhtong();
		// Load danh sach thu chi
		// ct_lv_thuChi=(ListView) findViewById(R.id.ct_lv_thuChi);
		chiTieuAdapter = new ChitieuAdapter(MainActivity03.this,
				R.layout.item_lv_thuchi, (ArrayList<ChiTieu>) hdLstChiTieu);
		ct_lv_thuChi.setAdapter(chiTieuAdapter);
		openChart();
		// =============== hien thi tab danhMucAdapter2
		danhMucAdapter2 = new DanhMucAdapter2(MainActivity03.this,
				R.layout.item_lv_danhmuc, (ArrayList<DanhMuc>) lstDanhMuc);
		ct_dl_lv_thuChi.setAdapter(danhMucAdapter2);
		// =============== hien thi tab taiKhoanAdapter
		taiKhoanAdapter = new TaiKhoanAdapter(MainActivity03.this,
				R.layout.item_lv_taikhoan, (ArrayList<TaiKhoan>) lstTaiKhoan);
		ct_tk_lv_thuChi.setAdapter(taiKhoanAdapter);
		hdTaiKhoanSpinnerAdapter = new TaiKhoanSpinnerAdapter(
				MainActivity03.this, android.R.layout.simple_spinner_item,
				(ArrayList<TaiKhoan>) lstTaiKhoan);
		ct_hd_spTaiKhoan.setAdapter(hdTaiKhoanSpinnerAdapter);
		ct_pt_spTaiKhoan.setAdapter(hdTaiKhoanSpinnerAdapter);
		openChart();
	}

	/**
	 * Tinh tong tien
	 */
	private void tinhtong() {
		tongChi = tongThu = tongThuChi = 0f;
		if (hdLstChiTieu != null && hdLstChiTieu.size() > 0) {
			for (ChiTieu item : hdLstChiTieu) {
				if (item.getDanhMucId() != null && item.getDanhMucId() != 0) {
					Log.e("getDanhMucId", item.getDanhMucId() + "");
					DanhMuc dm = danhMucDao.findByPK(item.getDanhMucId());
					if (dm.getDanhMucTrangThai() == 0)
						// if(item.getDanhMucId()==0)
						tongThu += item.getChiTieuSoTien();
					else
						tongChi += item.getChiTieuSoTien();
					tongThuChi = tongThu - tongChi;
				}
			}
		}
		// Hien thi ket qua tong thu, chi, tong chu chi
		ct_txt_TongThu.setText(getStringRes(R.string.ct_ct_thu) + ": "
				+ float2String2f(tongThu));
		ct_txt_TongChi.setText(getStringRes(R.string.ct_ct_chi) + ": "
				+ float2String2f(tongChi));
		ct_txt_TongThuChi.setText(getStringRes(R.string.ct_ct_thu_chi) + ": "
				+ float2String2f(tongThuChi));

	}

	/**
	 * Hien thi tro giup webview
	 */
	@SuppressLint("SetJavaScriptEnabled")
	private void loadTroGiupWebView() {
		// webView.loadUrl("http://developer.android.com");
		class JsObject {
			@JavascriptInterface
			public String toString() {
				return "injectedObject";
			}
		}
		webView.addJavascriptInterface(new JsObject(), "injectedObject");
		webView.loadUrl("http://developer.android.com");
		// webView.loadUrl("http://weavesilk.com");
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new MyWebViewCient());
	}

	public String getStringRes(int id) {
		return getApplication().getResources().getString(id);
	}

	public int getColorRes(int id) {
		return getApplication().getResources().getColor(id);
	}

	public Drawable getIconRes(int id) {
		return getApplication().getResources().getDrawable(id);
	}

	/**
	 * Lay gia tri Drawable tu System
	 * 
	 * @param id
	 *            String system
	 * @return String tu he thong
	 */
	public String getStringSys(int id) {
		return Resources.getSystem().getString(id);
	}

	/**
	 * Lay gia tri Drawable tu System
	 * 
	 * @param id
	 *            drawable id
	 * @return Drawable
	 */
	public Drawable getDrawalbeSys(int id) {
		return Resources.getSystem().getDrawable(id);
	}

	/**
	 * Lay gia tri Drawable tu Resource
	 * 
	 * @param id
	 *            drawable id
	 * @return Drawable
	 */
	public Drawable getDrawalbeRs(int id) {
		return Resources.getSystem().getDrawable(id);
	}

	/**
	 * Lay gia tri mang tu Resource
	 * 
	 * @param id
	 *            array id
	 * @return String[]
	 */
	public String[] getStringArrayRes(int id) {
		return getApplication().getResources().getStringArray(id);
	}

	/**
	 * Tao giu lieu test
	 */
	private void doCreateFakeDataChiTieu() {
		hdLstChiTieu = chiTieuDao.findAll();
	}

	// Click to del
	public void ct_lv_col_iconDelThuChiOnClick(View view) {
		ChiTieu item = chiTieuAdapter.getItem(ct_lv_thuChi
				.getSelectedItemPosition());
		Toast.makeText(MainActivity03.this, "Del" + item.toString(),
				Toast.LENGTH_SHORT).show();
		chiTieuDao.delete(item);
		displayListView();
	}

	/*
	 * Load cac tab vao trong tabhost
	 */
	public void loadTabs() {
		Log.i("LoadTabs", "LoadTabs starting...");
		// Lấy Tabhost id ra trước (cái này của built - in android
		final TabHost tabhost = (TabHost) findViewById(android.R.id.tabhost);
		// gọi lệnh setup
		tabhost.setup();
		TabHost.TabSpec spec;
		// Tạo tab1
		spec = tabhost.newTabSpec("t1");
		spec.setContent(R.id.tab1);
		TextView v1 = new TextView(this);
		v1.setText(getStringRes(R.string.ct_ct_thu_chi));
		spec.setIndicator(getStringRes(R.string.ct_ct_thu_chi));
		tabhost.addTab(spec);
		// Tạo tab2
		spec = tabhost.newTabSpec("t2");
		spec.setContent(R.id.tab2);
		spec.setIndicator(getStringRes(R.string.ct_tk_taikhoan));
		tabhost.addTab(spec);
		// Tạo tab3
		spec = tabhost.newTabSpec("t3");
		spec.setContent(R.id.tab3);
		spec.setIndicator(getStringRes(R.string.ct_dl_dulieu));
		tabhost.addTab(spec);
		// Tạo tab3
		spec = tabhost.newTabSpec("t4");
		spec.setContent(R.id.tab4);
		spec.setIndicator(getStringRes(R.string.ct_pt_phantich));
		tabhost.addTab(spec);
		// Tạo tab3
		spec = tabhost.newTabSpec("t5");
		spec.setContent(R.id.tab5);
		spec.setIndicator(getStringRes(R.string.ct_tg_trogiup));
		tabhost.addTab(spec);
		// Thiết lập tab mặc định được chọn ban đầu là tab 0
		for (int i = 0; i < tabhost.getTabWidget().getChildCount(); i++) {
			tabhost.getTabWidget().getChildAt(i)
					.setBackgroundColor(getColorRes(R.color.WhiteSmoke));
			tabhost.getTabWidget().getChildAt(i).getLayoutParams().height = 60;
		}
		tabhost.setCurrentTab(0);
		tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab())
				.setBackgroundColor(getColorRes(R.color.LightBlue));
		// Ví dụ tab1 chưa nhập thông tin xong mà lại qua tab 2 thì báo...
		tabhost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			public void onTabChanged(String arg0) {
				for (int i = 0; i < tabhost.getTabWidget().getChildCount(); i++) {
					tabhost.getTabWidget().getChildAt(i).getLayoutParams().height = 60;
					tabhost.getTabWidget()
							.getChildAt(i)
							.setBackgroundColor(getColorRes(R.color.WhiteSmoke));
				}
				tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab())
						.setBackgroundColor(getColorRes(R.color.LightBlue));
			}
		});
		Log.i("LoadTabs", "LoadTabs end...");
	}

	public void ct_dl_rd_thuOnClicked() {
		changeByRadio();

	}

	public void ct_dl_rd_chiOnClicked() {
		changeByRadio();
	}

	class MyWebViewCient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// if(Uri.parse(url).getHost().equals("weavesilk.com"))
			if (Uri.parse(url).getHost().equals("developer.android.com"))
				return false;
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);
			return true;
		}
	}

	// ==========================Draw pie chart========================
	private View mPieChart;

	protected void onCreate2(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button btnPieChart = (Button) findViewById(R.id.btnPieChart);
		OnClickListener clickEvent = new OnClickListener() {
			@Override
			public void onClick(View v) {
				openChart();
			}
		};
		btnPieChart.setOnClickListener(clickEvent);
	}

	private void openChart() {
		String[] categories = new String[] { getStringRes(R.string.ct_ct_thu),
				getStringRes(R.string.ct_ct_chi),
				getStringRes(R.string.ct_ct_thu_chi) };
		// double[] proportion={0.4,0.3,0.3};
		// double[] proportion={tongThu,tongChi};
		float perscentThu = tongThu / tongThuChi * 100;
		float perscentChi = tongChi / tongThuChi * 100;
		// double[] proportion={perscentThu,perscentChi};
		double[] proportion = { tongThu, tongChi };
		int[] color = { Color.BLUE, Color.RED, Color.YELLOW };
		CategorySeries expenseSeries = new CategorySeries(
				getStringRes(R.string.ct_ct_thu_chi));
		// Them ten va gia tri cho tung khoan chi
		for (int i = 0; i < proportion.length; i++) {
			expenseSeries.add(categories[i], proportion[i]);
		}
		DefaultRenderer defaultRenderer = new DefaultRenderer();
		for (int i = 0; i < proportion.length; i++) {
			SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
			seriesRenderer.setColor(color[i]);
			seriesRenderer.setDisplayChartValues(true);
			seriesRenderer.setChartValuesTextSize(40);
			// Them mau cho background
			defaultRenderer.setBackgroundColor(Color.GRAY);
			defaultRenderer.setApplyBackgroundColor(true);
			// Tao ra tung slice cua pie chart
			defaultRenderer.addSeriesRenderer(seriesRenderer);
		}
		defaultRenderer.setChartTitle(getStringRes(R.string.ct_ct_thu_chi));
		defaultRenderer.setChartTitleTextSize(60);
		defaultRenderer.setLabelsTextSize(30);
		defaultRenderer.setLegendTextSize(30);
		defaultRenderer.setDisplayValues(true);
		defaultRenderer.setZoomButtonsVisible(false);
		LinearLayout chartContainer = (LinearLayout) findViewById(R.id.chart);
		// remove view before painting the chart
		chartContainer.removeAllViews();
		// Ve pie chart
		mPieChart = ChartFactory.getPieChartView(getBaseContext(),
				expenseSeries, defaultRenderer);
		// Add view to the linearlayout
		chartContainer.addView(mPieChart);
	}

}
