package com.aptech.chitieu;

import java.text.SimpleDateFormat;
import java.util.*;

import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import com.aptech.chitieu.entity.ChiTieu;
import com.aptech.chitieu.entity.DanhMuc;
import com.aptech.chitieu.entity.TaiKhoan;
import com.aptech.chitieu.util.DateUtils;
import com.aptech.chitieu2.adapter.ChitieuAdapter;
import com.aptech.chitieu2.adapter.DanhMucAdapter2;
import com.aptech.chitieu2.adapter.TaiKhoanAdapter;
import com.aptech.chitieu2.adapter.TaiKhoanSpinnerAdapter;
import com.aptech.chitieu2.dao.ChiTieuDao;
import com.aptech.chitieu2.dao.DanhMucDao;
import com.aptech.chitieu2.dao.HinhAnhDao;
import com.aptech.chitieu2.dao.SuKienDao;
import com.aptech.chitieu2.dao.TaiKhoanDao;
import com.aptech.chitieu2.dao.TienDao;
import android.R.layout;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View.OnClickListener;
import android.webkit.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.*;
import static com.aptech.chitieu.util.DigestUtil.*;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Title : Main Class<br/>
 * Description : The fisrt run program here<br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Jul 16, 2015 10:26:32 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Jul 16, 2015 10:26:32 PM
 */
public class TaiKhoanMain extends Activity {
	// public static List<ChiTieu> listThuChi =new ArrayList<ChiTieu>();
	private float tongThuChi = 0f;
	private float tongThu = 0f;
	private float tongChi = 0f;
	// Control
	private TextView ct_txt_chiTieuId = null;
	private ListView ct_lv_thuChi = null;
	private RadioButton ct_rd_thu = null;
	private RadioButton ct_rd_chi = null;
	private TextView ct_tk_txt_taiKhoanTen = null;
	private TextView ct_txt_tienChiTieu = null;
	private EditText ct_tk_txt_ngayTao = null;
	private EditText ct_tk_ct_GhiChu = null;
	// Control View
	private TextView ct_txt_TongChi = null;
	private TextView ct_txt_TongThu = null;
	private TextView ct_txt_TongThuChi = null;

	private Spinner ct_sp_tenDanhMuc = null;
	private Spinner ct_hd_spTaiKhoan = null;
	private WebView webView = null;
	private DatePickerDialog.OnDateSetListener date = null;
	private ChiTieu chiTieuCurrent = null;
	// tab tai khoan
	ListView ct_tk_lv_thuChi = null;
	// tab du lieu
	ListView ct_dl_lv_thuChi = null;
	// Adapter
	private Calendar calendar = Calendar.getInstance();
	// Làm việc với Database
	private List<ChiTieu> hdLstChiTieu = new ArrayList<ChiTieu>();
	private List<DanhMuc> lstDanhMuc = new ArrayList<DanhMuc>();
	private List<TaiKhoan> lstTaiKhoan = new ArrayList<TaiKhoan>();
	private List<DanhMuc> lstDanhMucTab = new ArrayList<DanhMuc>();
	private ArrayAdapter<DanhMuc> danhMucAdapter = null;
	private ChitieuAdapter chiTieuAdapter;
	private TaiKhoanAdapter taiKhoanAdapter;
	private TaiKhoanSpinnerAdapter hdTaiKhoanSpinnerAdapter = null;
	private DanhMucAdapter2 danhMucAdapter2;
	private DanhMucDao danhMucDao;
	private ChiTieuDao chiTieuDao;
	private HinhAnhDao hinhAnhDao;
	private TaiKhoanDao taiKhoanDao;
	private SuKienDao suKienDao;
	private TienDao tienDao;
	private TaiKhoan taiKhoanCurrent;
	private DanhMuc item;
	private DanhMuc item2;
	private DanhMuc danhMuc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_taikhoan_add);
		Bundle extras = getIntent().getExtras();
		initComponent();

		initComponent();
		// xu ly du lieu tu form Main sang du lieu
		if (extras.getSerializable("taiKhoan") != null) {
			taiKhoanCurrent = (TaiKhoan) extras.getSerializable("taiKhoan");
		} else {
			taiKhoanCurrent = taiKhoanDao.createEntity();
			taiKhoanCurrent.setTaiKhoanTen("");
			taiKhoanCurrent.setTaiKhoanNgayTao(DateUtils.getddmmYYYY());
		}
		Log.i("taiKhoan", taiKhoanCurrent.getTaiKhoanTen());
		setTaiKhoanFromControl();
		// changeByRadio();

		// TextView ct_add_bt_themgiaodich = (TextView)
		// findViewById(R.id.ct_add_bt_themgiaodich);
		TextView ct_add_bt_x = (TextView) findViewById(R.id.ct_add_bt_x);
		TextView ct_add_bt_luu = (TextView) findViewById(R.id.ct_add_bt_luu);
		TextView ct_add_bt_del = (TextView) findViewById(R.id.ct_add_bt_del);
		ct_add_bt_luu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i("", "ct_add_bt_luu");
				taiKhoanCurrent = getTaiKhoanFromControl();
				Log.i("taiKhoan_click Save ", taiKhoanCurrent.getTaiKhoanTen());
				taiKhoanDao.insertOrUpdate(getTaiKhoanFromControl());
				backMainMenu();
			}

		});
		ct_add_bt_x.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i("", "ct_add_bt_x");
				backMainMenu();
			}
		});

		ct_add_bt_del.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i("", "ct_add_bt_del");
				// Lay toan bo danh muc trong chi tieu thuoc Tai Khoan
				List<ChiTieu> list = chiTieuDao.findBy(ChiTieu.TaiKhoanId,
						taiKhoanCurrent.getTaiKhoanId());
				for (ChiTieu item : list)
					chiTieuDao.delete(item);
				// Lay toan bo danh muc trong chi tieu thuoc Tai Khoan
				taiKhoanDao.delete(taiKhoanCurrent);

				backMainMenu();
			}
		});
		// loadTabs();
		// changeByRadio();
		// doCreateFakeDataChiTieu();
		// // loadTroGiupWebView();
		// // doCreateFakeDataChiTieu2();
		// displayListView();
		date = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				calendar.set(Calendar.YEAR, year);
				calendar.set(Calendar.MONTH, monthOfYear);
				calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				updateLabel();
			}
		};
		ct_tk_txt_ngayTao.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new DatePickerDialog(TaiKhoanMain.this, date, calendar
						.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
						calendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});

	}

	/**
	 * Back to Main Menu
	 */
	protected void backMainMenu() {
		Intent intent = new Intent(this, MainActivity03.class);
		startActivity(intent);
	}

	/**
	 * Return {@link TaiKhoan} object from controls
	 * 
	 * @return {@link TaiKhoan} object
	 */
	protected TaiKhoan getTaiKhoanFromControl() {
		taiKhoanCurrent.setTaiKhoanTen(ct_tk_txt_taiKhoanTen.getText()
				.toString());
		taiKhoanCurrent.setTaiKhoanNgayTao(ct_tk_txt_ngayTao.getText()
				.toString());
		taiKhoanCurrent.setTaiKhoanGhiChu(ct_tk_ct_GhiChu.getText().toString());
		return taiKhoanCurrent;
	}

	/**
	 * Set {@link ChiTieu} object for the controls
	 * 
	 * @return {@link ChiTieu} object
	 */
	int i = 1;

	protected TaiKhoan setTaiKhoanFromControl() {
		if (taiKhoanCurrent == null)
			taiKhoanCurrent = taiKhoanDao.getEntityClass();
		// ct_txt_chiTieuId = (TextView) findViewById(R.id.ct_txt_chiTieuId);
		// ct_txt_chiTieuId.setText(taiKhoanCurrent.getTaiKhoanId());
		ct_tk_txt_taiKhoanTen.setText(taiKhoanCurrent.getTaiKhoanTen());
		ct_tk_txt_ngayTao.setText(taiKhoanCurrent.getTaiKhoanNgayTao());
		ct_tk_ct_GhiChu.setText(taiKhoanCurrent.getTaiKhoanGhiChu());

		return taiKhoanCurrent;
	}

	public void btnClickBtDel(View view) {
		chiTieuDao.delete(chiTieuCurrent);
		displayListView();
	}

	private void updateLabel() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		ct_tk_txt_ngayTao.setText(sdf.format(calendar.getTime()));
	}

	public void ct_rd_chiOnClicked(View view) {
		changeByRadio();
	}

	public void ct_rd_thuOnClicked(View view) {
		changeByRadio();
	}

	/**
	 * initialization component
	 */
	public void initComponent() {
		Log.i("initComponent", "initComponent start..");
		// -------------initialization component source-------------
		ct_sp_tenDanhMuc = (Spinner) findViewById(R.id.ct_sp_tenDanhMuc);
		ct_hd_spTaiKhoan = (Spinner) findViewById(R.id.ct_hd_spTaiKhoan);
		ct_txt_chiTieuId = (TextView) findViewById(R.id.ct_txt_chiTieuId);
		ct_rd_thu = (RadioButton) findViewById(R.id.ct_rd_thu);
		ct_rd_chi = (RadioButton) findViewById(R.id.ct_rd_chi);
		ct_lv_thuChi = (ListView) findViewById(R.id.ct_lv_thuChi);
		ct_tk_txt_taiKhoanTen = (TextView) findViewById(R.id.ct_tk_txt_taiKhoanTen);
		ct_txt_tienChiTieu = (TextView) findViewById(R.id.ct_txt_tienChiTieu);
		ct_tk_ct_GhiChu = (EditText) findViewById(R.id.ct_tk_ct_GhiChu);
		// Hien thi ket qua tong thu, chi, tong chu chi-----------------
		ct_txt_TongThu = (TextView) findViewById(R.id.ct_txt_TongThu);
		ct_txt_TongChi = (TextView) findViewById(R.id.ct_txt_TongChi);
		ct_txt_TongThuChi = (TextView) findViewById(R.id.ct_txt_TongThuChi);
		// ============================Tab tai khoan--------------------
		ct_tk_lv_thuChi = (ListView) findViewById(R.id.ct_tk_lv_thuChi);
		// ============================Tab du lieu----------------------
		ct_dl_lv_thuChi = (ListView) findViewById(R.id.ct_dl_lv_thuChi);
		// ------
		webView = (WebView) findViewById(R.id.ct_tg_wv_trogiup);
		ct_tk_txt_ngayTao = (EditText) findViewById(R.id.ct_tk_txt_ngayTao);
		// ---------------------------init dao--------------------------
		danhMucDao = new DanhMucDao(this);
		chiTieuDao = new ChiTieuDao(this);
		hinhAnhDao = new HinhAnhDao(this);
		taiKhoanDao = new TaiKhoanDao(this);
		suKienDao = new SuKienDao(this);
		tienDao = new TienDao(this);

		List<ChiTieu> del = chiTieuDao.findBy(ChiTieu.DanhMucId, 0);
		for (ChiTieu chiTieu : del) {
			chiTieuDao.delete(chiTieu);
		}
		lstDanhMuc = danhMucDao.findAll();
		hdLstChiTieu = chiTieuDao.findAll();
		lstDanhMucTab = danhMucDao.findAll();
		lstTaiKhoan = taiKhoanDao.findAll();
		// -----------------------init adapter--------------------------

		Log.i("initComponent", "initComponent end..");
	}

	/**
	 * Su kien thay doi theo radio button thu chi
	 */
	private void changeByRadio() {
		Log.i("changeByRadio", "changeByRadio start..");
		lstDanhMuc.removeAll(lstDanhMuc);
		if (ct_rd_thu.isChecked()) {
			lstDanhMuc = danhMucDao.findBy(DanhMuc.DanhMucTrangThai, 0);
			// ct_txt_tenChiTieu.setText(getStringRes(R.string.ct_ct_thu) + " "
			// + hdLstChiTieu.size());
			ct_tk_txt_taiKhoanTen.setText(chiTieuCurrent.getChiTieuTen());
		} else {
			lstDanhMuc = danhMucDao.findBy(DanhMuc.DanhMucTrangThai, 1);
			// ct_txt_tenChiTieu.setText(getStringRes(R.string.ct_ct_chi) + " "
			// + hdLstChiTieu.size());
			ct_tk_txt_taiKhoanTen.setText(chiTieuCurrent.getChiTieuTen());
		}
		// ct_txt_tienChiTieu
		// .setText(getStringRes(R.string.ct_ct_tienChiTieu_50000));
		danhMucAdapter = new com.aptech.chitieu2.adapter.DanhMucAdapter(
				TaiKhoanMain.this, layout.simple_spinner_item,
				(ArrayList<DanhMuc>) lstDanhMuc);
		// danhMucAdapter = new DanhMucAdapter(this, R.layout.item_sp_danhmuc,
		// (ArrayList<DanhMuc>) lstDanhMuc);
		ct_sp_tenDanhMuc.setAdapter(danhMucAdapter);
		Log.i("changeByRadio", "changeByRadio end..");
	}

	public void mnActionSettingsClick(View view) {
		Toast.makeText(TaiKhoanMain.this, "mnActionSettingsClick",
				Toast.LENGTH_SHORT).show();
	}

	/**
	 * Su kien click nut them
	 * 
	 * @param v
	 */
	public void btnClickBtThem(View v) {
		int status = 1;
		// DanhMuc ctDm = null;
		// if (ct_rd_thu.isChecked())
		// status = 0;
		// ctDm = danhMucAdapter.getItem(ct_sp_tenDanhMuc
		// .getSelectedItemPosition());
		// ctDm.setDanhMucTrangThai(status);
		// chiTieuCurrent.setDanhMucId(ctDm.getId());
		// chiTieuCurrent.setChiTieuTen(ct_txt_tenChiTieu.getText().toString());
		// chiTieuCurrent.setChiTieuSoTien(Double.parseDouble(ct_txt_tienChiTieu
		// .getText().toString()));
		// chiTieuCurrent.setChiTieuNgayTao(ct_tk_txt_ngayTao.getText().toString());
		// chiTieuDao.insertOrUpdate(chiTieuCurrent);
		// chiTieuCurrent = null;

		displayListView();
	}

	/**
	 * Hien thi displayListView
	 */
	public void displayListView() {
		hdLstChiTieu.removeAll(hdLstChiTieu);
		// Collections.sort(listThuChi);
		hdLstChiTieu = chiTieuDao.findAll();
		tinhtong();
		// Load danh sach thu chi
		// ct_lv_thuChi=(ListView) findViewById(R.id.ct_lv_thuChi);
		chiTieuAdapter = new ChitieuAdapter(TaiKhoanMain.this,
				R.layout.item_lv_thuchi, (ArrayList<ChiTieu>) hdLstChiTieu);
		ct_lv_thuChi.setAdapter(chiTieuAdapter);
		openChart();
		// =============== hien thi tab danhMucAdapter2
		danhMucAdapter2 = new DanhMucAdapter2(TaiKhoanMain.this,
				R.layout.item_lv_danhmuc, (ArrayList<DanhMuc>) lstDanhMucTab);
		ct_dl_lv_thuChi.setAdapter(danhMucAdapter2);
		// =============== hien thi tab taiKhoanAdapter
		taiKhoanAdapter = new TaiKhoanAdapter(TaiKhoanMain.this,
				R.layout.item_lv_taikhoan, (ArrayList<TaiKhoan>) lstTaiKhoan);
		ct_tk_lv_thuChi.setAdapter(taiKhoanAdapter);
		hdTaiKhoanSpinnerAdapter = new TaiKhoanSpinnerAdapter(
				TaiKhoanMain.this, android.R.layout.simple_spinner_item,
				(ArrayList<TaiKhoan>) lstTaiKhoan);
		ct_hd_spTaiKhoan.setAdapter(hdTaiKhoanSpinnerAdapter);

		openChart();
	}

	/**
	 * Tinh tong tien
	 */
	private void tinhtong() {
		tongChi = tongThu = tongThuChi = 0f;
		if (hdLstChiTieu != null && hdLstChiTieu.size() > 0) {
			for (ChiTieu item : hdLstChiTieu) {
				if (item.getDanhMucId() != null && item.getDanhMucId() != 0) {
					Log.e("getDanhMucId", item.getDanhMucId() + "");
					DanhMuc dm = danhMucDao.findByPK(item.getDanhMucId());
					if (dm.getDanhMucTrangThai() == 0)
						// if(item.getDanhMucId()==0)
						tongThu += item.getChiTieuSoTien();
					else
						tongChi += item.getChiTieuSoTien();
					tongThuChi = tongThu - tongChi;
				}
			}
		}
		// Hien thi ket qua tong thu, chi, tong chu chi
		ct_txt_TongThu.setText(getStringRes(R.string.ct_ct_thu) + ": "
				+ float2String2f(tongThu));
		ct_txt_TongChi.setText(getStringRes(R.string.ct_ct_chi) + ": "
				+ float2String2f(tongChi));
		ct_txt_TongThuChi.setText(getStringRes(R.string.ct_ct_thu_chi) + ": "
				+ float2String2f(tongThuChi));

	}

	/**
	 * Hien thi tro giup webview
	 */
	@SuppressLint("SetJavaScriptEnabled")
	private void loadTroGiupWebView() {
		// webView.loadUrl("http://developer.android.com");
		class JsObject {
			@JavascriptInterface
			public String toString() {
				return "injectedObject";
			}
		}
		webView.addJavascriptInterface(new JsObject(), "injectedObject");
		webView.loadUrl("http://developer.android.com");
		// webView.loadUrl("http://weavesilk.com");
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new MyWebViewCient());
	}

	public String getStringRes(int id) {
		return getApplication().getResources().getString(id);
	}

	public int getColorRes(int id) {
		return getApplication().getResources().getColor(id);
	}

	public Drawable getIconRes(int id) {
		return getApplication().getResources().getDrawable(id);
	}

	/**
	 * Lay gia tri Drawable tu System
	 * 
	 * @param id
	 *            String system
	 * @return String tu he thong
	 */
	public String getStringSys(int id) {
		return Resources.getSystem().getString(id);
	}

	/**
	 * Lay gia tri Drawable tu System
	 * 
	 * @param id
	 *            drawable id
	 * @return Drawable
	 */
	public Drawable getDrawalbeSys(int id) {
		return Resources.getSystem().getDrawable(id);
	}

	/**
	 * Lay gia tri Drawable tu Resource
	 * 
	 * @param id
	 *            drawable id
	 * @return Drawable
	 */
	public Drawable getDrawalbeRs(int id) {
		return Resources.getSystem().getDrawable(id);
	}

	/**
	 * Lay gia tri mang tu Resource
	 * 
	 * @param id
	 *            array id
	 * @return String[]
	 */
	public String[] getStringArrayRes(int id) {
		return getApplication().getResources().getStringArray(id);
	}

	/**
	 * Tao giu lieu test
	 */
	private void doCreateFakeDataChiTieu() {
		hdLstChiTieu = chiTieuDao.findAll();
	}

	// Click to del
	public void ct_lv_col_iconDelThuChiOnClick(View view) {
		ChiTieu item = chiTieuAdapter.getItem(ct_lv_thuChi
				.getSelectedItemPosition());
		Toast.makeText(TaiKhoanMain.this, "Del" + item.toString(),
				Toast.LENGTH_SHORT).show();
		chiTieuDao.delete(item);
		displayListView();
	}

	/*
	 * Load cac tab vao trong tabhost
	 */
	public void loadTabs() {
		Log.i("LoadTabs", "LoadTabs starting...");
		// Lấy Tabhost id ra trước (cái này của built - in android
		final TabHost tabhost = (TabHost) findViewById(android.R.id.tabhost);
		// gọi lệnh setup
		tabhost.setup();
		TabHost.TabSpec spec;
		// Tạo tab1
		spec = tabhost.newTabSpec("t1");
		spec.setContent(R.id.tab1);
		TextView v1 = new TextView(this);
		v1.setText(getStringRes(R.string.ct_ct_thu_chi));
		spec.setIndicator(getStringRes(R.string.ct_ct_thu_chi));
		tabhost.addTab(spec);
		// Tạo tab2
		spec = tabhost.newTabSpec("t2");
		spec.setContent(R.id.tab2);
		spec.setIndicator(getStringRes(R.string.ct_tk_taikhoan));
		tabhost.addTab(spec);
		// Tạo tab3
		spec = tabhost.newTabSpec("t3");
		spec.setContent(R.id.tab3);
		spec.setIndicator(getStringRes(R.string.ct_dl_dulieu));
		tabhost.addTab(spec);
		// Tạo tab3
		spec = tabhost.newTabSpec("t4");
		spec.setContent(R.id.tab4);
		spec.setIndicator(getStringRes(R.string.ct_pt_phantich));
		tabhost.addTab(spec);
		// Tạo tab3
		spec = tabhost.newTabSpec("t5");
		spec.setContent(R.id.tab5);
		spec.setIndicator(getStringRes(R.string.ct_tg_trogiup));
		tabhost.addTab(spec);
		// Thiết lập tab mặc định được chọn ban đầu là tab 0
		for (int i = 0; i < tabhost.getTabWidget().getChildCount(); i++) {
			tabhost.getTabWidget().getChildAt(i)
					.setBackgroundColor(getColorRes(R.color.WhiteSmoke));
			tabhost.getTabWidget().getChildAt(i).getLayoutParams().height = 60;
		}
		tabhost.setCurrentTab(0);
		tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab())
				.setBackgroundColor(getColorRes(R.color.LightBlue));
		// Ví dụ tab1 chưa nhập thông tin xong mà lại qua tab 2 thì báo...
		tabhost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			public void onTabChanged(String arg0) {
				for (int i = 0; i < tabhost.getTabWidget().getChildCount(); i++) {
					tabhost.getTabWidget().getChildAt(i).getLayoutParams().height = 60;
					tabhost.getTabWidget()
							.getChildAt(i)
							.setBackgroundColor(getColorRes(R.color.WhiteSmoke));
				}
				tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab())
						.setBackgroundColor(getColorRes(R.color.LightBlue));
			}
		});
		Log.i("LoadTabs", "LoadTabs end...");
	}

	class MyWebViewCient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// if(Uri.parse(url).getHost().equals("weavesilk.com"))
			if (Uri.parse(url).getHost().equals("developer.android.com"))
				return false;
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);
			return true;
		}
	}

	// ==========================Draw pie chart========================
	private View mPieChart;

	protected void onCreate2(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button btnPieChart = (Button) findViewById(R.id.btnPieChart);
		OnClickListener clickEvent = new OnClickListener() {
			@Override
			public void onClick(View v) {
				openChart();
			}
		};
		btnPieChart.setOnClickListener(clickEvent);
	}

	private void openChart() {
		String[] categories = new String[] { getStringRes(R.string.ct_ct_thu),
				getStringRes(R.string.ct_ct_chi),
				getStringRes(R.string.ct_ct_thu_chi) };
		// double[] proportion={0.4,0.3,0.3};
		// double[] proportion={tongThu,tongChi};
		float perscentThu = tongThu / tongThuChi * 100;
		float perscentChi = tongChi / tongThuChi * 100;
		// double[] proportion={perscentThu,perscentChi};
		double[] proportion = { tongThu, tongChi };
		int[] color = { Color.BLUE, Color.RED, Color.YELLOW };
		CategorySeries expenseSeries = new CategorySeries(
				getStringRes(R.string.ct_ct_thu_chi));
		// Them ten va gia tri cho tung khoan chi
		for (int i = 0; i < proportion.length; i++) {
			expenseSeries.add(categories[i], proportion[i]);
		}
		DefaultRenderer defaultRenderer = new DefaultRenderer();
		for (int i = 0; i < proportion.length; i++) {
			SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
			seriesRenderer.setColor(color[i]);
			seriesRenderer.setDisplayChartValues(true);
			seriesRenderer.setChartValuesTextSize(40);
			// Them mau cho background
			defaultRenderer.setBackgroundColor(Color.GRAY);
			defaultRenderer.setApplyBackgroundColor(true);
			// Tao ra tung slice cua pie chart
			defaultRenderer.addSeriesRenderer(seriesRenderer);
		}
		defaultRenderer.setChartTitle(getStringRes(R.string.ct_ct_thu_chi));
		defaultRenderer.setChartTitleTextSize(60);
		defaultRenderer.setLabelsTextSize(30);
		defaultRenderer.setLegendTextSize(30);
		defaultRenderer.setDisplayValues(true);
		defaultRenderer.setZoomButtonsVisible(false);
		LinearLayout chartContainer = (LinearLayout) findViewById(R.id.chart);
		// remove view before painting the chart
		chartContainer.removeAllViews();
		// Ve pie chart
		mPieChart = ChartFactory.getPieChartView(getBaseContext(),
				expenseSeries, defaultRenderer);
		// Add view to the linearlayout
		chartContainer.addView(mPieChart);
	}

}
