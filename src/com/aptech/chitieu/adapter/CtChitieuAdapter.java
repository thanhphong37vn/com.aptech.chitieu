package com.aptech.chitieu.adapter;
import java.util.ArrayList;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.aptech.chitieu.R;
import com.aptech.chitieu.entity.CtChiTieu;
import com.aptech.chitieu.util.DateUtils;
/**
 * @author HoanPV
 * @version Jul 2, 2015 : 5:00:58 PM
 */
public class CtChitieuAdapter extends
    ArrayAdapter<CtChiTieu> {
	Context	             mContext;
	ArrayList<CtChiTieu>	mLstChiTieu	=new ArrayList<CtChiTieu>();
	public CtChitieuAdapter(Context context, int resource,
	    ArrayList<CtChiTieu> objects) {
		super(context,resource,objects);
		this.mContext=context;
		this.mLstChiTieu=objects;
	}
	@Override
	public View getView(int position, View convertView,
	    ViewGroup parent) {
		View rowView=convertView;
		ViewHolder viewHolder=null;
		if(rowView==null) {
			LayoutInflater inflate=(LayoutInflater) mContext
			    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView=inflate.inflate(R.layout.item_lv_thuchi,null);
			viewHolder=new ViewHolder();
			viewHolder.ct_lv_col_tenChiTieu=(TextView) rowView
			    .findViewById(R.id.ct_lv_col_tenChiTieu);
			viewHolder.ct_lv_col_ngayTao=(TextView) rowView
			    .findViewById(R.id.ct_lv_col_ngayTao);
			viewHolder.ct_lv_col_iconThuChi=(ImageView) rowView
			    .findViewById(R.id.ct_lv_col_iconThuChi);
			// viewHolder.ct_lv_col_iconDelThuChi=(ImageButton) rowView
			// .findViewById(R.id.ct_lv_col_iconDelThuChi);
			viewHolder.ct_lv_col_TienChiTieu=(TextView) rowView
			    .findViewById(R.id.ct_lv_col_TienChiTieu);
			rowView.setTag(viewHolder);
		} else {
			viewHolder=(ViewHolder) convertView.getTag();
		}
		CtChiTieu chiTieu=mLstChiTieu.get(position);
		viewHolder.ct_lv_col_tenChiTieu.setText(chiTieu
		    .getTenChiTieu());
		if(chiTieu.getNgayTao()!=null)
		  viewHolder.ct_lv_col_ngayTao.setText(DateUtils
		      .yyyyMMdd(chiTieu.getNgayTao().getTime()));
		Log.i("getTienChiTieu",chiTieu.getTienChiTieu()+"");
		viewHolder.ct_lv_col_TienChiTieu.setText(String
		    .valueOf(chiTieu.getTienChiTieu()));
		if(chiTieu.getIdDanhMuc().getTrangThai()==0)
			viewHolder.ct_lv_col_iconThuChi
			    .setImageResource(R.drawable.ic_hd_add);
		else viewHolder.ct_lv_col_iconThuChi
		    .setImageResource(R.drawable.ic_hd_sub);
		return rowView;
	}
	static class ViewHolder {
		TextView		ct_lv_col_tenChiTieu;
		TextView		ct_lv_col_ngayTao;
		TextView		ct_lv_col_TienChiTieu;
		ImageView		ct_lv_col_iconThuChi;
		ImageButton	ct_lv_col_iconDelThuChi;
	}
}
