package com.aptech.chitieu.adapter;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.aptech.chitieu.R;
import com.aptech.chitieu.entity.CtDanhMuc;
/**
 * Title : Adapter Layer<br/>
 * Description : Ct DanhMuc Adapter class <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Jul 18, 2015 10:13:10 AM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Jul 18, 2015 10:13:10 AM
 */
public class CtDanhMucAdapter extends
    ArrayAdapter<CtDanhMuc> {
	Context	             mContext;
	ArrayList<CtDanhMuc>	mLstDanhMuc	=new ArrayList<CtDanhMuc>();
	public CtDanhMucAdapter(Context context, int resource,
	    ArrayList<CtDanhMuc> objects) {
		super(context,resource,objects);
		this.mContext=context;
		this.mLstDanhMuc=objects;
	}
	@Override
	public View getView(int position, View convertView,
	    ViewGroup parent) {
		View rowView=convertView;
		ViewHolder viewHolder=null;
		if(rowView==null) {
			LayoutInflater inflate=(LayoutInflater) mContext
			    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView=inflate
			    .inflate(R.layout.item_sp_danhmuc,null);
			viewHolder=new ViewHolder();
			viewHolder.sp_col_tv_tenDanhMuc=(TextView) rowView
			    .findViewById(R.id.sp_col_tv_tenDanhMuc);
			// viewHolder.sp_col_iconDanhMuc=(ImageView) rowView
			// .findViewById(R.id.sp_col_iconDanhMuc);
			rowView.setTag(viewHolder);
		} else {
			viewHolder=(ViewHolder) convertView.getTag();
		}
		CtDanhMuc ctDanhMuc=mLstDanhMuc.get(position);
		viewHolder.sp_col_tv_tenDanhMuc.setText(ctDanhMuc
		    .getTenDanhMuc());
		// if(ctDanhMuc.getTrangThai()==0)
		// viewHolder.sp_col_iconDanhMuc
		// .setImageResource(R.drawable.ic_hd_add);
		// else viewHolder.sp_col_iconDanhMuc
		// .setImageResource(R.drawable.ic_hd_sub);
		return rowView;
	}
	static class ViewHolder {
		TextView	sp_col_tv_tenDanhMuc;
		ImageView	sp_col_iconDanhMuc;
	}
}
