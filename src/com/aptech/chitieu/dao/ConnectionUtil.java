package com.aptech.chitieu.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
/**
 * Title : <br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 6, 2015 8:52:41 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 6, 2015 8:52:41 PM
 */
public class ConnectionUtil {
	private static Connection	conn	      =null;
	private static String	    databaseUrl	="jdbc:sqlite:ChiTieuDatabase.sqlite";
	public static Connection getConnection() {
		if(conn==null) {
			try {
				String sDriverName="org.sqlite.JDBC";
				Class.forName(sDriverName);
				conn=DriverManager.getConnection(databaseUrl);
				return conn;
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return conn;
	}
	private static ConnectionSource	connectSqlLite	=null;
	public static ConnectionSource getConnectionSQLLite() {
		if(connectSqlLite==null) {
			try {
				String sDriverName="org.sqlite.JDBC";
				Class.forName(sDriverName);
				connectSqlLite=new JdbcConnectionSource(databaseUrl);
				return connectSqlLite;
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return connectSqlLite;
	}
}
