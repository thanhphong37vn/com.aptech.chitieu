package com.aptech.chitieu.dao;
import java.util.ArrayList;
import java.util.List;
import com.aptech.chitieu.entity.CtChiTieu;
import com.aptech.chitieu.entity.CtDanhMuc;
import com.aptech.chitieu.util.DateUtils;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
/**
 * Xử lý dữ liệu với bảng ghichu thông qua đối tượng SQLiteDatabase
 */
/**
 * Title : Data Access Object Layer<br/>
 * Description : Has methods create, delete, getAll, findById<br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Jul 21, 2015 10:59:49 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Jul 21, 2015 10:59:49 PM
 */
public class CtChiTieuDataSource {
	// Database fields
	private SQLiteDatabase	database;
	private DatabaseHelperCTChitieu	dbHelper;
	// private CtDanhMucDataSource ctDanhMucDataSource;
	private String[]	     allColsCtChiTieu	={
	    DatabaseHelperCTChitieu.CtChiTieu_idChiTieu,
	    DatabaseHelperCTChitieu.CtChiTieu_tenChiTieu,
	    DatabaseHelperCTChitieu.CtChiTieu_tienChiTieu,
	    DatabaseHelperCTChitieu.CtChiTieu_trangThai,
	    DatabaseHelperCTChitieu.CtChiTieu_ghiChu,
	    DatabaseHelperCTChitieu.CtChiTieu_ngayTao,
	    DatabaseHelperCTChitieu.CtChiTieu_ngaySua	  };
	// Hàm contructor kiêm nhiệm vụ khởi tạo Database Helper
	public CtChiTieuDataSource(Context context) {
		dbHelper=new DatabaseHelperCTChitieu(context);
		open();
		// ctDanhMucDataSource=new CtDanhMucDataSource(context);
	}
	// Hàm xin phép mở database để đọc ghi dữ liệu
	public void open() throws SQLException {
		database=dbHelper.getWritableDatabase();
	}
	// Hàm đóng database lại kết thúc việc đọc ghi dữ liệu
	public void close() {
		dbHelper.close();
	}
	/** Thêm một bản ghi */
	public CtChiTieu create(CtChiTieu ctChiTieu) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(DatabaseHelper.CtChiTieu_idChiTieu,
		// ctChiTieu.getIdChiTieu());
		values.put(DatabaseHelperCTChitieu.CtChiTieu_tenChiTieu,
		    ctChiTieu.getTenChiTieu());
		values.put(DatabaseHelperCTChitieu.CtChiTieu_idDanhMuc,ctChiTieu
		    .getIdDanhMuc().getIdDanhMuc());
		values.put(DatabaseHelperCTChitieu.CtChiTieu_tienChiTieu,
		    ctChiTieu.getTienChiTieu());
		// values.put(DatabaseHelper.CtChiTieu_trangThai,ctChiTieu
		// .getIdDanhMuc().getTrangThai());
		values.put(DatabaseHelperCTChitieu.CtChiTieu_trangThai,
		    ctChiTieu.getTrangThai());
		values.put(DatabaseHelperCTChitieu.CtChiTieu_ghiChu,
		    ctChiTieu.getGhiChu());
		if(ctChiTieu.getNgayTao()!=null)
		  values.put(DatabaseHelperCTChitieu.CtChiTieu_ngayTao,ctChiTieu
		      .getNgayTao().toString());
		if(ctChiTieu.getNgaySua()!=null)
		  values.put(DatabaseHelperCTChitieu.CtChiTieu_ngaySua,ctChiTieu
		      .getNgaySua().toString());
		long insertId=database.insert(
		    DatabaseHelperCTChitieu.CtChiTieu_CtChiTieu,null,values);
		// // Trỏ tới database lấy dữ liệu vừa lưu - để kiểm tra xem dữ liệu ghi
		// // đúng chưa
		// // Truy vấn lấy bản ghi có ID = insertId
		// Cursor cursor=database.query(
		// DatabaseHelper.CtChiTieu_CtChiTieu,
		// allColsCtChiTieu,null,
		// DatabaseHelper.CtChiTieu_idChiTieu+" = "+insertId,
		// null,null,null,null);
		String q="SELECT ct.*, dm.*, dm.trangThai as dmTrangThai "
		    +"FROM CtChiTieu ct inner join CtDanhMuc  dm "
		    +"on ct.idDanhMuc = dm.idDanhMuc and idChiTieu = "
		    +insertId
		    +" and ct.trangThai<>3 order by ct.ngayTao ASC;";
		// Cursor cursor=database.query(
		// DatabaseHelper.CtChiTieu_CtChiTieu,
		// allColsCtChiTieu,null,null,null,null,
		// DatabaseHelper.CtChiTieu_ngayTao+" ASC");
		Cursor cursor=database.rawQuery(q,null);
		cursor.moveToFirst();
		CtChiTieu ctChiTieuResult=cursorToEntity(cursor);
		cursor.close();
		Log.e("create",ctChiTieuResult.toString());
		// Trả về đối tượng Comment
		return ctChiTieuResult;
	}
	/** Cập nhật một bản ghi */
	public void update(CtChiTieu ctChiTieu) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		values.put(DatabaseHelperCTChitieu.CtChiTieu_idChiTieu,
		    ctChiTieu.getIdChiTieu());
		values.put(DatabaseHelperCTChitieu.CtChiTieu_tenChiTieu,
		    ctChiTieu.getTenChiTieu());
		values.put(DatabaseHelperCTChitieu.CtChiTieu_trangThai,
		    ctChiTieu.getTrangThai());
		values.put(DatabaseHelperCTChitieu.CtChiTieu_ghiChu,
		    ctChiTieu.getGhiChu());
		values.put(DatabaseHelperCTChitieu.CtChiTieu_ngayTao,ctChiTieu
		    .getNgayTao().toString());
		values.put(DatabaseHelperCTChitieu.CtChiTieu_ngaySua,ctChiTieu
		    .getNgaySua().toString());
		// Điều kiện
		String whereClause=DatabaseHelperCTChitieu.CtChiTieu_idChiTieu
		    +" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String
		    .valueOf(ctChiTieu.getIdChiTieu())};
		// Thực hiện câu lệnh
		database.update(DatabaseHelperCTChitieu.CtChiTieu_CtChiTieu,
		    values,whereClause,whereArgs);
		long id=ctChiTieu.getIdChiTieu();
		Log.e("CommentsDataSource","Comment update with id: "
		    +id);
	}
	// Xóa một bản ghi
	public void delete(CtChiTieu ctChiTieu) {
		long id=ctChiTieu.getIdChiTieu();
		Log.e("CommentsDataSource","Comment deleted with id: "
		    +id);
		database.delete(DatabaseHelperCTChitieu.CtChiTieu_CtChiTieu,
		    DatabaseHelperCTChitieu.CtChiTieu_idChiTieu+" = "+id,null);
	}
	// Lấy tất cả dữ liệu
	public List<CtChiTieu> getAll3() {
		List<CtChiTieu> lstCtChiTieu=new ArrayList<CtChiTieu>();
		// Điều kiện
		// String whereClause=DatabaseHelper.CtChiTieu_trangThai
		// +" != ?";
		// Tham số cho điều kiện
		// String[] whereArgs=new String[]{String.valueOf(" ")};
		Cursor cursor=database.query(
		    DatabaseHelperCTChitieu.CtChiTieu_CtChiTieu,
		    allColsCtChiTieu,null,null,null,null,
		    DatabaseHelperCTChitieu.CtChiTieu_ngayTao+" ASC");
		Log.e("",cursor.toString());
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			CtChiTieu CtChiTieu=cursorToEntity(cursor);
			lstCtChiTieu.add(CtChiTieu);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtChiTieu;
	}// Lấy tất cả dữ liệu
	public List<CtChiTieu> getAll() {
		List<CtChiTieu> lstCtChiTieu=new ArrayList<CtChiTieu>();
		Log.e("Dong connection ?",database.isOpen()+"");
		String q="SELECT ct.*, dm.*, dm.trangThai as dmTrangThai "
		    +"FROM CtChiTieu ct inner join CtDanhMuc  dm "
		    +"on ct.idDanhMuc = dm.idDanhMuc and ct.trangThai<>3 order by ct.ngayTao ASC;";
		// Cursor cursor=database.query(
		// DatabaseHelper.CtChiTieu_CtChiTieu,
		// allColsCtChiTieu,null,null,null,null,
		// DatabaseHelper.CtChiTieu_ngayTao+" ASC");
		Cursor cursor=database.rawQuery(q,null);
		Log.e("getAll",String.valueOf(cursor.getCount()));
		if(cursor.moveToFirst()) {
			while(!cursor.isAfterLast()) {
				CtChiTieu CtChiTieu=cursorToEntity(cursor);
				lstCtChiTieu.add(CtChiTieu);
				cursor.moveToNext();
			}
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtChiTieu;
	}
	// Lấy tất cả dữ liệu
	public List<CtChiTieu> getAllByStatus(int status) {
		List<CtChiTieu> lstCtChiTieu=new ArrayList<CtChiTieu>();
		// Điều kiện
		String whereClause=DatabaseHelperCTChitieu.CtChiTieu_trangThai
		    +" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(status)};
		Cursor cursor=database.query(
		    DatabaseHelperCTChitieu.CtChiTieu_CtChiTieu,
		    allColsCtChiTieu,whereClause,whereArgs,null,null,
		    DatabaseHelperCTChitieu.CtChiTieu_tenChiTieu);
		Log.e("",cursor.toString());
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			CtChiTieu CtChiTieu=cursorToEntity(cursor);
			lstCtChiTieu.add(CtChiTieu);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtChiTieu;
	}
	private CtChiTieu cursorToEntity(Cursor cursor) {
		CtChiTieu ctChiTieu=new CtChiTieu();
		ctChiTieu.setIdChiTieu(cursor.getInt(cursor
		    .getColumnIndex("idChiTieu")));
		// int idDm=(int) cursor.getLong(cursor
		// .getColumnIndex("idDanhMuc"));
		// CtDanhMuc dm=ctDanhMucDataSource.findById(idDm);
		CtDanhMuc ctDM=new CtDanhMuc();
		ctDM.setIdDanhMuc(cursor.getInt(cursor
		    .getColumnIndex(DatabaseHelperCTChitieu.CtDanhMuc_idDanhMuc)));
		ctDM.setTenDanhMuc(cursor.getString(cursor
		    .getColumnIndex(DatabaseHelperCTChitieu.CtDanhMuc_tenDanhMuc)));
		ctDM.setTrangThai(cursor.getInt(cursor
		    .getColumnIndex("dmTrangThai")));
		// CtDanhMuc dm=ctDanhMucDataSource.findById();
		if(ctDM!=null) {
			ctChiTieu.setIdDanhMuc(ctDM);
			ctChiTieu.setTrangThai(ctDM.getTrangThai());
		}
		ctChiTieu
		    .setTenChiTieu(cursor.getString(cursor
		        .getColumnIndex(DatabaseHelperCTChitieu.CtChiTieu_tenChiTieu)));
		ctChiTieu
		    .setTienChiTieu(cursor.getFloat(cursor
		        .getColumnIndex(DatabaseHelperCTChitieu.CtChiTieu_tienChiTieu)));
		ctChiTieu.setGhiChu(cursor.getString(cursor
		    .getColumnIndex(DatabaseHelperCTChitieu.CtChiTieu_ghiChu)));
		ctChiTieu
		    .setNgayTao(DateUtils.parseDate(cursor.getString(cursor
		        .getColumnIndex(DatabaseHelperCTChitieu.CtChiTieu_ngayTao))));
		return ctChiTieu;
	}
}
