package com.aptech.chitieu.dao;
import java.util.ArrayList;
import java.util.List;
import com.aptech.chitieu.entity.CtDanhMuc;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
/**
 * Xử lý dữ liệu với bảng ghichu thông qua đối tượng SQLiteDatabase
 */
public class CtDanhMucDataSource {
	/** Database fields */
	private SQLiteDatabase	        database;
	private DatabaseHelperCTChitieu	dbHelper;
	private String[]	              allColsCtDanhMuc	={
	    DatabaseHelperCTChitieu.CtDanhMuc_idDanhMuc,
	    DatabaseHelperCTChitieu.CtDanhMuc_tenDanhMuc,
	    DatabaseHelperCTChitieu.CtDanhMuc_trangThai,
	    DatabaseHelperCTChitieu.CtDanhMuc_ghiChu,
	    DatabaseHelperCTChitieu.CtDanhMuc_ngayTao,
	    DatabaseHelperCTChitieu.CtDanhMuc_ngaySua	   };
	/** Hàm contructor kiêm nhiệm vụ khởi tạo Database Helper */
	public CtDanhMucDataSource(Context context) {
		dbHelper=new DatabaseHelperCTChitieu(context);
		open();
	}
	// Hàm xin phép mở database để đọc ghi dữ liệu
	public void open() throws SQLException {
		database=dbHelper.getWritableDatabase();
	}
	/** Hàm đóng database lại kết thúc việc đọc ghi dữ liệu */
	public void close() {
		dbHelper.close();
	}
	/** Thêm một bản ghi */
	public CtDanhMuc create(CtDanhMuc ctDanhMuc) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		values.put(DatabaseHelperCTChitieu.CtDanhMuc_idDanhMuc,
		    ctDanhMuc.getIdDanhMuc());
		values.put(
		    DatabaseHelperCTChitieu.CtDanhMuc_tenDanhMuc,
		    ctDanhMuc.getTenDanhMuc());
		values.put(DatabaseHelperCTChitieu.CtDanhMuc_trangThai,
		    ctDanhMuc.getTrangThai());
		values.put(DatabaseHelperCTChitieu.CtDanhMuc_ghiChu,
		    ctDanhMuc.getGhiChu());
		values.put(DatabaseHelperCTChitieu.CtDanhMuc_ngayTao,
		    ctDanhMuc.getNgayTao().toString());
		values.put(DatabaseHelperCTChitieu.CtDanhMuc_ngaySua,
		    ctDanhMuc.getNgaySua().toString());
		long insertId=database.insert(
		    DatabaseHelperCTChitieu.CtDanhMuc_CtDanhMuc,null,
		    values);
		// Trỏ tới database lấy dữ liệu vừa lưu - để kiểm tra xem dữ liệu ghi
		// đúng chưa
		// Truy vấn lấy bản ghi có ID = insertId
		Cursor cursor=database.query(
		    DatabaseHelperCTChitieu.CtDanhMuc_CtDanhMuc,null,
		    DatabaseHelperCTChitieu.CtChiTieu_idDanhMuc+" = "
		        +insertId,null,null,null,null);
		cursor.moveToFirst();
		CtDanhMuc ctDanhMucResult=cursorToEntity(cursor);
		cursor.close();
		// Trả về đối tượng Comment
		return ctDanhMucResult;
	}
	/** Cập nhật một bản ghi */
	public void update(CtDanhMuc ctDanhMuc) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		values.put(DatabaseHelperCTChitieu.CtDanhMuc_idDanhMuc,
		    ctDanhMuc.getIdDanhMuc());
		values.put(
		    DatabaseHelperCTChitieu.CtDanhMuc_tenDanhMuc,
		    ctDanhMuc.getTenDanhMuc());
		values.put(DatabaseHelperCTChitieu.CtDanhMuc_trangThai,
		    ctDanhMuc.getTrangThai());
		values.put(DatabaseHelperCTChitieu.CtDanhMuc_ghiChu,
		    ctDanhMuc.getGhiChu());
		values.put(DatabaseHelperCTChitieu.CtDanhMuc_ngayTao,
		    ctDanhMuc.getNgayTao().toString());
		values.put(DatabaseHelperCTChitieu.CtDanhMuc_ngaySua,
		    ctDanhMuc.getNgaySua().toString());
		// Điều kiện
		String whereClause=DatabaseHelperCTChitieu.CtDanhMuc_idDanhMuc
		    +" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String
		    .valueOf(ctDanhMuc.getIdDanhMuc())};
		// Thực hiện câu lệnh
		database.update(
		    DatabaseHelperCTChitieu.CtDanhMuc_CtDanhMuc,values,
		    whereClause,whereArgs);
		long id=ctDanhMuc.getIdDanhMuc();
		Log.e("CommentsDataSource","Comment update with id: "
		    +id);
	}
	// Xóa một bản ghi
	public void delete(CtDanhMuc ctDanhMuc) {
		long id=ctDanhMuc.getIdDanhMuc();
		Log.e("CommentsDataSource","Comment deleted with id: "
		    +id);
		database.delete(
		    DatabaseHelperCTChitieu.CtDanhMuc_CtDanhMuc,
		    DatabaseHelperCTChitieu.CtChiTieu_idDanhMuc+" = "
		        +id,null);
	}
	// Lấy tất cả dữ liệu
	public List<CtDanhMuc> getAll() {
		List<CtDanhMuc> lstCtDanhMuc=new ArrayList<CtDanhMuc>();
		Log.e("Dong connection ?",database.isOpen()+"");
		Cursor cursor=database.query(
		    DatabaseHelperCTChitieu.CtDanhMuc_CtDanhMuc,
		    allColsCtDanhMuc,null,null,null,null,null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			CtDanhMuc CtDanhMuc=cursorToEntity(cursor);
			lstCtDanhMuc.add(CtDanhMuc);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtDanhMuc;
	}
	// Lấy tất cả dữ liệu
	public List<CtDanhMuc> getAllByStatus(int status) {
		List<CtDanhMuc> lstCtDanhMuc=new ArrayList<CtDanhMuc>();
		// Điều kiện
		String whereClause=DatabaseHelperCTChitieu.CtDanhMuc_trangThai
		    +" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(status)};
		Cursor cursor=database.query(
		    DatabaseHelperCTChitieu.CtDanhMuc_CtDanhMuc,
		    allColsCtDanhMuc,whereClause,whereArgs,null,null,
		    DatabaseHelperCTChitieu.CtDanhMuc_tenDanhMuc);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			CtDanhMuc CtDanhMuc=cursorToEntity(cursor);
			lstCtDanhMuc.add(CtDanhMuc);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtDanhMuc;
	}
	// Lấy tất cả dữ liệu
	public CtDanhMuc findById(int idDanhMuc) {
		CtDanhMuc ctDanhMuc=null;
		// Điều kiện
		String whereClause=DatabaseHelperCTChitieu.CtDanhMuc_idDanhMuc
		    +" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String
		    .valueOf(idDanhMuc)};
		open();
		Cursor cursor=database.query(
		    DatabaseHelperCTChitieu.CtDanhMuc_CtDanhMuc,
		    allColsCtDanhMuc,whereClause,whereArgs,null,null,
		    DatabaseHelperCTChitieu.CtDanhMuc_tenDanhMuc);
		cursor.moveToFirst();
		if(!cursor.isAfterLast()) {
			ctDanhMuc=cursorToEntity(cursor);
		}
		if(ctDanhMuc==null) {
			ctDanhMuc=new com.aptech.chitieu.entity.CtDanhMuc(1,
			    null,1);
			return ctDanhMuc;
		}
		return ctDanhMuc;
	}
	private CtDanhMuc cursorToEntity(Cursor cursor) {
		CtDanhMuc ctDanhMuc=new CtDanhMuc();
		ctDanhMuc
		    .setIdDanhMuc(cursor.getInt(cursor
		        .getColumnIndex(DatabaseHelperCTChitieu.CtDanhMuc_idDanhMuc)));
		ctDanhMuc
		    .setTenDanhMuc(cursor.getString(cursor
		        .getColumnIndex(DatabaseHelperCTChitieu.CtDanhMuc_tenDanhMuc)));
		ctDanhMuc
		    .setTrangThai(cursor.getInt(cursor
		        .getColumnIndex(DatabaseHelperCTChitieu.CtDanhMuc_trangThai)));
		ctDanhMuc
		    .setGhiChu(cursor.getString(cursor
		        .getColumnIndex(DatabaseHelperCTChitieu.CtDanhMuc_ghiChu)));
		// ctDanhMuc.setNgayTao(new DaTE(CURSOR.GETSTRING(CURSOR
		// .GETCOLUMNINDEX(DATABASEHELPER.CTDANHMUC_NGAYTAO))));
		// CTDANHMUC.SETNGAYSUA(NEW DATE(CURSOR.GETSTRING(CURSOR
		// .GETCOLUMNINDEX(DatabaseHelper.CtDanhMuc_ngaySua))));
		return ctDanhMuc;
	}
}
