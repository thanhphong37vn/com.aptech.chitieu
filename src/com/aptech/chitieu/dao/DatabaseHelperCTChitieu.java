package com.aptech.chitieu.dao;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
// * @author VTMinh
 */
public class DatabaseHelperCTChitieu extends
    SQLiteOpenHelper {
	private static final String	DATABASE_NAME	            ="DBChiTieu.sqlite";	                                                    // Tên
																																																																	// CSDL
	private static final int	  DATABASE_VERSION	        =2;	                                                                    // Phiên
																																																																	// bản
																																																																	// CSDL
																																																																	// (version)
	// Khoi tao Bang CtDanhMuc
	private static final String	DATABASE_CREATE_CtDanhMuc	=" CREATE TABLE \"CtDanhMuc\" (\"idDanhMuc\" INTEGER PRIMARY KEY AUTOINCREMENT"
	                                                          +" NOT NULL UNIQUE , \"tenDanhMuc\" TEXT, \"trangThai\" INTEGER DEFAULT 2,"
	                                                          +"\"ghiChu\" TEXT, \"ngayTao\" DATETIME NOT NULL DEFAULT CURRENT_DATE,"
	                                                          +"\"ngaySua\" DATETIME);";
	public static final String	CtDanhMuc_CtDanhMuc	      ="CtDanhMuc";
	public static final String	CtDanhMuc_idDanhMuc	      ="idDanhMuc";
	public static final String	CtDanhMuc_tenDanhMuc	    ="tenDanhMuc";
	public static final String	CtDanhMuc_trangThai	      ="trangThai";
	public static final String	CtDanhMuc_ghiChu	        ="ghiChu";
	public static final String	CtDanhMuc_ngayTao	        ="ngayTao";
	public static final String	CtDanhMuc_ngaySua	        ="ngaySua";
	// Khoi tao Bang CtChiTieu
	private static final String	DATABASE_CREATE_CtChiTieu	="CREATE TABLE \"CtChiTieu\" (\"idChiTieu\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , "
	                                                          +"\"idDanhMuc\" INTEGER, \"tenChiTieu\" TEXT, \"tienChiTieu\" FLOAT, "
	                                                          +"\"trangThai\" INTEGER DEFAULT 0, \"ghiChu\" TEXT, "
	                                                          +"\"ngayTao\" DATETIME DEFAULT CURRENT_DATE, \"ngaySua\" DATETIME);";
	// --Coomment
	public static final String	CtChiTieu_CtChiTieu	      ="CtChiTieu";
	public static final String	CtChiTieu_idChiTieu	      ="idChiTieu";
	public static final String	CtChiTieu_idDanhMuc	      ="idDanhMuc";
	public static final String	CtChiTieu_tenChiTieu	    ="tenChiTieu";
	public static final String	CtChiTieu_tienChiTieu	    ="tienChiTieu";
	public static final String	CtChiTieu_trangThai	      ="trangThai";
	public static final String	CtChiTieu_ghiChu	        ="ghiChu";
	public static final String	CtChiTieu_ngayTao	        ="ngayTao";
	public static final String	CtChiTieu_ngaySua	        ="ngaySua";
	public DatabaseHelperCTChitieu(Context context) {
		// Khi khởi tạo đối tượng DatabaseHelper thì hàm super cần truyền vào
		// tên Database và số hiệu phiên bản database. Trong trường hợp số hiệu
		// phiên bản thay đổi thì hàm onUpgrade sẽ được gọi
		super(context,DATABASE_NAME,null,DATABASE_VERSION);
	}
	/**
	 * Khởi tạo database lần đầu tiên
	 */
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE_CtDanhMuc);
		database.execSQL(DATABASE_CREATE_CtChiTieu);
		for(int i=0;i<DatabaseConstants.arrInsert.length;i++)
			database.execSQL(DatabaseConstants.arrInsert[i]);
	}
	// Khi phiên bản ứng dụng nâng cấp, database thay đổi thêm trường hoặc bớt
	// trường dữ liệu thì cần viết lệnh cập nhật theo phiên bản ở đây
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion,
	    int newVersion) {
		Log.e(
		    DatabaseHelperCTChitieu.class.getName(),
		    "Cập nhật CSDL từ phiên bản "
		        +oldVersion
		        +" đến "
		        +newVersion
		        +", vui lòng kiểm tra cẩn thẩn để tránh tổn thất dữ liệu cũ");
		// Viết câu lệnh cập nhật database ở đây - ví dụ tạo 1 bảng mới
		// db.execSQL("CREATE TABLE \"tblUser\" (\"id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, \"name\" TEXT, \"type\" TEXT)");
	}
	static class DatabaseConstants {
		protected static String[]	arrInsert	=new String[]{
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Thu',0,'Thu');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Thu nhập tổng quan',0,'Thu nhập tổng quan');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('An sinh xã hội',0,'An sinh xã hội');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Lương',0,'Lương');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Quà đã nhận',0,'Quà đã nhận');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Tiền cho thuê đã nhận',0,'Tiền cho thuê đã nhận');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Thực phẩm',1,'Thực phẩm');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Chỗ ở',1,'Chỗ ở');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Nơi ở chính',1,'Nơi ở chính');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Chỗ ở khác',1,'Chỗ ở khác');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Hoạt động hộ gia đình',1,'Hoạt động hộ gia đình');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Đồ nội thất và trang thiết bị',1,'Đồ nội thất và trang thiết bị');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Quần áo và phụ kiện',1,'Quần áo và phụ kiện');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Vận tải',1,'Vận tải');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Chăm sóc sức khỏe',1,'Chăm sóc sức khỏe');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Chăm sóc cá nhân',1,'Chăm sóc cá nhân');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Vui chơi giải trí',1,'Vui chơi giải trí');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Giáo dục',1,'Giáo dục');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Tài liệu và ấn phẩm',1,'Tài liệu và ấn phẩm');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Thuốc lá và đồ uống',1,'Thuốc lá và đồ uống');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Trò chơi may rủi',1,'Trò chơi may rủi');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Chi linh tinh',1,'Chi linh tinh');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Thuế thu nhập',1,'Thuế thu nhập');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Bảo hiểm xã hội',1,'Bảo hiểm xã hội');",
		                                        "INSERT INTO CtDanhMuc(tenDanhMuc,trangThai,ghiChu)VALUES('Quà tặng tiền',1,'Quà tặng tiền');"};
	}
}
