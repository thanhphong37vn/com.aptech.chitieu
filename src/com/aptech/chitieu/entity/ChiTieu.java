package com.aptech.chitieu.entity;
import org.oni.webapp.dao.entity.base.EntityBase;
import org.oni.webapp.dao.entity.base.EntityBaseImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
/**
 * @author Admin
 */
// @Entity
// @Table(name = "ChiTieu")
// @XmlRootElement
// @NamedQueries({
// @NamedQuery(name = "ChiTieu.findAll", query = "SELECT c FROM ChiTieu c"),
// @NamedQuery(name = "ChiTieu.findByChiTieuId", query =
// "SELECT c FROM ChiTieu c WHERE c.chiTieuId = :chiTieuId"),
// @NamedQuery(name = "ChiTieu.findByDanhMucId", query =
// "SELECT c FROM ChiTieu c WHERE c.danhMucId = :danhMucId"),
// @NamedQuery(name = "ChiTieu.findByTaiKhoanId", query =
// "SELECT c FROM ChiTieu c WHERE c.taiKhoanId = :taiKhoanId"),
// @NamedQuery(name = "ChiTieu.findByHinhAnhId", query =
// "SELECT c FROM ChiTieu c WHERE c.hinhAnhId = :hinhAnhId"),
// @NamedQuery(name = "ChiTieu.findBySuKienId", query =
// "SELECT c FROM ChiTieu c WHERE c.suKienId = :suKienId"),
// @NamedQuery(name = "ChiTieu.findByChiTieuTen", query =
// "SELECT c FROM ChiTieu c WHERE c.chiTieuTen = :chiTieuTen"),
// @NamedQuery(name = "ChiTieu.findByChiTieuSoTien", query =
// "SELECT c FROM ChiTieu c WHERE c.chiTieuSoTien = :chiTieuSoTien"),
// @NamedQuery(name = "ChiTieu.findByChitieuNguoiGD", query =
// "SELECT c FROM ChiTieu c WHERE c.chitieuNguoiGD = :chitieuNguoiGD"),
// @NamedQuery(name = "ChiTieu.findByChiTieuDiaDiem", query =
// "SELECT c FROM ChiTieu c WHERE c.chiTieuDiaDiem = :chiTieuDiaDiem"),
// @NamedQuery(name = "ChiTieu.findByChiTieuTrangThai", query =
// "SELECT c FROM ChiTieu c WHERE c.chiTieuTrangThai = :chiTieuTrangThai"),
// @NamedQuery(name = "ChiTieu.findByChiTieuGhiChu", query =
// "SELECT c FROM ChiTieu c WHERE c.chiTieuGhiChu = :chiTieuGhiChu"),
// @NamedQuery(name = "ChiTieu.findByChiTieuNgayTao", query =
// "SELECT c FROM ChiTieu c WHERE c.chiTieuNgayTao = :chiTieuNgayTao"),
// @NamedQuery(name = "ChiTieu.findByChiTieuNgaySua", query =
// "SELECT c FROM ChiTieu c WHERE c.chiTieuNgaySua = :chiTieuNgaySua")})
@DatabaseTable(tableName="ChiTieu")
public class ChiTieu extends EntityBaseImpl<Integer>
    implements EntityBase<Integer> {
	public static final String	 ChiTieu	        ="ChiTieu";
	public static final String	 ChiTieuId	      ="ChiTieuId";
	public static final String	 DanhMucId	      ="DanhMucId";
	public static final String	 TaiKhoanId	      ="TaiKhoanId";
	public static final String	 HinhAnhId	      ="HinhAnhId";
	public static final String	 SuKienId	        ="SuKienId";
	public static final String	 ChiTieuTen	      ="ChiTieuTen";
	public static final String	 ChiTieuSoTien	  ="ChiTieuSoTien";
	public static final String	 ChitieuNguoiGD	  ="ChitieuNguoiGD";
	public static final String	 ChiTieuDiaDiem	  ="ChiTieuDiaDiem";
	public static final String	 ChiTieuTrangThai	="ChiTieuTrangThai";
	public static final String	 ChiTieuGhiChu	  ="ChiTieuGhiChu";
	public static final String	 ChiTieuNgayTao	  ="ChiTieuNgayTao";
	public static final String	 ChiTieuNgaySua	  ="ChiTieuNgaySua";
	public static final String[]	ChiTieuColumns	={
	    ChiTieuId,DanhMucId,TaiKhoanId,HinhAnhId,SuKienId,
	    ChiTieuTen,ChiTieuSoTien,ChitieuNguoiGD,
	    ChiTieuDiaDiem,ChiTieuTrangThai,ChiTieuGhiChu,
	    ChiTieuNgayTao,ChiTieuNgaySua	            };
	private static final long	   serialVersionUID	=1L;
	@DatabaseField(columnName="ChiTieuId", id=true, generatedId=true)
	private int	                 chiTieuId;
	@DatabaseField(columnName="DanhMucId")
	private Integer	             danhMucId;
	@DatabaseField(columnName="TaiKhoanId")
	private int	                 taiKhoanId;
	@DatabaseField(columnName="HinhAnhId")
	private Integer	             hinhAnhId;
	@DatabaseField(columnName="SuKienId")
	private Integer	             suKienId;
	@DatabaseField(columnName="ChiTieuTen")
	private String	             chiTieuTen;
	@DatabaseField(columnName="ChiTieuSoTien")
	private Double	             chiTieuSoTien;
	@DatabaseField(columnName="ChitieuNguoiGD")
	private String	             chitieuNguoiGD;
	@DatabaseField(columnName="ChiTieuDiaDiem")
	private String	             chiTieuDiaDiem;
	@DatabaseField(columnName="ChiTieuTrangThai")
	private Integer	             chiTieuTrangThai;
	@DatabaseField(columnName="ChiTieuGhiChu")
	private String	             chiTieuGhiChu;
	@DatabaseField(columnName="ChiTieuNgayTao")
	private String	             chiTieuNgayTao;
	@DatabaseField(columnName="ChiTieuNgaySua")
	private String	             chiTieuNgaySua;
	public ChiTieu(int chiTieuId, int taiKhoanId) {
		this.chiTieuId=chiTieuId;
		this.taiKhoanId=taiKhoanId;
	}
	/**
	 * 
	 */
	public ChiTieu() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param createPK
	 */
	public ChiTieu(Integer createPK) {
		// TODO Auto-generated constructor stub
	}
	public int getChiTieuId() {
		return chiTieuId;
	}
	public void setChiTieuId(int chiTieuId) {
		this.chiTieuId=chiTieuId;
	}
	public Integer getDanhMucId() {
		return danhMucId;
	}
	public void setDanhMucId(Integer danhMucId) {
		this.danhMucId=danhMucId;
	}
	public int getTaiKhoanId() {
		return taiKhoanId;
	}
	public void setTaiKhoanId(int taiKhoanId) {
		this.taiKhoanId=taiKhoanId;
	}
	public Integer getHinhAnhId() {
		return hinhAnhId;
	}
	public void setHinhAnhId(Integer hinhAnhId) {
		this.hinhAnhId=hinhAnhId;
	}
	public Integer getSuKienId() {
		return suKienId;
	}
	public void setSuKienId(Integer suKienId) {
		this.suKienId=suKienId;
	}
	public String getChiTieuTen() {
		return chiTieuTen;
	}
	public void setChiTieuTen(String chiTieuTen) {
		this.chiTieuTen=chiTieuTen;
	}
	public Double getChiTieuSoTien() {
		return chiTieuSoTien;
	}
	public void setChiTieuSoTien(Double chiTieuSoTien) {
		this.chiTieuSoTien=chiTieuSoTien;
	}
	public String getChitieuNguoiGD() {
		return chitieuNguoiGD;
	}
	public void setChitieuNguoiGD(String chitieuNguoiGD) {
		this.chitieuNguoiGD=chitieuNguoiGD;
	}
	public String getChiTieuDiaDiem() {
		return chiTieuDiaDiem;
	}
	public void setChiTieuDiaDiem(String chiTieuDiaDiem) {
		this.chiTieuDiaDiem=chiTieuDiaDiem;
	}
	public Integer getChiTieuTrangThai() {
		return chiTieuTrangThai;
	}
	public void setChiTieuTrangThai(Integer chiTieuTrangThai) {
		this.chiTieuTrangThai=chiTieuTrangThai;
	}
	public String getChiTieuGhiChu() {
		return chiTieuGhiChu;
	}
	public void setChiTieuGhiChu(String chiTieuGhiChu) {
		this.chiTieuGhiChu=chiTieuGhiChu;
	}
	public String getChiTieuNgayTao() {
		return chiTieuNgayTao;
	}
	public void setChiTieuNgayTao(String chiTieuNgayTao) {
		this.chiTieuNgayTao=chiTieuNgayTao;
	}
	public String getChiTieuNgaySua() {
		return chiTieuNgaySua;
	}
	public void setChiTieuNgaySua(String chiTieuNgaySua) {
		this.chiTieuNgaySua=chiTieuNgaySua;
	}
	@Override
	public String toString() {
		return "com.aptech.chitieu.entity.ChiTieu[ chiTieuPK="
		    +chiTieuId+" ]";
	}
	/*
	 * (non-Javadoc)
	 * @see org.oni.webapp.dao.entity.base.EntityBase#getId()
	 */
	@Override
	public Integer getId() {
		return this.chiTieuId;
	}
	/*
	 * (non-Javadoc)
	 * @see org.oni.webapp.dao.entity.base.EntityBase#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Integer pk) {
		this.chiTieuId=pk;
	}
}
