package com.aptech.chitieu.entity;
import java.util.Date;
import org.oni.webapp.dao.entity.base.EntityBase;
import com.aptech.chitieu.util.DateUtils;
// ctChiTieu
/**
 * @author HoanPV
 * @version Jul 4, 2015 : 10:51:56 PM
 */
public class CtChiTieu implements Comparable<CtChiTieu>,
    EntityBase<Integer> {
	private static final long	serialVersionUID	=1L;
	/** int Có Id Danh mục */
	int	                      idChiTieu;
	/** ten Danh mục */
	CtDanhMuc	                idDanhMuc;
	/** float Có Tiền chi tiêu */
	String	                  tenChiTieu;
	float	                    tienChiTieu;
	/** int Có 0 : Ẩn: 1 : Hiện : */
	/** 2 : Xóa Danh mục */
	int	                      trangThai;
	/** text Ghi chú */
	String	                  ghiChu;
	/** dateTime Thời gian khởi tạo */
	Date	                    ngayTao;
	/** dateTime Thời gian update */
	Date	                    ngaySua;
	public CtChiTieu() {}
	public CtChiTieu(int idChiTieu, CtDanhMuc idDanhMuc,
	    String tenChiTieu, float tienChiTieu, int trangThai) {
		this.idChiTieu=idChiTieu;
		this.idDanhMuc=idDanhMuc;
		this.tenChiTieu=tenChiTieu;
		this.tienChiTieu=tienChiTieu;
		this.trangThai=trangThai;
	}
	public int getIdChiTieu() {
		return idChiTieu;
	}
	public void setIdChiTieu(int idChiTieu) {
		this.idChiTieu=idChiTieu;
	}
	public CtDanhMuc getIdDanhMuc() {
		return idDanhMuc;
	}
	public void setIdDanhMuc(CtDanhMuc idDanhMuc) {
		this.idDanhMuc=idDanhMuc;
	}
	public String getTenChiTieu() {
		return tenChiTieu;
	}
	public void setTenChiTieu(String tenChiTieu) {
		this.tenChiTieu=tenChiTieu;
	}
	public float getTienChiTieu() {
		return tienChiTieu;
	}
	public void setTienChiTieu(float tienChiTieu) {
		this.tienChiTieu=tienChiTieu;
	}
	public int getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(int trangThai) {
		this.trangThai=trangThai;
	}
	public String getGhiChu() {
		return ghiChu;
	}
	public void setGhiChu(String ghiChu) {
		this.ghiChu=ghiChu;
	}
	public Date getNgayTao() {
		return ngayTao;
	}
	public void setNgayTao(Date ngayTao) {
		this.ngayTao=ngayTao;
	}
	public Date getNgaySua() {
		return ngaySua;
	}
	public void setNgaySua(Date ngaySua) {
		this.ngaySua=ngaySua;
	}
	@Override
	public String toString() {
		StringBuilder builder=new StringBuilder();
		builder.append("CtChiTieu [idChiTieu=")
		    .append(idChiTieu).append(",");
		builder.append(" tenChiTieu=").append(tenChiTieu)
		    .append(",");
		builder.append(" idDanhMuc=").append(idDanhMuc)
		    .append(",");
		builder.append(" tienChiTieu=").append(tienChiTieu)
		    .append(",");
		builder.append(" trangThai=").append(trangThai)
		    .append(", ");
		builder.append(" ghiChu=").append(ghiChu).append(",");
		builder.append(" ngayTao=").append(ngayTao).append(",");
		builder.append(" ngaySua=").append(ngaySua).append("]");
		return builder.toString();
	}
	public String toView() {
		StringBuilder builder=new StringBuilder();
		builder.append(idChiTieu).append("-");
		builder.append(tenChiTieu).append("-");
		builder.append(tienChiTieu).append("-");
		builder.append(DateUtils.yyyyMMdd(ngayTao.getTime()));
		return builder.toString();
	}
	@Override
	public int compareTo(CtChiTieu another) {
		return this.ngayTao.compareTo(another.getNgayTao());
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.entity.EntityBase#getId()
	 */
	@Override
	public Integer getId() {
		return this.idChiTieu;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.entity.EntityBase#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Integer pk) {
		this.idChiTieu=pk;
	}
}
