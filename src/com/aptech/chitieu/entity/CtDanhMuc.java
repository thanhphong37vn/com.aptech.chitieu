package com.aptech.chitieu.entity;
import java.util.Date;
import org.oni.webapp.dao.entity.base.EntityBase;
/**
 * Title : Model Entity <br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 6, 2015 4:58:33 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 6, 2015 4:58:33 PM
 */
public class CtDanhMuc implements EntityBase<Integer>,
    Comparable<CtDanhMuc> {
	private static final long	serialVersionUID	=1L;
	private int	              idDanhMuc;
	/** idDanhMuc int Có Id Danh mucj */
	/** mục */
	private String	          tenDanhMuc;
	/** text Tên loại danh mục */
	/** int Có 0: Thu | 1 : Chi : */
	/** 2:HoatDong 3 : Xoa */
	private int	              trangThai;
	private String	          ghiChu;
	/** text Ghi chú8 */
	private Date	            ngayTao;
	/** dateTime Thời gian khởi tạo */
	private Date	            ngaySua;
	/** dateTime Thời gian update */
	public int getIdDanhMuc() {
		return idDanhMuc;
	}
	public void setIdDanhMuc(int idDanhMuc) {
		this.idDanhMuc=idDanhMuc;
	}
	public String getTenDanhMuc() {
		return tenDanhMuc;
	}
	public void setTenDanhMuc(String tenDanhMuc) {
		this.tenDanhMuc=tenDanhMuc;
	}
	public int getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(int trangThai) {
		this.trangThai=trangThai;
	}
	public String getGhiChu() {
		return ghiChu;
	}
	public void setGhiChu(String ghiChu) {
		this.ghiChu=ghiChu;
	}
	public Date getNgayTao() {
		return ngayTao;
	}
	public void setNgayTao(Date ngayTao) {
		this.ngayTao=ngayTao;
	}
	public Date getNgaySua() {
		return ngaySua;
	}
	public void setNgaySua(Date ngaySua) {
		this.ngaySua=ngaySua;
	}
	public CtDanhMuc(int idDanhMuc, String tenDanhMuc,
	    int trangThai, String ghiChu, Date ngayTao,
	    Date ngaySua) {
		super();
		this.idDanhMuc=idDanhMuc;
		this.tenDanhMuc=tenDanhMuc;
		this.trangThai=trangThai;
		this.ghiChu=ghiChu;
		this.ngayTao=ngayTao;
		this.ngaySua=ngaySua;
	}
	public CtDanhMuc() {}
	public CtDanhMuc(int idDanhMuc, String tenDanhMuc,
	    int trangThai) {
		super();
		this.idDanhMuc=idDanhMuc;
		this.tenDanhMuc=tenDanhMuc;
		this.trangThai=trangThai;
	}
	@Override
	public String toString() {
		return this.tenDanhMuc;
	}
	public String toString2() {
		StringBuilder builder=new StringBuilder();
		builder.append("CtDanhMuc [idDanhMuc=")
		    .append(idDanhMuc).append(",");
		builder.append(" tenDanhMuc=").append(tenDanhMuc)
		    .append(",");
		builder.append(" trangThai=").append(trangThai)
		    .append(", ");
		builder.append(" ghiChu=").append(ghiChu).append(",");
		builder.append(" ngayTao=").append(ngayTao).append(",");
		builder.append(" ngaySua=").append(ngaySua).append("]");
		return builder.toString();
	}
	@Override
	public int compareTo(CtDanhMuc another) {
		return this.getTenDanhMuc().compareTo(
		    another.getTenDanhMuc());
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.entity.EntityBase#setId(java.lang.Integer)
	 */
	@Override
	public Integer getId() {
		return this.getId();
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.entity.EntityBase#setId(java.lang.Integer)
	 */
	@Override
	public void setId(Integer pk) {
		// TODO Auto-generated method stub
	}
}
