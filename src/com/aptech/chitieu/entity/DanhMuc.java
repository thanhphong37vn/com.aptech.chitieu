package com.aptech.chitieu.entity;
import org.oni.webapp.dao.entity.base.EntityBaseImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
/**
 * @author Admin
 */
// @Entity
// @XmlRootElement
// @NamedQueries({
// @NamedQuery(name = "DanhMuc.findAll", query = "SELECT d FROM DanhMuc d"),
// @NamedQuery(name = "DanhMuc.findByDanhMucId", query =
// "SELECT d FROM DanhMuc d WHERE d.danhMucId = :danhMucId"),
// @NamedQuery(name = "DanhMuc.findByDanhMucTen", query =
// "SELECT d FROM DanhMuc d WHERE d.danhMucTen = :danhMucTen"),
// @NamedQuery(name = "DanhMuc.findByDanhMucTrangThai", query =
// "SELECT d FROM DanhMuc d WHERE d.danhMucTrangThai = :danhMucTrangThai"),
// @NamedQuery(name = "DanhMuc.findByDanhMucGhiChu", query =
// "SELECT d FROM DanhMuc d WHERE d.danhMucGhiChu = :danhMucGhiChu"),
// @NamedQuery(name = "DanhMuc.findByDanhMucNgayTao", query =
// "SELECT d FROM DanhMuc d WHERE d.danhMucNgayTao = :danhMucNgayTao"),
// @NamedQuery(name = "DanhMuc.findByDanhMucNgaySua", query =
// "SELECT d FROM DanhMuc d WHERE d.danhMucNgaySua = :danhMucNgaySua"),
// @NamedQuery(name = "DanhMuc.findByHinhAnhId", query =
// "SELECT d FROM DanhMuc d WHERE d.hinhAnhId = :hinhAnhId")})
@DatabaseTable(tableName="DanhMuc")
public class DanhMuc extends EntityBaseImpl<Integer> {
	public static final String	 DanhMuc	        ="DanhMuc";
	public static final String	 DanhMucId	      ="DanhMucId";
	public static final String	 DanhMucTen	      ="DanhMucTen";
	public static final String	 DanhMucTrangThai	="DanhMucTrangThai";
	public static final String	 DanhMucGhiChu	  ="DanhMucGhiChu";
	public static final String	 DanhMucNgayTao	  ="DanhMucNgayTao";
	public static final String	 DanhMucNgaySua	  ="DanhMucNgaySua";
	public static final String	 HinhAnhId	      ="HinhAnhId";
	public static final String[]	DanhMucColumns	={
	    DanhMucId,DanhMucTen,DanhMucTrangThai,DanhMucGhiChu,
	    DanhMucNgayTao,DanhMucNgaySua,HinhAnhId	  };
	private static final long	   serialVersionUID	=1L;
	@DatabaseField(columnName="DanhMucId", id=true, generatedId=true)
	private int	                 danhMucId;
	@DatabaseField(columnName="DanhMucTen")
	private String	             danhMucTen;
	@DatabaseField(columnName="DanhMucTrangThai")
	private Integer	             danhMucTrangThai;
	@DatabaseField(columnName="DanhMucGhiChu")
	private String	             danhMucGhiChu;
	@DatabaseField(columnName="DanhMucNgayTao")
	private String	             danhMucNgayTao;
	@DatabaseField(columnName="DanhMucNgaySua")
	private String	             danhMucNgaySua;
	@DatabaseField(columnName="HinhAnhId")
	private Integer	             hinhAnhId;
	public DanhMuc() {}
	public DanhMuc(int danhMucId) {
		this.danhMucId=danhMucId;
	}
	public DanhMuc(int danhMucId, String danhMucNgayTao) {
		this.danhMucId=danhMucId;
		this.danhMucNgayTao=danhMucNgayTao;
	}
	public int getDanhMucId() {
		return danhMucId;
	}
	public void setDanhMucId(int danhMucId) {
		this.danhMucId=danhMucId;
	}
	public String getDanhMucTen() {
		return danhMucTen;
	}
	public void setDanhMucTen(String danhMucTen) {
		this.danhMucTen=danhMucTen;
	}
	public Integer getDanhMucTrangThai() {
		return danhMucTrangThai;
	}
	public void setDanhMucTrangThai(Integer danhMucTrangThai) {
		this.danhMucTrangThai=danhMucTrangThai;
	}
	public String getDanhMucGhiChu() {
		return danhMucGhiChu;
	}
	public void setDanhMucGhiChu(String danhMucGhiChu) {
		this.danhMucGhiChu=danhMucGhiChu;
	}
	public String getDanhMucNgayTao() {
		return danhMucNgayTao;
	}
	public void setDanhMucNgayTao(String danhMucNgayTao) {
		this.danhMucNgayTao=danhMucNgayTao;
	}
	public String getDanhMucNgaySua() {
		return danhMucNgaySua;
	}
	public void setDanhMucNgaySua(String danhMucNgaySua) {
		this.danhMucNgaySua=danhMucNgaySua;
	}
	public Integer getHinhAnhId() {
		return hinhAnhId;
	}
	public void setHinhAnhId(Integer hinhAnhId) {
		this.hinhAnhId=hinhAnhId;
	}
	/*
	 * (non-Javadoc)
	 * @see org.oni.webapp.dao.entity.base.EntityBase#getId()
	 */
	@Override
	public Integer getId() {
		return this.danhMucId;
	}
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */@Override
	public String toString() {
		return this.getDanhMucTen();
	}
	public String toString3() {
		return "DanhMuc [danhMucId="+this.danhMucId
		    +", danhMucTen="+this.danhMucTen
		    +", danhMucTrangThai="+this.danhMucTrangThai
		    +", danhMucGhiChu="+this.danhMucGhiChu
		    +", danhMucNgayTao="+this.danhMucNgayTao
		    +", danhMucNgaySua="+this.danhMucNgaySua
		    +", hinhAnhId="+this.hinhAnhId+"]";
	}
	public String toString2() {
		return "DanhMuc [danhMucId="+this.danhMucId+"]";
	}
	/*
	 * (non-Javadoc)
	 * @see org.oni.webapp.dao.entity.base.EntityBase#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Integer pk) {
		this.danhMucId=pk;
	}
}
