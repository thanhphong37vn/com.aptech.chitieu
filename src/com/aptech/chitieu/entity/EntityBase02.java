package com.aptech.chitieu.entity;
import java.io.Serializable;
/**
 * Title : Model Layer<br/>
 * Description : Interface Entity <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Jul 14, 2015 10:14:47 AM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Jul 14, 2015 10:14:47 AM
 */
public interface EntityBase02<PK extends Serializable> extends
    Serializable {
	/**
	 * get primary key value
	 * 
	 * @return primary key
	 */
	PK getId();
	/**
	 * set primary key value
	 * 
	 * @param pk
	 *          primary key
	 */
	void setId(PK pk);
}
