package com.aptech.chitieu.entity;

import org.oni.webapp.dao.entity.base.EntityBaseImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Admin
 */
// @Entity
// @XmlRootElement
// @NamedQueries({
// @NamedQuery(name="HinhAnh.findAll", query="SELECT h FROM HinhAnh h"),
// @NamedQuery(name="HinhAnh.findByHinhAnhId",
// query="SELECT h FROM HinhAnh h WHERE h.hinhAnhId = :hinhAnhId"),
// @NamedQuery(name="HinhAnh.findByHinhAnhTen",
// query="SELECT h FROM HinhAnh h WHERE h.hinhAnhTen = :hinhAnhTen"),
// @NamedQuery(name="HinhAnh.findByHinhAnhDuongDan",
// query="SELECT h FROM HinhAnh h WHERE h.hinhAnhDuongDan = :hinhAnhDuongDan"),
// @NamedQuery(name="HinhAnh.findByHinhAnhTrangThai",
// query="SELECT h FROM HinhAnh h WHERE h.hinhAnhTrangThai = :hinhAnhTrangThai"),
// @NamedQuery(name="HinhAnh.findByHinhAnhGhiChu",
// query="SELECT h FROM HinhAnh h WHERE h.hinhAnhGhiChu = :hinhAnhGhiChu"),
// @NamedQuery(name="HinhAnh.findByHinhAnhNgayTao",
// query="SELECT h FROM HinhAnh h WHERE h.hinhAnhNgayTao = :hinhAnhNgayTao"),
// @NamedQuery(name="HinhAnh.findByHinhAnhNgaySua",
// query="SELECT h FROM HinhAnh h WHERE h.hinhAnhNgaySua = :hinhAnhNgaySua")})
@DatabaseTable(tableName = "HinhAnh")
public class HinhAnh extends EntityBaseImpl<Integer> {
	public static final String HinhAnh = "HinhAnh";
	public static final String HinhAnhId = "HinhAnhId";
	public static final String HinhAnhTen = "HinhAnhTen";
	public static final String HinhAnhDuongDan = "HinhAnhDuongDan";
	public static final String HinhAnhTrangThai = "HinhAnhTrangThai";
	public static final String HinhAnhGhiChu = "HinhAnhGhiChu";
	public static final String HinhAnhNgayTao = "HinhAnhNgayTao";
	public static final String HinhAnhNgaySua = "HinhAnhNgaySua";
	public static final String[] HinhAnhColumns = { HinhAnhId, HinhAnhTen,
			HinhAnhDuongDan, HinhAnhTrangThai, HinhAnhGhiChu, HinhAnhNgayTao,
			HinhAnhNgaySua };
	private static final long serialVersionUID = 1L;
	@DatabaseField(columnName = "HinhAnhId", id = true, generatedId = true)
	private Integer hinhAnhId;
	@DatabaseField(columnName = "HinhAnhTen")
	private String hinhAnhTen;
	@DatabaseField(columnName = "HinhAnhDuongDan")
	private String hinhAnhDuongDan;
	@DatabaseField(columnName = "HinhAnhTrangThai")
	private Integer hinhAnhTrangThai;
	@DatabaseField(columnName = "HinhAnhGhiChu")
	private String hinhAnhGhiChu;
	@DatabaseField(columnName = "HinhAnhNgayTao")
	private String hinhAnhNgayTao;
	@DatabaseField(columnName = "HinhAnhNgaySua")
	private String hinhAnhNgaySua;

	public HinhAnh() {
	}

	public HinhAnh(int hinhAnhId) {
		this.hinhAnhId = hinhAnhId;
	}

	public HinhAnh(int hinhAnhId, String hinhAnhNgayTao) {
		this.hinhAnhId = hinhAnhId;
		this.hinhAnhNgayTao = hinhAnhNgayTao;
	}

	public int getHinhAnhId() {
		return hinhAnhId;
	}

	public void setHinhAnhId(int hinhAnhId) {
		this.hinhAnhId = hinhAnhId;
	}

	public String getHinhAnhTen() {
		return hinhAnhTen;
	}

	public void setHinhAnhTen(String hinhAnhTen) {
		this.hinhAnhTen = hinhAnhTen;
	}

	public String getHinhAnhDuongDan() {
		return hinhAnhDuongDan;
	}

	public void setHinhAnhDuongDan(String hinhAnhDuongDan) {
		this.hinhAnhDuongDan = hinhAnhDuongDan;
	}

	public Integer getHinhAnhTrangThai() {
		return hinhAnhTrangThai;
	}

	public void setHinhAnhTrangThai(Integer hinhAnhTrangThai) {
		this.hinhAnhTrangThai = hinhAnhTrangThai;
	}

	public String getHinhAnhGhiChu() {
		return hinhAnhGhiChu;
	}

	public void setHinhAnhGhiChu(String hinhAnhGhiChu) {
		this.hinhAnhGhiChu = hinhAnhGhiChu;
	}

	public String getHinhAnhNgayTao() {
		return hinhAnhNgayTao;
	}

	public void setHinhAnhNgayTao(String hinhAnhNgayTao) {
		this.hinhAnhNgayTao = hinhAnhNgayTao;
	}

	public String getHinhAnhNgaySua() {
		return hinhAnhNgaySua;
	}

	public void setHinhAnhNgaySua(String hinhAnhNgaySua) {
		this.hinhAnhNgaySua = hinhAnhNgaySua;
	}

	@Override
	public String toString() {
		return this.getHinhAnhDuongDan();
	}

	public String toString2() {
		return "com.aptech.chitieu.entity.HinhAnh[ hinhAnhPK=" + hinhAnhId
				+ " ]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.entity.EntityBase#getId()
	 */
	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return this.hinhAnhId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.entity.EntityBase#setId(java.lang.Object)
	 */
	@Override
	public void setId(Integer pk) {
		// TODO Auto-generated method stub
		this.hinhAnhId = pk;
	}

}
