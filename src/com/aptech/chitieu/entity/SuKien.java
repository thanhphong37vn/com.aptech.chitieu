package com.aptech.chitieu.entity;
import org.oni.webapp.dao.entity.base.EntityBaseImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
/**
 * @author Admin
 */
// @Entity
// @XmlRootElement
// @NamedQueries({
// @NamedQuery(name="SuKien.findAll", query="SELECT s FROM SuKien s"),
// @NamedQuery(name="SuKien.findBySuKienId",
// query="SELECT s FROM SuKien s WHERE s.suKienId = :suKienId"),
// @NamedQuery(name="SuKien.findBySuKienTen",
// query="SELECT s FROM SuKien s WHERE s.suKienTen = :suKienTen"),
// @NamedQuery(name="SuKien.findBySuKienTrangThai",
// query="SELECT s FROM SuKien s WHERE s.suKienTrangThai = :suKienTrangThai"),
// @NamedQuery(name="SuKien.findBySuKienGhiChu",
// query="SELECT s FROM SuKien s WHERE s.suKienGhiChu = :suKienGhiChu"),
// @NamedQuery(name="SuKien.findBySuKienNgayTao",
// query="SELECT s FROM SuKien s WHERE s.suKienNgayTao = :suKienNgayTao"),
// @NamedQuery(name="SuKien.findBySuKienNgaySua",
// query="SELECT s FROM SuKien s WHERE s.suKienNgaySua = :suKienNgaySua"),
// @NamedQuery(name="SuKien.findByHinhAnhId",
// query="SELECT s FROM SuKien s WHERE s.hinhAnhId = :hinhAnhId")})
@DatabaseTable(tableName="SuKien")
public class SuKien extends EntityBaseImpl<Integer> {
	public static final String	 SuKien	          ="SuKien";
	public static final String	 SuKienId	        ="SuKienId";
	public static final String	 SuKienTen	      ="SuKienTen";
	public static final String	 SuKienTrangThai	="SuKienTrangThai";
	public static final String	 SuKienGhiChu	    ="SuKienGhiChu";
	public static final String	 SuKienNgayTao	  ="SuKienNgayTao";
	public static final String	 SuKienNgaySua	  ="SuKienNgaySua";
	public static final String	 HinhAnhId	      ="HinhAnhId";
	public static final String[]	SuKienColumns	  ={SuKienId,
	    SuKienTen,SuKienTrangThai,SuKienGhiChu,SuKienNgayTao,
	    SuKienNgaySua,HinhAnhId	                  };
	private static final long	   serialVersionUID	=1L;
	@DatabaseField(columnName="SuKienId", id=true, generatedId=true)
	private int	                 suKienId;
	@DatabaseField(columnName="SuKienTen")
	private String	             suKienTen;
	@DatabaseField(columnName="SuKienTrangThai")
	private Integer	             suKienTrangThai;
	@DatabaseField(columnName="SuKienGhiChu")
	private String	             suKienGhiChu;
	@DatabaseField(columnName="SuKienNgayTao")
	private String	             suKienNgayTao;
	@DatabaseField(columnName="SuKienNgaySua")
	private String	             suKienNgaySua;
	@DatabaseField(columnName="HinhAnhId")
	private Integer	             hinhAnhId;
	public SuKien() {}
	public SuKien(int suKienId) {
		this.suKienId=suKienId;
	}
	public SuKien(int suKienId, String suKienNgayTao) {
		this.suKienId=suKienId;
		this.suKienNgayTao=suKienNgayTao;
	}
	public int getSuKienId() {
		return suKienId;
	}
	public void setSuKienId(int suKienId) {
		this.suKienId=suKienId;
	}
	public String getSuKienTen() {
		return suKienTen;
	}
	public void setSuKienTen(String suKienTen) {
		this.suKienTen=suKienTen;
	}
	public Integer getSuKienTrangThai() {
		return suKienTrangThai;
	}
	public void setSuKienTrangThai(Integer suKienTrangThai) {
		this.suKienTrangThai=suKienTrangThai;
	}
	public String getSuKienGhiChu() {
		return suKienGhiChu;
	}
	public void setSuKienGhiChu(String suKienGhiChu) {
		this.suKienGhiChu=suKienGhiChu;
	}
	public String getSuKienNgayTao() {
		return suKienNgayTao;
	}
	public void setSuKienNgayTao(String suKienNgayTao) {
		this.suKienNgayTao=suKienNgayTao;
	}
	public String getSuKienNgaySua() {
		return suKienNgaySua;
	}
	public void setSuKienNgaySua(String suKienNgaySua) {
		this.suKienNgaySua=suKienNgaySua;
	}
	public Integer getHinhAnhId() {
		return hinhAnhId;
	}
	public void setHinhAnhId(Integer hinhAnhId) {
		this.hinhAnhId=hinhAnhId;
	}
	@Override
	public String toString() {
		return "com.aptech.chitieu.entity.SuKien[ suKienPK="
		    +suKienId+" ]";
	}
	/*
	 * (non-Javadoc)
	 * @see org.oni.webapp.dao.entity.base.EntityBase#getId()
	 */
	@Override
	public Integer getId() {
		return this.suKienId;
	}
	/*
	 * (non-Javadoc)
	 * @see org.oni.webapp.dao.entity.base.EntityBase#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Integer pk) {
		this.suKienId=pk;
	}
}
