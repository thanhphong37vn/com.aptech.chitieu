package com.aptech.chitieu.entity;

import org.oni.webapp.dao.entity.base.EntityBaseImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Admin
 */
// @Entity
// @XmlRootElement
// @NamedQueries({
// @NamedQuery(name="TaiKhoan.findAll", query="SELECT t FROM TaiKhoan t"),
// @NamedQuery(name="TaiKhoan.findByTaiKhoanId",
// query="SELECT t FROM TaiKhoan t WHERE t.taiKhoanId = :taiKhoanId"),
// @NamedQuery(name="TaiKhoan.findByTaiKhoanTen",
// query="SELECT t FROM TaiKhoan t WHERE t.taiKhoanTen = :taiKhoanTen"),
// @NamedQuery(name="TaiKhoan.findByTaiKhoanTrangThai",
// query="SELECT t FROM TaiKhoan t WHERE t.taiKhoanTrangThai = :taiKhoanTrangThai"),
// @NamedQuery(name="TaiKhoan.findByTaiKhoanGhiChu",
// query="SELECT t FROM TaiKhoan t WHERE t.taiKhoanGhiChu = :taiKhoanGhiChu"),
// @NamedQuery(name="TaiKhoan.findByTaiKhoanNgayTao",
// query="SELECT t FROM TaiKhoan t WHERE t.taiKhoanNgayTao = :taiKhoanNgayTao"),
// @NamedQuery(name="TaiKhoan.findByTaiKhoanNgaySua",
// query="SELECT t FROM TaiKhoan t WHERE t.taiKhoanNgaySua = :taiKhoanNgaySua"),
// @NamedQuery(name="TaiKhoan.findByHinhAnhId",
// query="SELECT t FROM TaiKhoan t WHERE t.hinhAnhId = :hinhAnhId")})
@DatabaseTable(tableName = "TaiKhoan")
public class TaiKhoan extends EntityBaseImpl<Integer> {
	public static final String TaiKhoan = "TaiKhoan";
	public static final String TaiKhoanId = "TaiKhoanId";
	public static final String TaiKhoanTen = "TaiKhoanTen";
	public static final String TaiKhoanTrangThai = "TaiKhoanTrangThai";
	public static final String TaiKhoanGhiChu = "TaiKhoanGhiChu";
	public static final String TaiKhoanNgayTao = "TaiKhoanNgayTao";
	public static final String TaiKhoanNgaySua = "TaiKhoanNgaySua";
	public static final String HinhAnhId = "HinhAnhId";
	public static final String[] TaiKhoanColumns = { TaiKhoanId, TaiKhoanTen,
			TaiKhoanTrangThai, TaiKhoanGhiChu, TaiKhoanNgayTao,
			TaiKhoanNgaySua, HinhAnhId };
	private static final long serialVersionUID = 1L;
	@DatabaseField(columnName = "TaiKhoanId", id = true, generatedId = true)
	private int taiKhoanId;
	@DatabaseField(columnName = "TaiKhoanTen")
	private String taiKhoanTen;
	@DatabaseField(columnName = "TaiKhoanTrangThai")
	private Integer taiKhoanTrangThai;
	@DatabaseField(columnName = "TaiKhoanGhiChu")
	private String taiKhoanGhiChu;
	@DatabaseField(columnName = "TaiKhoanNgayTao")
	private String taiKhoanNgayTao;
	@DatabaseField(columnName = "TaiKhoanNgaySua")
	private String taiKhoanNgaySua;
	@DatabaseField(columnName = "HinhAnhId")
	private Integer hinhAnhId;

	public TaiKhoan() {
	}

	public TaiKhoan(int taiKhoanId) {
		this.taiKhoanId = taiKhoanId;
	}

	public TaiKhoan(int taiKhoanId, String taiKhoanNgayTao) {
		this.taiKhoanId = taiKhoanId;
		this.taiKhoanNgayTao = taiKhoanNgayTao;
	}

	public int getTaiKhoanId() {
		return taiKhoanId;
	}

	public void setTaiKhoanId(int taiKhoanId) {
		this.taiKhoanId = taiKhoanId;
	}

	public String getTaiKhoanTen() {
		return taiKhoanTen;
	}

	public void setTaiKhoanTen(String taiKhoanTen) {
		this.taiKhoanTen = taiKhoanTen;
	}

	public Integer getTaiKhoanTrangThai() {
		return taiKhoanTrangThai;
	}

	public void setTaiKhoanTrangThai(Integer taiKhoanTrangThai) {
		this.taiKhoanTrangThai = taiKhoanTrangThai;
	}

	public String getTaiKhoanGhiChu() {
		return taiKhoanGhiChu;
	}

	public void setTaiKhoanGhiChu(String taiKhoanGhiChu) {
		this.taiKhoanGhiChu = taiKhoanGhiChu;
	}

	public String getTaiKhoanNgayTao() {
		return taiKhoanNgayTao;
	}

	public void setTaiKhoanNgayTao(String taiKhoanNgayTao) {
		this.taiKhoanNgayTao = taiKhoanNgayTao;
	}

	public String getTaiKhoanNgaySua() {
		return taiKhoanNgaySua;
	}

	public void setTaiKhoanNgaySua(String taiKhoanNgaySua) {
		this.taiKhoanNgaySua = taiKhoanNgaySua;
	}

	public Integer getHinhAnhId() {
		return hinhAnhId;
	}

	public void setHinhAnhId(Integer hinhAnhId) {
		this.hinhAnhId = hinhAnhId;
	}

	@Override
	public String toString() {
		return this.taiKhoanTen;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.oni.webapp.dao.entity.base.EntityBase#getId()
	 */
	@Override
	public Integer getId() {
		return this.taiKhoanId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.oni.webapp.dao.entity.base.EntityBase#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Integer pk) {
		this.taiKhoanId = pk;
	}
}
