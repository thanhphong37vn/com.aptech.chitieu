package com.aptech.chitieu.entity;
import org.oni.webapp.dao.entity.base.EntityBaseImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
/**
 * @author Admin
 */
// @EntityC
// @XmlRootElement
// @NamedQueries({
// @NamedQuery(name = "Tien.findAll", query = "SELECT t FROM Tien t"),
// @NamedQuery(name = "Tien.findByTienId", query =
// "SELECT t FROM Tien t WHERE t.tienId = :tienId"),
// @NamedQuery(name = "Tien.findByTienQuocGia", query =
// "SELECT t FROM Tien t WHERE t.tienQuocGia = :tienQuocGia"),
// @NamedQuery(name = "Tien.findByTienTen", query =
// "SELECT t FROM Tien t WHERE t.tienTen = :tienTen"),
// @NamedQuery(name = "Tien.findByTienKyHieu", query =
// "SELECT t FROM Tien t WHERE t.tienKyHieu = :tienKyHieu"),
// @NamedQuery(name = "Tien.findByTienMaTien", query =
// "SELECT t FROM Tien t WHERE t.tienMaTien = :tienMaTien"),
// @NamedQuery(name = "Tien.findByTienTrangThai", query =
// "SELECT t FROM Tien t WHERE t.tienTrangThai = :tienTrangThai"),
// @NamedQuery(name = "Tien.findByTienGhiChu", query =
// "SELECT t FROM Tien t WHERE t.tienGhiChu = :tienGhiChu"),
// @NamedQuery(name = "Tien.findByTienNgayTao", query =
// "SELECT t FROM Tien t WHERE t.tienNgayTao = :tienNgayTao"),
// @NamedQuery(name = "Tien.findByTienNgaySua", query =
// "SELECT t FROM Tien t WHERE t.tienNgaySua = :tienNgaySua"),
// @NamedQuery(name = "Tien.findByHinhAnhId", query =
// "SELECT t FROM Tien t WHERE t.hinhAnhId = :hinhAnhId")})
@DatabaseTable(tableName="Tien")
public class Tien extends EntityBaseImpl<Integer>  {
	public static final String	 Tien	            ="Tien";
	public static final String	 TienId	          ="TienId";
	public static final String	 TienQuocGia	    ="TienQuocGia";
	public static final String	 TienTen	        ="TienTen";
	public static final String	 TienKyHieu	      ="TienKyHieu";
	public static final String	 TienMaTien	      ="TienMaTien";
	public static final String	 TienTrangThai	  ="TienTrangThai";
	public static final String	 TienGhiChu	      ="TienGhiChu";
	public static final String	 TienNgayTao	    ="TienNgayTao";
	public static final String	 TienNgaySua	    ="TienNgaySua";
	public static final String	 HinhAnhId	      ="HinhAnhId";
	public static final String[]	TienColumns	    ={TienId,
	    TienQuocGia,TienTen,TienKyHieu,TienMaTien,
	    TienTrangThai,TienGhiChu,TienNgayTao,TienNgaySua,
	    HinhAnhId	                                };
	private static final long	   serialVersionUID	=1L;
	@DatabaseField(columnName="TienId", id=true, generatedId=true)
	private int	                 tienId;
	@DatabaseField(columnName="TienQuocGia")
	private String	             tienQuocGia;
	@DatabaseField(columnName="TienTen")
	private String	             tienTen;
	@DatabaseField(columnName="TienKyHieu")
	private String	             tienKyHieu;
	@DatabaseField(columnName="TienMaTien")
	private String	             tienMaTien;
	@DatabaseField(columnName="TienTrangThai")
	private Integer	             tienTrangThai;
	@DatabaseField(columnName="TienGhiChu")
	private String	             tienGhiChu;
	@DatabaseField(columnName="TienNgayTao")
	private String	             tienNgayTao;
	@DatabaseField(columnName="TienNgaySua")
	private String	             tienNgaySua;
	@DatabaseField(columnName="HinhAnhId")
	private Integer	             hinhAnhId;
	public Tien() {}
	public Tien(int tienId) {
		this.tienId=tienId;
	}
	public Tien(int tienId, String tienNgayTao) {
		this.tienId=tienId;
		this.tienNgayTao=tienNgayTao;
	}
	public int getTienId() {
		return tienId;
	}
	public void setTienId(int tienId) {
		this.tienId=tienId;
	}
	public String getTienQuocGia() {
		return tienQuocGia;
	}
	public void setTienQuocGia(String tienQuocGia) {
		this.tienQuocGia=tienQuocGia;
	}
	public String getTienTen() {
		return tienTen;
	}
	public void setTienTen(String tienTen) {
		this.tienTen=tienTen;
	}
	public String getTienKyHieu() {
		return tienKyHieu;
	}
	public void setTienKyHieu(String tienKyHieu) {
		this.tienKyHieu=tienKyHieu;
	}
	public String getTienMaTien() {
		return tienMaTien;
	}
	public void setTienMaTien(String tienMaTien) {
		this.tienMaTien=tienMaTien;
	}
	public Integer getTienTrangThai() {
		return tienTrangThai;
	}
	public void setTienTrangThai(Integer tienTrangThai) {
		this.tienTrangThai=tienTrangThai;
	}
	public String getTienGhiChu() {
		return tienGhiChu;
	}
	public void setTienGhiChu(String tienGhiChu) {
		this.tienGhiChu=tienGhiChu;
	}
	public String getTienNgayTao() {
		return tienNgayTao;
	}
	public void setTienNgayTao(String tienNgayTao) {
		this.tienNgayTao=tienNgayTao;
	}
	public String getTienNgaySua() {
		return tienNgaySua;
	}
	public void setTienNgaySua(String tienNgaySua) {
		this.tienNgaySua=tienNgaySua;
	}
	public Integer getHinhAnhId() {
		return hinhAnhId;
	}
	public void setHinhAnhId(Integer hinhAnhId) {
		this.hinhAnhId=hinhAnhId;
	}
	@Override
	public String toString() {
		return "com.aptech.chitieu.entity.Tien[ tienPK="+tienId
		    +" ]";
	}
	/*
	 * (non-Javadoc)
	 * @see org.oni.webapp.dao.entity.base.EntityBase#getId()
	 */
	@Override
	public Integer getId() {
		return this.tienId;
	}
	/*
	 * (non-Javadoc)
	 * @see org.oni.webapp.dao.entity.base.EntityBase#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Integer pk) {
		this.tienId=pk;
	}
}
