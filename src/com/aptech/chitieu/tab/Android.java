package com.aptech.chitieu.tab;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import com.aptech.chitieu.MainActivity;
import com.aptech.chitieu.R;
import com.aptech.chitieu.adapter.CtChitieuAdapter;
import com.aptech.chitieu.dao.CtChiTieuDataSource;
import com.aptech.chitieu.dao.CtDanhMucDataSource;
import com.aptech.chitieu.entity.CtChiTieu;
import com.aptech.chitieu.entity.CtDanhMuc;
import android.app.DatePickerDialog;
// import android.app.Fragment;import java.text.SimpleDateFormat;
import java.util.*;
import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import com.aptech.chitieu.adapter.CtChitieuAdapter;
import com.aptech.chitieu.adapter.CtDanhMucAdapter;
import com.aptech.chitieu.dao.CtChiTieuDataSource;
import com.aptech.chitieu.dao.CtDanhMucDataSource;
import com.aptech.chitieu.entity.CtChiTieu;
import com.aptech.chitieu.entity.CtDanhMuc;
import com.aptech.chitieu.util.DateUtils;
import android.R.layout;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View.OnClickListener;
import android.webkit.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.*;
import static com.aptech.chitieu.util.DigestUtil.*;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
public class Android extends Fragment {
	// public static List<CtChiTieu> listThuChi =new ArrayList<CtChiTieu>();
	private float	                             tongThuChi	        =0f;
	private float	                             tongThu	          =0f;
	private float	                             tongChi	          =0f;
	// Control
	private ListView	                         ct_lv_thuChi	      =null;
	private RadioButton	                       ct_rd_thu	        =null;
	private RadioButton	                       ct_rd_chi	        =null;
	private TextView	                         ct_txt_tenChiTieu	=null;
	private TextView	                         ct_txt_tienChiTieu	=null;
	private EditText	                         ct_txt_ngayTao	    =null;
	// Control View
	private TextView	                         ct_txt_TongChi	    =null;
	private TextView	                         ct_txt_TongThu	    =null;
	private TextView	                         ct_txt_TongThuChi	=null;
	private Spinner	                           ct_sp_tenDanhMuc	  =null;
	private WebView	                           webView	          =null;
	private DatePickerDialog.OnDateSetListener	date	            =null;
	// Adapter
	private Calendar	                         calendar	          =Calendar
	                                                                  .getInstance();
	// Làm việc với Database
	private List<CtChiTieu>	                   listThuChi	        =new ArrayList<CtChiTieu>();
	private List<CtDanhMuc>	                   lstCtDM	          =new ArrayList<CtDanhMuc>();
	private ArrayAdapter<CtDanhMuc>	           ctDanhMucAdapter	  =null;
	private CtChitieuAdapter	                 ctChiTieuAdapter;
	private CtDanhMucDataSource	               ctDanhMucDataSource;
	private CtChiTieuDataSource	               ctChiTieuDataSource;
	private View	                             tabAndroid;
	@Override
	public View onCreateView(LayoutInflater inflater,
	    ViewGroup container, Bundle savedInstanceState) {
		View android=inflater.inflate(
		    R.layout.tab_android_frag,container,false);
		// ((TextView) android.findViewById(R.id.textView))
		// .setText("Android");
		return android;
	}
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	// @Override
	// public void onCreate(Bundle savedInstanceState) {
	// onCreate233(savedInstanceState);
	// }
	/*
	 * (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initComponent();
		// loadTabs();
		changeByRadio();
		doCreateFakeDataChiTieu();
		loadTroGiupWebView();
		// doCreateFakeDataChiTieu2();
		displayListView();
		date=new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year,
			    int monthOfYear, int dayOfMonth) {
				// TODO Auto-generated method stub
				calendar.set(Calendar.YEAR,year);
				calendar.set(Calendar.MONTH,monthOfYear);
				calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
				updateLabel();
			}
		};
		ct_txt_ngayTao
		    .setOnClickListener(new OnClickListener() {
			    @Override
			    public void onClick(View v) {
				    // TODO Auto-generated method stub
				    new DatePickerDialog(tabAndroid.getContext(),
				        date,calendar.get(Calendar.YEAR),calendar
				            .get(Calendar.MONTH),calendar
				            .get(Calendar.DAY_OF_MONTH)).show();
			    }
		    });
		// Update cot gia tri date
		updateLabel();
		// su kien thay doi menu theo radio button : Thu | Chi
		changeByRadio();
		// An icon
		ct_lv_thuChi
		    .setOnItemClickListener(new OnItemClickListener() {
			    @Override
			    public void onItemClick(AdapterView<?> parent,
			        View view, int position, long id) {
				    CtChiTieu item=ctChiTieuAdapter
				        .getItem(position);
				    ct_txt_tenChiTieu.setText(item.getTenChiTieu());
				    ct_txt_tienChiTieu.setText(item
				        .getTienChiTieu()+"");
				    calendar.setTime(item.getNgayTao());
				    updateLabel();
				    // ct_txt_ngayTao.setText(DateUtils.yyyyMMdd(date)+"");
				    if(item.getIdDanhMuc().getTrangThai()==0) {
					    ct_rd_thu.setSelected(true);
				    } else {
					    ct_rd_chi.setSelected(true);
				    }
			    }
		    });
		// return tabAndroid;
	}
	private void updateLabel() {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd",
		    Locale.US);
		ct_txt_ngayTao.setText(sdf.format(calendar.getTime()));
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id=item.getItemId();
		if(id==R.id.action_settings) { return true; }
		return super.onOptionsItemSelected(item);
	}
	public void ct_rd_chiOnClicked(View view) {
		changeByRadio();
	}
	public void ct_rd_thuOnClicked(View view) {
		changeByRadio();
	}
	// 08-04 07:43:54.772: E/Chitieu :(1049): CtChiTieu [idChiTieu=92,
	// tenChiTieu=Thu 91, idDanhMuc=An sinh xã hội, tienChiTieu=50000.0,
	// trangThai=0, ghiChu=An sinh xã hội, ngayTao=Sat Jul 18 00:00:00 GMT+07:00
	// 2015, ngaySua=null]
	/**
	 * initialization component
	 */
	public void initComponent() {
		// initialization component source
		ct_sp_tenDanhMuc=(Spinner) tabAndroid
		    .findViewById(R.id.ct_sp_tenDanhMuc);
		ct_rd_thu=(RadioButton) tabAndroid
		    .findViewById(R.id.ct_rd_thu);
		ct_rd_chi=(RadioButton) tabAndroid
		    .findViewById(R.id.ct_rd_chi);
		ct_lv_thuChi=(ListView) tabAndroid
		    .findViewById(R.id.ct_lv_thuChi);
		ct_txt_tenChiTieu=(TextView) tabAndroid
		    .findViewById(R.id.ct_txt_tenChiTieu);
		ct_txt_tienChiTieu=(TextView) tabAndroid
		    .findViewById(R.id.ct_txt_tienChiTieu);
		// Hien thi ket qua tong thu, chi, tong chu chi
		ct_txt_TongThu=(TextView) tabAndroid
		    .findViewById(R.id.ct_txt_TongThu);
		ct_txt_TongChi=(TextView) tabAndroid
		    .findViewById(R.id.ct_txt_TongChi);
		ct_txt_TongThuChi=(TextView) tabAndroid
		    .findViewById(R.id.ct_txt_TongThuChi);
		// ------
		webView=(WebView) tabAndroid
		    .findViewById(R.id.ct_tg_wv_trogiup);
		ct_txt_ngayTao=(EditText) tabAndroid
		    .findViewById(R.id.ct_txt_ngayTao);
		// initialization data source
		ctDanhMucDataSource=new CtDanhMucDataSource(
		    tabAndroid.getContext());
		ctDanhMucDataSource.open();
		ctChiTieuDataSource=new CtChiTieuDataSource(
		    tabAndroid.getContext());
		ctChiTieuDataSource.open();
	}
	/**
	 * Su kien thay doi theo radio button thu chi
	 */
	private void changeByRadio() {
		lstCtDM.removeAll(lstCtDM);
		if(ct_rd_thu.isChecked()) {
			lstCtDM=ctDanhMucDataSource.getAllByStatus(0);
			ct_txt_tenChiTieu
			    .setText(getStringRes(R.string.ct_ct_thu)+" "
			        +listThuChi.size());
		} else {
			lstCtDM=ctDanhMucDataSource.getAllByStatus(1);
			ct_txt_tenChiTieu
			    .setText(getStringRes(R.string.ct_ct_chi)+" "
			        +listThuChi.size());
		}
		ctDanhMucAdapter=new CtDanhMucAdapter(
		    tabAndroid.getContext(),layout.simple_spinner_item,
		    (ArrayList<CtDanhMuc>) lstCtDM);
		// ctDanhMucAdapter=new CtDanhMucAdapter(
		// MainActivity.this,R.layout.item_sp_danhmuc,
		// (ArrayList<CtDanhMuc>) lstCtDM);
		ct_sp_tenDanhMuc.setAdapter(ctDanhMucAdapter);
	}
	/**
	 * Su kien click nut them
	 * 
	 * @param v
	 */
	public void btnClickBtThem(View v) {
		// TimePicker ct_t_gioTao=(TimePicker) findViewById(R.id.ct_t_gioTao);
		int status=1;
		if(ct_rd_thu.isChecked()) status=0;
		CtDanhMuc ctDm=ctDanhMucAdapter
		    .getItem(ct_sp_tenDanhMuc.getSelectedItemPosition());
		ctDm.setTrangThai(status);
		// Add data
		CtChiTieu chiTieu=new CtChiTieu(listThuChi.size()+1,
		    ctDm,ct_txt_tenChiTieu.getText().toString(),
		    Float.parseFloat(ct_txt_tienChiTieu.getText()
		        .toString()),2);
		chiTieu.setNgayTao(DateUtils
		    .stringToDate(ct_txt_ngayTao.getText().toString()));
		listThuChi.add(chiTieu);
		ctChiTieuDataSource.create(chiTieu);
		// ctChiTieuAdapter.notifyDataSetChanged();
		displayListView();
		changeByRadio();
	}
	public void displayListView() {
		listThuChi.removeAll(listThuChi);
		// Collections.sort(listThuChi);
		listThuChi=ctChiTieuDataSource.getAll();
		if(listThuChi!=null&&listThuChi.size()>0) {
			tongChi=tongThu=tongThuChi=0f;
			for(CtChiTieu item:listThuChi) {
				if(item.getIdDanhMuc()!=null) {
					if(item.getIdDanhMuc().getTrangThai()==0)
						tongThu+=item.getTienChiTieu();
					else tongChi+=item.getTienChiTieu();
					tongThuChi=tongThu-tongChi;
				}
			}
		}
		// Hien thi ket qua tong thu, chi, tong chu chi
		ct_txt_TongThu.setText(getStringRes(R.string.ct_ct_thu)
		    +": "+float2String2f(tongThu));
		ct_txt_TongChi.setText(getStringRes(R.string.ct_ct_chi)
		    +": "+float2String2f(tongChi));
		ct_txt_TongThuChi
		    .setText(getStringRes(R.string.ct_ct_thu_chi)+": "
		        +float2String2f(tongThuChi));
		// Load danh sach thu chi
		// ct_lv_thuChi=(ListView) findViewById(R.id.ct_lv_thuChi);
		ctChiTieuAdapter=new CtChitieuAdapter(
		    tabAndroid.getContext(),R.layout.item_lv_thuchi,
		    (ArrayList<CtChiTieu>) listThuChi);
		ct_lv_thuChi.setAdapter(ctChiTieuAdapter);
		// openChart();
	}
	/**
	 * Hien thi tro giup webview
	 */
	@SuppressLint("SetJavaScriptEnabled")
	private void loadTroGiupWebView() {
		// webView.loadUrl("http://developer.android.com");
		class JsObject {
			@JavascriptInterface
			public String toString() {
				return "injectedObject";
			}
		}
		webView.addJavascriptInterface(new JsObject(),
		    "injectedObject");
		// webView.loadUrl("http://developer.android.com");
		webView.loadUrl("http://weavesilk.com");
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new MyWebViewCient());
	}
	public String getStringRes(int id) {
		return tabAndroid.getContext().getResources()
		    .getString(id);
	}
	public int getColorRes(int id) {
		return tabAndroid.getContext().getResources()
		    .getColor(id);
	}
	public Drawable getIconRes(int id) {
		return tabAndroid.getContext().getResources()
		    .getDrawable(id);
	}
	/**
	 * Lay gia tri Drawable tu System
	 * 
	 * @param id
	 *          String system
	 * @return String tu he thong
	 */
	public String getStringSys(int id) {
		return Resources.getSystem().getString(id);
	}
	/**
	 * Lay gia tri Drawable tu System
	 * 
	 * @param id
	 *          drawable id
	 * @return Drawable
	 */
	public Drawable getDrawalbeSys(int id) {
		return Resources.getSystem().getDrawable(id);
	}
	/**
	 * Lay gia tri Drawable tu Resource
	 * 
	 * @param id
	 *          drawable id
	 * @return Drawable
	 */
	public Drawable getDrawalbeRs(int id) {
		return Resources.getSystem().getDrawable(id);
	}
	/**
	 * Lay gia tri mang tu Resource
	 * 
	 * @param id
	 *          array id
	 * @return String[]
	 */
	public String[] getStringArrayRes(int id) {
		return tabAndroid.getContext().getResources()
		    .getStringArray(id);
	}
	/**
	 * Tao giu lieu test
	 */
	private void doCreateFakeDataChiTieu() {
		listThuChi=ctChiTieuDataSource.getAll();
		if(listThuChi.size()<=0) {
			for(int i=1;i<20;i++) {
				// int Có 0: Thu | 1 : Chi : 2 : Xoa
				CtChiTieu chiTieu=new CtChiTieu(
				    listThuChi.size()+1,new CtDanhMuc(i,null,1),
				    "Chitieu"+i,i*123f,2);
				chiTieu.setNgayTao(DateUtils.getNowDate());
				ctChiTieuDataSource.create(chiTieu);
			}
			listThuChi=ctChiTieuDataSource.getAll();
		}
	}
	// Click to del
	public void ct_lv_col_iconDelThuChiOnClick(View view) {
		CtChiTieu item=ctChiTieuAdapter.getItem(ct_lv_thuChi
		    .getSelectedItemPosition());
		Toast.makeText(tabAndroid.getContext(),
		    "Del"+item.toString(),Toast.LENGTH_SHORT).show();
		ctChiTieuDataSource.delete(item);
		displayListView();
	}
	/*
	 * Load cac tab vao trong tabhost
	 */
	public void loadTabs() {
		// Lấy Tabhost id ra trước (cái này của built - in android
		final TabHost tabhost=(TabHost) tabAndroid
		    .findViewById(android.R.id.tabhost);
		// gọi lệnh setup
		tabhost.setup();
		TabHost.TabSpec spec;
		// Tạo tab1
		spec=tabhost.newTabSpec("t1");
		spec.setContent(R.id.tab1);
		TextView v1=new TextView(tabAndroid.getContext());
		v1.setText(getStringRes(R.string.ct_ct_thu_chi));
		spec.setIndicator(getStringRes(R.string.ct_ct_thu_chi));
		tabhost.addTab(spec);
		// Tạo tab2
		spec=tabhost.newTabSpec("t2");
		spec.setContent(R.id.tab2);
		spec.setIndicator(getStringRes(R.string.ct_tk_taikhoan));
		tabhost.addTab(spec);
		// Tạo tab3
		spec=tabhost.newTabSpec("t3");
		spec.setContent(R.id.tab3);
		spec.setIndicator(getStringRes(R.string.ct_dl_dulieu));
		tabhost.addTab(spec);
		// Tạo tab3
		spec=tabhost.newTabSpec("t4");
		spec.setContent(R.id.tab4);
		spec.setIndicator(getStringRes(R.string.ct_pt_phantich));
		tabhost.addTab(spec);
		// Tạo tab3
		spec=tabhost.newTabSpec("t5");
		spec.setContent(R.id.tab5);
		spec.setIndicator(getStringRes(R.string.ct_tg_trogiup));
		tabhost.addTab(spec);
		// Thiết lập tab mặc định được chọn ban đầu là tab 0
		for(int i=0;i<tabhost.getTabWidget().getChildCount();i++) {
			tabhost
			    .getTabWidget()
			    .getChildAt(i)
			    .setBackgroundColor(
			        getColorRes(R.color.WhiteSmoke));
			tabhost.getTabWidget().getChildAt(i)
			    .getLayoutParams().height=60;
		}
		tabhost.setCurrentTab(0);
		tabhost.getTabWidget()
		    .getChildAt(tabhost.getCurrentTab())
		    .setBackgroundColor(getColorRes(R.color.LightBlue));
		// Ví dụ tab1 chưa nhập thông tin xong mà lại qua tab 2 thì báo...
		tabhost
		    .setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			    public void onTabChanged(String arg0) {
				    for(int i=0;i<tabhost.getTabWidget()
				        .getChildCount();i++) {
					    tabhost.getTabWidget().getChildAt(i)
					        .getLayoutParams().height=60;
					    tabhost
					        .getTabWidget()
					        .getChildAt(i)
					        .setBackgroundColor(
					            getColorRes(R.color.WhiteSmoke));
				    }
				    tabhost
				        .getTabWidget()
				        .getChildAt(tabhost.getCurrentTab())
				        .setBackgroundColor(
				            getColorRes(R.color.LightBlue));
			    }
		    });
	}
	class MyWebViewCient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view,
		    String url) {
			if(Uri.parse(url).getHost().equals("weavesilk.com"))
			  return false;
			Intent intent=new Intent(Intent.ACTION_VIEW,
			    Uri.parse(url));
			startActivity(intent);
			return true;
		}
	}
	// ==========================Draw pie chart========================
	private View	mPieChart;
	protected void onCreate2(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_main);
		Button btnPieChart=(Button) tabAndroid
		    .findViewById(R.id.btnPieChart);
		OnClickListener clickEvent=new OnClickListener() {
			@Override
			public void onClick(View v) {
				openChart();
			}
		};
		btnPieChart.setOnClickListener(clickEvent);
	}
	private void openChart() {
		String[] categories=new String[]{
		    getStringRes(R.string.ct_ct_thu),
		    getStringRes(R.string.ct_ct_chi),
		    getStringRes(R.string.ct_ct_thu_chi)};
		// double[] proportion={0.4,0.3,0.3};
		// double[] proportion={tongThu,tongChi};
		float perscentThu=tongThu/tongThuChi*100;
		float perscentChi=tongChi/tongThuChi*100;
		// double[] proportion={perscentThu,perscentChi};
		double[] proportion={tongThu,tongChi};
		int[] color={Color.BLUE,Color.RED,Color.YELLOW};
		CategorySeries expenseSeries=new CategorySeries(
		    getStringRes(R.string.ct_ct_thu_chi));
		// Them ten va gia tri cho tung khoan chi
		for(int i=0;i<proportion.length;i++) {
			expenseSeries.add(categories[i],proportion[i]);
		}
		DefaultRenderer defaultRenderer=new DefaultRenderer();
		for(int i=0;i<proportion.length;i++) {
			SimpleSeriesRenderer seriesRenderer=new SimpleSeriesRenderer();
			seriesRenderer.setColor(color[i]);
			seriesRenderer.setDisplayChartValues(true);
			seriesRenderer.setChartValuesTextSize(40);
			// Them mau cho background
			defaultRenderer.setBackgroundColor(Color.GRAY);
			defaultRenderer.setApplyBackgroundColor(true);
			// Tao ra tung slice cua pie chart
			defaultRenderer.addSeriesRenderer(seriesRenderer);
		}
		defaultRenderer
		    .setChartTitle(getStringRes(R.string.ct_ct_thu_chi));
		defaultRenderer.setChartTitleTextSize(60);
		defaultRenderer.setLabelsTextSize(30);
		defaultRenderer.setLegendTextSize(30);
		defaultRenderer.setDisplayValues(true);
		defaultRenderer.setZoomButtonsVisible(false);
		LinearLayout chartContainer=(LinearLayout) tabAndroid
		    .findViewById(R.id.chart);
		// remove view before painting the chart
		chartContainer.removeAllViews();
		// Ve pie chart
		// mPieChart=ChartFactory
		// .getPieChartView(this.getBaseContext(),
		// expenseSeries,defaultRenderer);
		// Add view to the linearlayout
		chartContainer.addView(mPieChart);
	}
	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.main,menu);
	// return true;
	// }
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// // Handle action bar item clicks here. The action bar will
	// // automatically handle clicks on the Home/Up button, so long
	// // as you specify a parent activity in AndroidManifest.xml.
	// int id=item.getItemId();
	// if(id==R.id.action_settings) { return true; }
	// return super.onOptionsItemSelected(item);
	// }
}
