package com.aptech.chitieu.util;

/**
 * 
 * Title : Ho tro thao tac voi kieu so <br/>
 * Description : Ho tro thao tac voi kieu so<br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Jul 14, 2015 10:13:02 AM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Jul 14, 2015 10:13:02 AM
 * 
 */
public class DigestUtil {
	/**
	 * Doi so float sang chuoi format theo 2 so thap phan
	 * 
	 * @param f
	 *            float number
	 * @return gia tri chuoi cua so float
	 */
	public static String float2String2f(float f) {
		return String.format("%.2f", f);
	}
}
