package com.aptech.chitieu.util;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

/**
 * 
 * Lay du lieu tu he thong ho tro
 * 
 * Title : <br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * 
 * Create on Jul 14, 2015 10:06:56 AM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Jul 14, 2015 10:06:56 AM
 * 
 */
public class ResUtil {
	/**
	 * Lay chuoi tu nguon cua he thong
	 * 
	 * @param id
	 *            idChuoi Resource
	 * @return chuoi he thong ho tro
	 */
	public static String getChuoi(int id) {
		return Resources.getSystem().getString(id);
	}

	/**
	 * Lay Drawable tu he thong ho tro
	 * 
	 * @param idDrawable
	 * @return Drawable
	 */
	public static Drawable getIcon(int id) {
		return Resources.getSystem().getDrawable(id);
	}
}
