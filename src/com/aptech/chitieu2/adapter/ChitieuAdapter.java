package com.aptech.chitieu2.adapter;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.aptech.chitieu.R;
import com.aptech.chitieu.entity.ChiTieu;
import com.aptech.chitieu2.dao.DanhMucDao;
import com.squareup.picasso.Picasso;

/**
 * Title : <br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 17, 2015 3:45:45 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Jul 2, 2015 : 5:00:58 PM
 */
@SuppressLint("InflateParams")
public class ChitieuAdapter extends ArrayAdapter<ChiTieu> {
	Context mContext;
	ArrayList<ChiTieu> mLstChiTieu = new ArrayList<ChiTieu>();
	DanhMucDao danhMucDao = null;

	public ChitieuAdapter(Context context, int resource,
			ArrayList<ChiTieu> objects) {
		super(context, resource, objects);
		this.mContext = context;
		this.mLstChiTieu = objects;
		this.danhMucDao = new DanhMucDao(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		ViewHolder viewHolder = null;
		if (rowView == null) {
			LayoutInflater inflate = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflate.inflate(R.layout.item_lv_thuchi, null);
			viewHolder = new ViewHolder();
			viewHolder.ct_lv_col_tenChiTieu = (TextView) rowView
					.findViewById(R.id.ct_lv_col_tenChiTieu);
			viewHolder.ct_lv_col_ngayTao = (TextView) rowView
					.findViewById(R.id.ct_lv_col_ngayTao);
			viewHolder.ct_lv_col_iconThuChi = (ImageView) rowView
					.findViewById(R.id.ct_lv_col_iconThuChi);
			// viewHolder.ct_lv_col_iconDelThuChi=(ImageButton) rowView
			// .findViewById(R.id.ct_lv_col_iconDelThuChi);
			viewHolder.ct_lv_col_TienChiTieu = (TextView) rowView
					.findViewById(R.id.ct_lv_col_TienChiTieu);
			rowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		ChiTieu chiTieu = mLstChiTieu.get(position);
		viewHolder.ct_lv_col_tenChiTieu.setText(chiTieu.getChiTieuTen());
		if (chiTieu.getChiTieuNgayTao() != null)
			viewHolder.ct_lv_col_ngayTao.setText(chiTieu.getChiTieuNgayTao());
		viewHolder.ct_lv_col_TienChiTieu.setText(String.valueOf(chiTieu
				.getChiTieuSoTien()));
		if (chiTieu.getDanhMucId() != null & chiTieu.getDanhMucId() != 0) {
			// if(danhMucDao.findByPK(chiTieu.getDanhMucId())
			// .getDanhMucTrangThai()==0)
			// viewHolder.ct_lv_col_iconThuChi
			// .setImageResource(R.drawable.ic_hd_add);
			// else viewHolder.ct_lv_col_iconThuChi
			// .setImageResource(R.drawable.ic_hd_sub);
			if (danhMucDao.findByPK(chiTieu.getDanhMucId())
					.getDanhMucTrangThai() == 0)
				Picasso.with(mContext)
						.load("file:///android_asset/images/check.png"
						// +
						// hinhAnhDao.findByPK(danhMuc.getHinhAnhId()).getHinhAnhDuongDan()
						).into(viewHolder.ct_lv_col_iconThuChi);
			else
				Picasso.with(mContext)
						.load("file:///android_asset/images/cart.png"
						// +
						// hinhAnhDao.findByPK(danhMuc.getHinhAnhId()).getHinhAnhDuongDan()
						).into(viewHolder.ct_lv_col_iconThuChi);
		}
		return rowView;
	}

	static class ViewHolder {
		TextView ct_lv_col_tenChiTieu;
		TextView ct_lv_col_ngayTao;
		TextView ct_lv_col_TienChiTieu;
		ImageView ct_lv_col_iconThuChi;
		ImageButton ct_lv_col_iconDelThuChi;
	}
}
