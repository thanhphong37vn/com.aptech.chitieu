package com.aptech.chitieu2.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.aptech.chitieu.R;
import com.aptech.chitieu.entity.ChiTieu;
import com.aptech.chitieu.entity.DanhMuc;
import com.aptech.chitieu2.dao.ChiTieuDao;
import com.aptech.chitieu2.dao.DanhMucDao;
import com.aptech.chitieu2.dao.TaiKhoanDao;

/**
 * Title : Adapter Layer<br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 11, 2015 10:40:05 AM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 11, 2015 10:40:05 AM
 */
public class DanhMucAdapter extends ArrayAdapter<DanhMuc> {
	Context context;
	ArrayList<DanhMuc> mLstDanhMuc = new ArrayList<DanhMuc>();

	public DanhMucAdapter(Context context, int resource,
			ArrayList<DanhMuc> objects) {
		super(context, resource, objects);
		this.context = context;
		this.mLstDanhMuc = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		ViewHolder viewHolder = null;
		if (rowView == null) {
			LayoutInflater inflate = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflate.inflate(R.layout.item_sp_danhmuc, null);
			viewHolder = new ViewHolder();
			viewHolder.sp_col_tv_tenDanhMuc = (TextView) rowView
					.findViewById(R.id.sp_col_tv_tenDanhMuc);
			// viewHolder.sp_col_iconDanhMuc=(ImageView) rowView
			// .findViewById(R.id.sp_col_iconDanhMuc);
			rowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		DanhMuc danhMuc = mLstDanhMuc.get(position);
		viewHolder.sp_col_tv_tenDanhMuc.setText(danhMuc.getDanhMucTen());
		// if(DanhMuc.getTrangThai()==0)
		// viewHolder.sp_col_iconDanhMuc
		// .setImageResource(R.drawable.ic_hd_add);
		// else viewHolder.sp_col_iconDanhMuc
		// .setImageResource(R.drawable.ic_hd_sub);
		return rowView;
	}

	double tongChi, tongThu, tongThuChi;

	public Double getTongThuChi(int danhMucId) {

		tongChi = tongThu = tongThuChi = 0d;
		TaiKhoanDao taiKhoanDao = new TaiKhoanDao(context);
		ChiTieuDao chiTieuDao = new ChiTieuDao(context);
		DanhMucDao danhMucDao = new DanhMucDao(context);
		List<ChiTieu> lstChiTieu = chiTieuDao.findBy(ChiTieu.DanhMucId,
				danhMucId);
		if (lstChiTieu != null && lstChiTieu.size() > 0) {
			for (ChiTieu item : lstChiTieu) {
				if (item.getDanhMucId() != null && item.getDanhMucId() != 0) {
					DanhMuc dm = danhMucDao.findByPK(item.getDanhMucId());
					if (dm.getDanhMucTrangThai() == 0)
						tongThu += item.getChiTieuSoTien();
					else
						tongChi += item.getChiTieuSoTien();
					tongThuChi = tongThu - tongChi;
				}
			}
		}
		return tongThuChi;
	}

	static class ViewHolder {
		TextView sp_col_tv_tenDanhMuc;
		ImageView sp_col_iconDanhMuc;
	}
}
