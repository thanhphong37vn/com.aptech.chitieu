package com.aptech.chitieu2.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.aptech.chitieu.R;
import com.aptech.chitieu.entity.ChiTieu;
import com.aptech.chitieu.entity.DanhMuc;
import com.aptech.chitieu2.dao.ChiTieuDao;
import com.aptech.chitieu2.dao.DanhMucDao;
import com.aptech.chitieu2.dao.HinhAnhDao;
import com.aptech.chitieu2.dao.TaiKhoanDao;
import com.squareup.picasso.Picasso;

/**
 * Title : Adapter Layer<br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 11, 2015 10:40:05 AM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 11, 2015 10:40:05 AM
 */
public class DanhMucAdapter2 extends ArrayAdapter<DanhMuc> {
	Context mContext;
	ArrayList<DanhMuc> mLstDanhMuc = new ArrayList<DanhMuc>();
	HinhAnhDao hinhAnhDao = null;

	public DanhMucAdapter2(Context context, int resource,
			ArrayList<DanhMuc> objects) {
		super(context, resource, objects);
		this.mContext = context;
		this.mLstDanhMuc = objects;
		this.hinhAnhDao = new HinhAnhDao(mContext);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		ViewHolder viewHolder = null;
		if (rowView == null) {
			LayoutInflater inflate = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflate.inflate(R.layout.item_lv_danhmuc, null);
			viewHolder = new ViewHolder();
			viewHolder.ct_dl_lv_col_tenChiTieu = (TextView) rowView
					.findViewById(R.id.ct_dl_lv_col_tenChiTieu);
			viewHolder.ct_dl_lv_col_iconThuChi = (ImageView) rowView
					.findViewById(R.id.ct_dl_lv_col_iconThuChi);
			viewHolder.ct_dl_lv_col_TienChiTieu = (TextView) rowView
					.findViewById(R.id.ct_dl_lv_col_TienChiTieu);
			rowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		DanhMuc danhMuc = mLstDanhMuc.get(position);
		viewHolder.ct_dl_lv_col_tenChiTieu.setText(danhMuc.getDanhMucTen());
		viewHolder.ct_dl_lv_col_TienChiTieu.setText(getTongThuChi(danhMuc
				.getDanhMucId()) + "");
		// if (danhMuc.getHinhAnhId() == null)
		// danhMuc.setDanhMucId(hinhAnhDao.findAll().get(0).getHinhAnhId());

		// Picasso.with(mContext).load("file:///android_asset/images/x.png"
		// // + hinhAnhDao.findByPK(danhMuc.getHinhAnhId()).getHinhAnhDuongDan()
		// ).into(viewHolder.ct_dl_lv_col_iconThuChi);

		if (danhMuc.getDanhMucTrangThai() == 0)
			Picasso.with(mContext)
					.load("file:///android_asset/images/check.png"
					// +
					// hinhAnhDao.findByPK(danhMuc.getHinhAnhId()).getHinhAnhDuongDan()
					).into(viewHolder.ct_dl_lv_col_iconThuChi);
		else
			Picasso.with(mContext).load("file:///android_asset/images/cart.png"
			// +
			// hinhAnhDao.findByPK(danhMuc.getHinhAnhId()).getHinhAnhDuongDan()
					).into(viewHolder.ct_dl_lv_col_iconThuChi);
		return rowView;
	}

	double tongChi, tongThu, tongThuChi;

	public Double getTongThuChi(int danhMucId) {

		tongChi = tongThu = tongThuChi = 0d;
		TaiKhoanDao taiKhoanDao = new TaiKhoanDao(mContext);
		ChiTieuDao chiTieuDao = new ChiTieuDao(mContext);
		DanhMucDao danhMucDao = new DanhMucDao(mContext);
		List<ChiTieu> lstChiTieu = chiTieuDao.findBy(ChiTieu.DanhMucId,
				danhMucId);
		if (lstChiTieu != null && lstChiTieu.size() > 0) {
			for (ChiTieu item : lstChiTieu) {
				if (item.getDanhMucId() != null && item.getDanhMucId() != 0) {
					DanhMuc dm = danhMucDao.findByPK(item.getDanhMucId());
					if (dm.getDanhMucTrangThai() == 0)
						tongThu += item.getChiTieuSoTien();
					else
						tongChi += item.getChiTieuSoTien();
					tongThuChi = tongThu - tongChi;
				}
			}
		}
		return tongThuChi;
	}

	static class ViewHolder {
		TextView ct_dl_lv_col_tenChiTieu;
		TextView ct_dl_lv_col_TienChiTieu;
		ImageView ct_dl_lv_col_iconThuChi;
	}
}
