package com.aptech.chitieu2.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.aptech.chitieu.R;
import com.aptech.chitieu.entity.HinhAnh;
import com.squareup.picasso.Picasso;

/**
 * Title : Adapter Layer<br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 11, 2015 10:40:05 AM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 11, 2015 10:40:05 AM
 */
public class HinhAnhGridViewAdapter extends ArrayAdapter<HinhAnh> {
	Context context;
	ArrayList<HinhAnh> mLstHinhAnh = new ArrayList<HinhAnh>();

	public HinhAnhGridViewAdapter(Context context, int resource,
			ArrayList<HinhAnh> objects) {
		super(context, resource, objects);
		this.context = context;
		this.mLstHinhAnh = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		ViewHolder viewHolder = null;
		if (rowView == null) {
			LayoutInflater inflate = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflate.inflate(R.layout.item_gr_hinhanh, null);
			viewHolder = new ViewHolder();
			viewHolder.ct_ha_hinhAnhTen = (TextView) rowView
					.findViewById(R.id.ct_ha_hinhAnhTen);
			viewHolder.ct_ha_hinhAnh = (ImageView) rowView
					.findViewById(R.id.ct_ha_hinhAnh);
			rowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		HinhAnh hinhAnh = mLstHinhAnh.get(position);
		viewHolder.ct_ha_hinhAnhTen.setText(hinhAnh.getHinhAnhTen());
		// viewHolder.ct_ha_hinhAnh.setImageBitmap(getBitmapFromAsset(hinhAnh
		// .getHinhAnhDuongDan()));
		Picasso.with(context)
				.load("file:///android_asset" + hinhAnh.getHinhAnhDuongDan())
				.into(viewHolder.ct_ha_hinhAnh);

		return rowView;
	}

	private Bitmap getBitmapFromAsset(String strName) {
		AssetManager assetManager = context.getAssets();
		InputStream istr = null;
		try {
			istr = assetManager.open(strName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Bitmap bitmap = BitmapFactory.decodeStream(istr);
		return bitmap;
	}

	static class ViewHolder {
		TextView ct_ha_hinhAnhTen;
		ImageView ct_ha_hinhAnh;
	}
}
