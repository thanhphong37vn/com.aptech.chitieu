package com.aptech.chitieu2.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.aptech.chitieu.R;
import com.aptech.chitieu.entity.ChiTieu;
import com.aptech.chitieu.entity.DanhMuc;
import com.aptech.chitieu.entity.TaiKhoan;
import com.aptech.chitieu2.dao.ChiTieuDao;
import com.aptech.chitieu2.dao.DanhMucDao;
import com.aptech.chitieu2.dao.TaiKhoanDao;
import com.squareup.picasso.Picasso;

/**
 * Title : Adapter Layer<br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 11, 2015 10:40:05 AM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 11, 2015 10:40:05 AM
 */
public class TaiKhoanAdapter extends ArrayAdapter<TaiKhoan> {
	Context context;
	ArrayList<TaiKhoan> mLstTaiKhoan = new ArrayList<TaiKhoan>();

	public TaiKhoanAdapter(Context context, int resource,
			ArrayList<TaiKhoan> objects) {
		super(context, resource, objects);
		this.context = context;
		this.mLstTaiKhoan = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		ViewHolder viewHolder = null;
		if (rowView == null) {
			LayoutInflater inflate = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflate.inflate(R.layout.item_lv_taikhoan, null);
			viewHolder = new ViewHolder();
			viewHolder.ct_tk_lv_col_tenChiTieu = (TextView) rowView
					.findViewById(R.id.ct_tk_lv_col_tenChiTieu);
			viewHolder.ct_tk_lv_col_TienChiTieu = (TextView) rowView
					.findViewById(R.id.ct_tk_lv_col_TienChiTieu);
			viewHolder.ct_tk_lv_col_iconThuChi = (ImageView) rowView
					.findViewById(R.id.ct_tk_lv_col_iconThuChi);
			rowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		TaiKhoan TaiKhoan = mLstTaiKhoan.get(position);
		viewHolder.ct_tk_lv_col_tenChiTieu.setText(TaiKhoan.getTaiKhoanTen());
		viewHolder.ct_tk_lv_col_TienChiTieu.setText(getTongThuChi(TaiKhoan
				.getTaiKhoanId()) + ""); // if(TaiKhoan.getTrangThai()==0)
		Picasso.with(context).load("file:///android_asset/images/money.png")
				.into(viewHolder.ct_tk_lv_col_iconThuChi);

		return rowView;
	}

	double tongChi, tongThu, tongThuChi;

	public Double getTongThuChi(int taiKhoanId) {

		tongChi = tongThu = tongThuChi = 0d;
		TaiKhoanDao taiKhoanDao = new TaiKhoanDao(context);
		ChiTieuDao chiTieuDao = new ChiTieuDao(context);
		DanhMucDao danhMucDao = new DanhMucDao(context);
		List<ChiTieu> lstChiTieu = chiTieuDao.findBy(ChiTieu.TaiKhoanId,
				taiKhoanId);
		if (lstChiTieu != null && lstChiTieu.size() > 0) {
			for (ChiTieu item : lstChiTieu) {
				if (item.getDanhMucId() != null && item.getDanhMucId() != 0) {
					DanhMuc dm = danhMucDao.findByPK(item.getDanhMucId());
					if (dm.getDanhMucTrangThai() == 0)
						tongThu += item.getChiTieuSoTien();
					else
						tongChi += item.getChiTieuSoTien();
					tongThuChi = tongThu - tongChi;
				}
			}
		}
		return tongThuChi;
	}

	static class ViewHolder {
		TextView ct_tk_lv_col_tenChiTieu;
		TextView ct_tk_lv_col_TienChiTieu;
		ImageView ct_tk_lv_col_iconThuChi;
	}
}
