package com.aptech.chitieu2.adapter;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.aptech.chitieu.R;
import com.aptech.chitieu.entity.TaiKhoan;

/**
 * Title : Adapter Layer<br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 11, 2015 10:40:05 AM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 11, 2015 10:40:05 AM
 */
public class TaiKhoanSpinnerAdapter extends ArrayAdapter<TaiKhoan> {
	Context mContext;
	ArrayList<TaiKhoan> mLstTaiKhoan = new ArrayList<TaiKhoan>();

	public TaiKhoanSpinnerAdapter(Context context, int resource,
			ArrayList<TaiKhoan> objects) {
		super(context, resource, objects);
		this.mContext = context;
		this.mLstTaiKhoan = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		ViewHolder viewHolder = null;
		if (rowView == null) {
			LayoutInflater inflate = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflate.inflate(R.layout.item_sp_taikhoan, null);
			viewHolder = new ViewHolder();
			viewHolder.sp_col_tv_tenTaiKhoan = (TextView) rowView
					.findViewById(R.id.sp_col_tv_tenTaiKhoan);
			// viewHolder.sp_col_iconTaiKhoan=(ImageView) rowView
			// .findViewById(R.id.sp_col_iconTaiKhoan);
			rowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		TaiKhoan TaiKhoan = mLstTaiKhoan.get(position);
		viewHolder.sp_col_tv_tenTaiKhoan.setText(TaiKhoan.getTaiKhoanTen());
		// if(TaiKhoan.getTrangThai()==0)
		// viewHolder.sp_col_iconTaiKhoan
		// .setImageResource(R.drawable.ic_hd_add);
		// else viewHolder.sp_col_iconTaiKhoan
		// .setImageResource(R.drawable.ic_hd_sub);
		return rowView;
	}

	static class ViewHolder {
		TextView sp_col_tv_tenTaiKhoan;
		ImageView ct_dl_lv_col_iconThuChi;
	}
}
