package com.aptech.chitieu2.dao;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.oni.webapp.enumeration.SortDir;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.aptech.chitieu.entity.ChiTieu;
import com.aptech.chitieu2.dao.GenericDaoI;
/**
 * Title :ChiTieuDao <br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 10, 2015 2:41:26 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 10, 2015 2:41:26 PM
 */
public class ChiTieuDao extends DatasoureDao implements
    GenericDaoI<ChiTieu,Integer> {
	/**
	 * @param context
	 */
	public ChiTieuDao(Context context) {
		super(context);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#countAll()
	 */
	@Override
	public long countAll() {
		return database.compileStatement(
		    "select * from "+ChiTieu.ChiTieu)
		    .simpleQueryForLong();
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#createEntity()
	 */
	@Override
	public ChiTieu createEntity() {
		return new ChiTieu(createPK());
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#createPK()
	 */
	@Override
	public Integer createPK() {
		return (int) (database
		    .compileStatement(
		        "SELECT ChiTieuId  FROM ChiTieu order by ChiTieuId DESC LIMIT 1;")
		    .simpleQueryForLong()+1);
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#delete(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	public void delete(ChiTieu entity) {
		deleteByPK(entity.getId());
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public void deleteBy(String fieldName,
	    Collection<?> values) {
		database.delete(ChiTieu.ChiTieu,fieldName+" in ("
		    +values+")",null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public void deleteBy(String fieldName, Object value) {
		database.delete(ChiTieu.ChiTieu,fieldName+" = "+value,
		    null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPK(java.io.Serializable)
	 */
	@Override
	public void deleteByPK(Integer pk) {
		database.delete(ChiTieu.ChiTieu,ChiTieu.ChiTieuId+" = "
		    +pk,null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPKs(java.util.Collection)
	 */
	@Override
	public void deleteByPKs(Collection<Integer> pks) {
		database.delete(ChiTieu.ChiTieu,ChiTieu.ChiTieuId
		    +" in ("+pks+")",null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#find(int, int, java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<ChiTieu> find(int offset, int size,
	    String sortKey, SortDir sortDir) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll()
	 */
	@Override
	public List<ChiTieu> findAll() {
		List<ChiTieu> lstCtChiTieu=new ArrayList<ChiTieu>();
		Cursor cursor=database.query(ChiTieu.ChiTieu,null,null,
		    null,null,null,ChiTieu.ChiTieuNgayTao+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			ChiTieu ChiTieu=cursorToEntity(cursor);
			lstCtChiTieu.add(ChiTieu);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtChiTieu;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll(java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<ChiTieu> findAll(String sortKey,
	    SortDir sortDir) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.util.Map)
	 */
	@Override
	public List<ChiTieu> findBy(Map<String,Object> map) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public List<ChiTieu> findBy(String fieldName,
	    Collection<?> values) {
		List<ChiTieu> lstCtChiTieu=new ArrayList<ChiTieu>();
		// Điều kiện
		String whereClause=fieldName+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(values)};
		Cursor cursor=database.query(ChiTieu.ChiTieu,
		    ChiTieu.ChiTieuColumns,whereClause,whereArgs,null,
		    null,ChiTieu.ChiTieuNgayTao+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			ChiTieu ChiTieu=cursorToEntity(cursor);
			lstCtChiTieu.add(ChiTieu);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtChiTieu;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<ChiTieu> findBy(String fieldName, Object value) {
		List<ChiTieu> lstCtChiTieu=new ArrayList<ChiTieu>();
		// Điều kiện
		String whereClause=fieldName+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(value)};
		Cursor cursor=database.query(ChiTieu.ChiTieu,
		    ChiTieu.ChiTieuColumns,whereClause,whereArgs,null,
		    null,ChiTieu.ChiTieuNgayTao+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			ChiTieu CtChiTieu=cursorToEntity(cursor);
			lstCtChiTieu.add(CtChiTieu);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtChiTieu;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPK(java.io.Serializable)
	 */
	@Override
	@SuppressWarnings("static-access")
	public ChiTieu findByPK(Integer pk) {
		ChiTieu ChiTieu=null;
		// Điều kiện
		String whereClause=ChiTieu.ChiTieuId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		Cursor cursor=database.query(ChiTieu.ChiTieu,
		    ChiTieu.ChiTieuColumns,whereClause,whereArgs,null,
		    null,ChiTieu.ChiTieuNgayTao+" DESC");
		cursor.moveToFirst();
		if(!cursor.isAfterLast()) {
			ChiTieu=cursorToEntity(cursor);
		}
		// Đóng cursor lại
		cursor.close();
		return ChiTieu;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPKs(java.util.Collection)
	 */
	@Override
	public List<ChiTieu> findByPKs(Collection<?> pks) {
		List<ChiTieu> lstCtChiTieu=new ArrayList<ChiTieu>();
		// Điều kiện
		String whereClause=ChiTieu.ChiTieuId+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pks)};
		Cursor cursor=database.query(ChiTieu.ChiTieu,
		    ChiTieu.ChiTieuColumns,whereClause,whereArgs,null,
		    null,ChiTieu.ChiTieuNgayTao+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			ChiTieu ChiTieu=cursorToEntity(cursor);
			lstCtChiTieu.add(ChiTieu);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtChiTieu;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findOne(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<ChiTieu> findOne(String fieldName,
	    Object value) {
		List<ChiTieu> lstCtChiTieu=new ArrayList<ChiTieu>();
		// Điều kiện
		String whereClause=fieldName+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(value)};
		Cursor cursor=database.query(ChiTieu.ChiTieu,
		    ChiTieu.ChiTieuColumns,whereClause,whereArgs,null,
		    null,ChiTieu.ChiTieuNgayTao+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			ChiTieu ChiTieu=cursorToEntity(cursor);
			lstCtChiTieu.add(ChiTieu);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtChiTieu;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#getAllPKs()
	 */
	@Override
	public Collection<Integer> getAllPKs() {
		List<Integer> lstPKs=new ArrayList<Integer>();
		Cursor cursor=database.query(ChiTieu.ChiTieu,
		    ChiTieu.ChiTieuColumns,null,null,null,null,null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			lstPKs.add(cursor.getInt(cursor
			    .getColumnIndex(ChiTieu.ChiTieuId)));
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstPKs;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insert(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	@SuppressWarnings("static-access")
	public Integer insert(ChiTieu ChiTieu) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(ChiTieu.ChiTieuId,ChiTieu.getChiTieuId());
		values.put(ChiTieu.ChiTieuTen,ChiTieu.getChiTieuTen());
		values.put(ChiTieu.DanhMucId,ChiTieu.getDanhMucId());
		values.put(ChiTieu.TaiKhoanId,ChiTieu.getTaiKhoanId());
		values.put(ChiTieu.HinhAnhId,ChiTieu.getHinhAnhId());
		values.put(ChiTieu.SuKienId,ChiTieu.getSuKienId());
		values.put(ChiTieu.ChiTieuSoTien,
		    ChiTieu.getChiTieuSoTien());
		values.put(ChiTieu.ChitieuNguoiGD,
		    ChiTieu.getChitieuNguoiGD());
		values.put(ChiTieu.ChiTieuDiaDiem,
		    ChiTieu.getChiTieuDiaDiem());
		values.put(ChiTieu.ChitieuNguoiGD,
		    ChiTieu.getChitieuNguoiGD());
		values.put(ChiTieu.ChiTieuTrangThai,
		    ChiTieu.getChiTieuTrangThai());
		values.put(ChiTieu.ChiTieuGhiChu,
		    ChiTieu.getChiTieuGhiChu());
		values.put(ChiTieu.ChiTieuTrangThai,
		    ChiTieu.getChiTieuTrangThai());
		values.put(ChiTieu.ChiTieuGhiChu,
		    ChiTieu.getChiTieuGhiChu());
		values.put(ChiTieu.ChiTieuNgayTao,
		    ChiTieu.getChiTieuNgayTao());
		values.put(ChiTieu.ChiTieuNgaySua,
		    ChiTieu.getChiTieuNgaySua());
		long insertId=database.insert(ChiTieu.ChiTieu,null,
		    values);
		// Trỏ tới database lấy dữ liệu vừa lưu - để kiểm tra xem dữ liệu ghi
		// đúng chưa
		// Truy vấn lấy bản ghi có ID = insertId
		Cursor cursor=database.query(ChiTieu.ChiTieu,null,
		    ChiTieu.ChiTieuId+" = "+insertId,null,null,null,
		    null);
		cursor.moveToFirst();
		ChiTieu dm=cursorToEntity(cursor);
		cursor.close();
		// Trả về đối tượng Comment
		return dm.getId();
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insertOrUpdate(org.oni.webapp.dao.entity
	 * .base.EntityBase)
	 */
	@Override
	public void insertOrUpdate(ChiTieu entity) {
		if(isExisted(entity.getChiTieuId()))
			update(entity);
		else insert(entity);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.io.Serializable)
	 */
	@Override
	public boolean isExisted(Integer pk) {
		if(findByPK(pk)!=null) return true;
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public boolean isExisted(String fieldName,
	    Collection<?> values) {
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public boolean isExisted(String fieldName, Object value) {
		if(findBy(fieldName,value)!=null) return true;
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#update(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	@SuppressWarnings("static-access")
	public void update(ChiTieu ChiTieu) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(ChiTieu.ChiTieuId,ChiTieu.getChiTieuId());
		// values.put(ChiTieu.ChiTieuTen,ChiTieu.getChiTieuTen());
		// values.put(ChiTieu.ChiTieuTrangThai,
		// ChiTieu.getChiTieuTrangThai());
		// values.put(ChiTieu.ChiTieuGhiChu,
		// ChiTieu.getChiTieuGhiChu());
		// values.put(ChiTieu.ChiTieuNgayTao,
		// ChiTieu.getChiTieuNgayTao());
		// values.put(ChiTieu.ChiTieuNgaySua,
		// ChiTieu.getChiTieuNgaySua());
		values.put(ChiTieu.ChiTieuTen,ChiTieu.getChiTieuTen());
		values.put(ChiTieu.DanhMucId,ChiTieu.getDanhMucId());
		values.put(ChiTieu.TaiKhoanId,ChiTieu.getTaiKhoanId());
		values.put(ChiTieu.HinhAnhId,ChiTieu.getHinhAnhId());
		values.put(ChiTieu.SuKienId,ChiTieu.getSuKienId());
		values.put(ChiTieu.ChiTieuSoTien,
		    ChiTieu.getChiTieuSoTien());
		values.put(ChiTieu.ChitieuNguoiGD,
		    ChiTieu.getChitieuNguoiGD());
		values.put(ChiTieu.ChiTieuDiaDiem,
		    ChiTieu.getChiTieuDiaDiem());
		values.put(ChiTieu.ChitieuNguoiGD,
		    ChiTieu.getChitieuNguoiGD());
		values.put(ChiTieu.ChiTieuTrangThai,
		    ChiTieu.getChiTieuTrangThai());
		values.put(ChiTieu.ChiTieuGhiChu,
		    ChiTieu.getChiTieuGhiChu());
		values.put(ChiTieu.ChiTieuTrangThai,
		    ChiTieu.getChiTieuTrangThai());
		values.put(ChiTieu.ChiTieuGhiChu,
		    ChiTieu.getChiTieuGhiChu());
		values.put(ChiTieu.ChiTieuNgayTao,
		    ChiTieu.getChiTieuNgayTao());
		values.put(ChiTieu.ChiTieuNgaySua,
		    ChiTieu.getChiTieuNgaySua());
		// Điều kiện
		String whereClause=ChiTieu.ChiTieuId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(ChiTieu
		    .getChiTieuId())};
		// Thực hiện câu lệnh
		database.update(ChiTieu.ChiTieu,values,whereClause,
		    whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#updateField(java.io.Serializable,
	 * java.lang.String, java.lang.Object)
	 */
	@Override
	public void updateField(Integer pk, String fieldName,
	    Object value) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(ChiTieu.ChiTieuId,ChiTieu.getChiTieuId());
		values.put(fieldName,String.valueOf(value));
		// Điều kiện
		String whereClause=ChiTieu.ChiTieuId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		// Thực hiện câu lệnh
		database.update(ChiTieu.ChiTieu,values,whereClause,
		    whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#updateFields(java.io.Serializable,
	 * java.util.Map)
	 */
	@Override
	public void updateFields(Integer pk,
	    Map<String,Object> valueMap) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		Set<String> keySet=valueMap.keySet();
		for(String key:keySet) {
			values.put(key,String.valueOf(valueMap.get(key)));
		}
		// Điều kiện
		String whereClause=ChiTieu.ChiTieuId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		// Thực hiện câu lệnh
		database.update(ChiTieu.ChiTieu,values,whereClause,
		    whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#getEntityClass()
	 */
	@Override
	public ChiTieu getEntityClass() {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#cursorToEntity(android.database.Cursor)
	 */
	@SuppressWarnings("static-access")
	@Override
	public ChiTieu cursorToEntity(Cursor cursor) {
		ChiTieu ChiTieu=new ChiTieu();
		ChiTieu.setChiTieuId(cursor.getInt(cursor
		    .getColumnIndex(ChiTieu.ChiTieuId)));
		ChiTieu.setDanhMucId(cursor.getInt(cursor
		    .getColumnIndex(ChiTieu.DanhMucId)));
		ChiTieu.setTaiKhoanId(cursor.getInt(cursor
		    .getColumnIndex(ChiTieu.TaiKhoanId)));
		ChiTieu.setHinhAnhId(cursor.getInt(cursor
		    .getColumnIndex(ChiTieu.HinhAnhId)));
		ChiTieu.setSuKienId(cursor.getInt(cursor
		    .getColumnIndex(ChiTieu.SuKienId)));
		ChiTieu.setChiTieuTen(cursor.getString(cursor
		    .getColumnIndex(ChiTieu.ChiTieuTen)));
		ChiTieu.setChiTieuSoTien(cursor.getDouble(cursor
		    .getColumnIndex(ChiTieu.ChiTieuSoTien)));
		ChiTieu.setChitieuNguoiGD(cursor.getString(cursor
		    .getColumnIndex(ChiTieu.ChitieuNguoiGD)));
		ChiTieu.setChiTieuDiaDiem(cursor.getString(cursor
		    .getColumnIndex(ChiTieu.ChiTieuDiaDiem)));
		ChiTieu.setChiTieuTrangThai(cursor.getInt(cursor
		    .getColumnIndex(ChiTieu.ChiTieuTrangThai)));
		ChiTieu.setChiTieuGhiChu(cursor.getString(cursor
		    .getColumnIndex(ChiTieu.ChiTieuGhiChu)));
		ChiTieu.setChiTieuNgayTao(cursor.getString(cursor
		    .getColumnIndex(ChiTieu.ChiTieuNgayTao)));
		ChiTieu.setChiTieuNgaySua(cursor.getString(cursor
		    .getColumnIndex(ChiTieu.ChiTieuNgaySua)));
		return ChiTieu;
	}
}
