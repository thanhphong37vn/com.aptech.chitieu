package com.aptech.chitieu2.dao;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.oni.webapp.enumeration.SortDir;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.aptech.chitieu.entity.DanhMuc;
import com.aptech.chitieu2.dao.GenericDaoI;
/**
 * Title :DanhMucDao <br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 10, 2015 2:41:26 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 10, 2015 2:41:26 PM
 */
public class DanhMucDao extends DatasoureDao implements
    GenericDaoI<DanhMuc,Integer> {
	/**
	 * @param context
	 */
	public DanhMucDao(Context context) {
		super(context);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#countAll()
	 */
	@Override
	public long countAll() {
		return database.compileStatement(
		    "select * from "+DanhMuc.DanhMuc)
		    .simpleQueryForLong();
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#createEntity()
	 */
	@Override
	public DanhMuc createEntity() {
		return new DanhMuc(createPK());
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#createPK()
	 */
	@Override
	public Integer createPK() {
		return (int) (database
		    .compileStatement(
		        "SELECT DanhMucId  FROM Danhmuc order by DanhMucId DESC LIMIT 1;")
		    .simpleQueryForLong()+1);
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#delete(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	public void delete(DanhMuc entity) {
		deleteByPK(entity.getId());
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public void deleteBy(String fieldName,
	    Collection<?> values) {
		database.delete(DanhMuc.DanhMuc,fieldName+" in ("
		    +values+")",null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public void deleteBy(String fieldName, Object value) {
		database.delete(DanhMuc.DanhMuc,fieldName+" = "+value,
		    null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPK(java.io.Serializable)
	 */
	@Override
	public void deleteByPK(Integer pk) {
		database.delete(DanhMuc.DanhMuc,DanhMuc.DanhMucId+" = "
		    +pk,null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPKs(java.util.Collection)
	 */
	@Override
	public void deleteByPKs(Collection<Integer> pks) {
		database.delete(DanhMuc.DanhMuc,DanhMuc.DanhMucId
		    +" in ("+pks+")",null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#find(int, int, java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<DanhMuc> find(int offset, int size,
	    String sortKey, SortDir sortDir) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll()
	 */
	@Override
	public List<DanhMuc> findAll() {
		List<DanhMuc> lstCtDanhMuc=new ArrayList<DanhMuc>();
		Cursor cursor=database.query(DanhMuc.DanhMuc,null,null,
		    null,null,null,DanhMuc.DanhMucTen+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			DanhMuc danhMuc=cursorToEntity(cursor);
			lstCtDanhMuc.add(danhMuc);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtDanhMuc;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll(java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<DanhMuc> findAll(String sortKey,
	    SortDir sortDir) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.util.Map)
	 */
	@Override
	public List<DanhMuc> findBy(Map<String,Object> map) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public List<DanhMuc> findBy(String fieldName,
	    Collection<?> values) {
		List<DanhMuc> lstCtDanhMuc=new ArrayList<DanhMuc>();
		// Điều kiện
		String whereClause=fieldName+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(values)};
		Cursor cursor=database.query(DanhMuc.DanhMuc,
		    DanhMuc.DanhMucColumns,whereClause,whereArgs,null,
		    null,DanhMuc.DanhMucTen+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			DanhMuc danhMuc=cursorToEntity(cursor);
			lstCtDanhMuc.add(danhMuc);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtDanhMuc;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<DanhMuc> findBy(String fieldName, Object value) {
		List<DanhMuc> lstCtDanhMuc=new ArrayList<DanhMuc>();
		// Điều kiện
		String whereClause=fieldName+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(value)};
		Cursor cursor=database.query(DanhMuc.DanhMuc,
		    DanhMuc.DanhMucColumns,whereClause,whereArgs,null,
		    null,DanhMuc.DanhMucTen+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			DanhMuc CtDanhMuc=cursorToEntity(cursor);
			lstCtDanhMuc.add(CtDanhMuc);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtDanhMuc;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPK(java.io.Serializable)
	 */
	@Override
	public DanhMuc findByPK(Integer pk) {
		DanhMuc danhMuc=null;
		// Điều kiện
		String whereClause=DanhMuc.DanhMucId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		Cursor cursor=database.query(DanhMuc.DanhMuc,
		    DanhMuc.DanhMucColumns,whereClause,whereArgs,null,
		    null,DanhMuc.DanhMucTen+" DESC");
		cursor.moveToFirst();
		if(!cursor.isAfterLast()) {
			danhMuc=cursorToEntity(cursor);
		}
		// Đóng cursor lại
		cursor.close();
		return danhMuc;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPKs(java.util.Collection)
	 */
	@Override
	public List<DanhMuc> findByPKs(Collection<?> pks) {
		List<DanhMuc> lstCtDanhMuc=new ArrayList<DanhMuc>();
		// Điều kiện
		String whereClause=DanhMuc.DanhMucId+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pks)};
		Cursor cursor=database.query(DanhMuc.DanhMuc,
		    DanhMuc.DanhMucColumns,whereClause,whereArgs,null,
		    null,DanhMuc.DanhMucTen+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			DanhMuc danhMuc=cursorToEntity(cursor);
			lstCtDanhMuc.add(danhMuc);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtDanhMuc;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findOne(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<DanhMuc> findOne(String fieldName,
	    Object value) {
		List<DanhMuc> lstCtDanhMuc=new ArrayList<DanhMuc>();
		// Điều kiện
		String whereClause=fieldName+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(value)};
		Cursor cursor=database.query(DanhMuc.DanhMuc,
		    DanhMuc.DanhMucColumns,whereClause,whereArgs,null,
		    null,DanhMuc.DanhMucTen+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			DanhMuc danhMuc=cursorToEntity(cursor);
			lstCtDanhMuc.add(danhMuc);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtDanhMuc;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#getAllPKs()
	 */
	@Override
	public Collection<Integer> getAllPKs() {
		List<Integer> lstPKs=new ArrayList<Integer>();
		Cursor cursor=database.query(DanhMuc.DanhMuc,
		    DanhMuc.DanhMucColumns,null,null,null,null,null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			lstPKs.add(cursor.getInt(cursor
			    .getColumnIndex(DanhMuc.DanhMucId)));
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstPKs;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insert(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	public Integer insert(DanhMuc danhMuc) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(DanhMuc.DanhMucId,danhMuc.getDanhMucId());
		values.put(DanhMuc.DanhMucTen,danhMuc.getDanhMucTen());
		values.put(DanhMuc.DanhMucTrangThai,
		    danhMuc.getDanhMucTrangThai());
		values.put(DanhMuc.DanhMucGhiChu,
		    danhMuc.getDanhMucGhiChu());
		values.put(DanhMuc.DanhMucNgayTao,
		    danhMuc.getDanhMucNgayTao());
		values.put(DanhMuc.DanhMucNgaySua,
		    danhMuc.getDanhMucNgaySua());
		long insertId=database.insert(DanhMuc.DanhMuc,null,
		    values);
		// Trỏ tới database lấy dữ liệu vừa lưu - để kiểm tra xem dữ liệu ghi
		// đúng chưa
		// Truy vấn lấy bản ghi có ID = insertId
		Cursor cursor=database.query(DanhMuc.DanhMuc,null,
		    DanhMuc.DanhMucId+" = "+insertId,null,null,null,
		    null);
		cursor.moveToFirst();
		DanhMuc dm=cursorToEntity(cursor);
		cursor.close();
		// Trả về đối tượng Comment
		return dm.getId();
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insertOrUpdate(org.oni.webapp.dao.entity
	 * .base.EntityBase)
	 */
	@Override
	public void insertOrUpdate(DanhMuc entity) {
		if(isExisted(entity.getDanhMucId()))
			update(entity);
		else insert(entity);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.io.Serializable)
	 */
	@Override
	public boolean isExisted(Integer pk) {
		if(findByPK(pk)!=null) return true;
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public boolean isExisted(String fieldName,
	    Collection<?> values) {
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public boolean isExisted(String fieldName, Object value) {
		if(findBy(fieldName,value)!=null) return true;
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#update(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	public void update(DanhMuc danhMuc) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(DanhMuc.DanhMucId,danhMuc.getDanhMucId());
		values.put(DanhMuc.DanhMucTen,danhMuc.getDanhMucTen());
		values.put(DanhMuc.DanhMucTrangThai,
		    danhMuc.getDanhMucTrangThai());
		values.put(DanhMuc.DanhMucGhiChu,
		    danhMuc.getDanhMucGhiChu());
		values.put(DanhMuc.DanhMucNgayTao,
		    danhMuc.getDanhMucNgayTao());
		values.put(DanhMuc.DanhMucNgaySua,
		    danhMuc.getDanhMucNgaySua());
		// Điều kiện
		String whereClause=DanhMuc.DanhMucId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(danhMuc
		    .getDanhMucId())};
		// Thực hiện câu lệnh
		database.update(DanhMuc.DanhMuc,values,whereClause,
		    whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#updateField(java.io.Serializable,
	 * java.lang.String, java.lang.Object)
	 */
	@Override
	public void updateField(Integer pk, String fieldName,
	    Object value) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(DanhMuc.DanhMucId,danhMuc.getDanhMucId());
		values.put(fieldName,String.valueOf(value));
		// Điều kiện
		String whereClause=DanhMuc.DanhMucId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		// Thực hiện câu lệnh
		database.update(DanhMuc.DanhMuc,values,whereClause,
		    whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#updateFields(java.io.Serializable,
	 * java.util.Map)
	 */
	@Override
	public void updateFields(Integer pk,
	    Map<String,Object> valueMap) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		Set<String> keySet=valueMap.keySet();
		for(String key:keySet) {
			values.put(key,String.valueOf(valueMap.get(key)));
		}
		// Điều kiện
		String whereClause=DanhMuc.DanhMucId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		// Thực hiện câu lệnh
		database.update(DanhMuc.DanhMuc,values,whereClause,
		    whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#getEntityClass()
	 */
	@Override
	public DanhMuc getEntityClass() {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#cursorToEntity(android.database.Cursor)
	 */
	@Override
	public DanhMuc cursorToEntity(Cursor cursor) {
		DanhMuc danhMuc=new DanhMuc();
		danhMuc.setDanhMucId(cursor.getInt(cursor
		    .getColumnIndex(DanhMuc.DanhMucId)));
		danhMuc.setDanhMucTen(cursor.getString(cursor
		    .getColumnIndex(DanhMuc.DanhMucTen)));
		danhMuc.setDanhMucTrangThai(cursor.getInt(cursor
		    .getColumnIndex(DanhMuc.DanhMucTrangThai)));
		danhMuc.setDanhMucGhiChu(cursor.getString(cursor
		    .getColumnIndex(DanhMuc.DanhMucGhiChu)));
		danhMuc.setDanhMucNgayTao(cursor.getString(cursor
		    .getColumnIndex(DanhMuc.DanhMucNgayTao)));
		danhMuc.setDanhMucNgaySua(cursor.getString(cursor
		    .getColumnIndex(DanhMuc.DanhMucNgaySua)));
		return danhMuc;
	}
}
