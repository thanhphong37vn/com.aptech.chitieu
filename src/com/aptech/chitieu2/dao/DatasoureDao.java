package com.aptech.chitieu2.dao;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.aptech.chitieu.dao.DatabaseHelperChiTieu;
/**
 * This is the interface of the base DAO.
 * 
 * @author DungTV
 * @param <E>
 *          Class of Entity
 * @param <PK>
 *          Class of Primary Key
 */
public abstract class DatasoureDao {
	/** Database fields */
	protected SQLiteDatabase	      database	=null;
	protected DatabaseHelperChiTieu	dbHelper	=null;
	/** Hàm contructor kiêm nhiệm vụ khởi tạo Database Helper */
	public DatasoureDao(Context context) {
		dbHelper=new DatabaseHelperChiTieu(context);
		open();
	}
	// Hàm xin phép mở database để đọc ghi dữ liệu
	public void open() throws SQLException {
		database=dbHelper.getWritableDatabase();
	}
	/** Hàm đóng database lại kết thúc việc đọc ghi dữ liệu */
	public void close() {
		dbHelper.close();
	}
}
