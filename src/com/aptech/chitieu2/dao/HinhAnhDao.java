package com.aptech.chitieu2.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.oni.webapp.enumeration.SortDir;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.aptech.chitieu.entity.HinhAnh;
import com.aptech.chitieu2.dao.GenericDaoI;

/**
 * Title :HinhAnhDao <br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 10, 2015 2:41:26 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 10, 2015 2:41:26 PM
 */
public class HinhAnhDao extends DatasoureDao implements
		GenericDaoI<HinhAnh, Integer> {
	/**
	 * @param context
	 */
	public HinhAnhDao(Context context) {
		super(context);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#countAll()
	 */
	@Override
	public long countAll() {
		return database.compileStatement("select * from " + HinhAnh.HinhAnh)
				.simpleQueryForLong();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#createEntity()
	 */
	@Override
	public HinhAnh createEntity() {
		return new HinhAnh(createPK());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#createPK()
	 */
	@Override
	public Integer createPK() {
		return (int) (database
				.compileStatement(
						"SELECT HinhAnhId  FROM HinhAnh order by HinhAnhId DESC LIMIT 1;")
				.simpleQueryForLong() + 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#delete(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	public void delete(HinhAnh entity) {
		deleteByPK(entity.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public void deleteBy(String fieldName, Collection<?> values) {
		database.delete(HinhAnh.HinhAnh, fieldName + " in (" + values + ")",
				null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public void deleteBy(String fieldName, Object value) {
		database.delete(HinhAnh.HinhAnh, fieldName + " = " + value, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPK(java.io.Serializable)
	 */
	@Override
	public void deleteByPK(Integer pk) {
		database.delete(HinhAnh.HinhAnh, HinhAnh.HinhAnhId + " = " + pk, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPKs(java.util.Collection)
	 */
	@Override
	public void deleteByPKs(Collection<Integer> pks) {
		database.delete(HinhAnh.HinhAnh, HinhAnh.HinhAnhId + " in (" + pks
				+ ")", null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#find(int, int, java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<HinhAnh> find(int offset, int size, String sortKey,
			SortDir sortDir) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll()
	 */
	@Override
	public List<HinhAnh> findAll() {
		List<HinhAnh> lstCtHinhAnh = new ArrayList<HinhAnh>();
		Cursor cursor = database.query(HinhAnh.HinhAnh, null, null, null, null,
				null, HinhAnh.HinhAnhTen + " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			HinhAnh HinhAnh = cursorToEntity(cursor);
			lstCtHinhAnh.add(HinhAnh);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtHinhAnh;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll(java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<HinhAnh> findAll(String sortKey, SortDir sortDir) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.util.Map)
	 */
	@Override
	public List<HinhAnh> findBy(Map<String, Object> map) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public List<HinhAnh> findBy(String fieldName, Collection<?> values) {
		List<HinhAnh> lstCtHinhAnh = new ArrayList<HinhAnh>();
		// Điều kiện
		String whereClause = fieldName + " in (?)";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(values) };
		Cursor cursor = database.query(HinhAnh.HinhAnh, HinhAnh.HinhAnhColumns,
				whereClause, whereArgs, null, null, HinhAnh.HinhAnhTen
						+ " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			HinhAnh HinhAnh = cursorToEntity(cursor);
			lstCtHinhAnh.add(HinhAnh);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtHinhAnh;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<HinhAnh> findBy(String fieldName, Object value) {
		List<HinhAnh> lstCtHinhAnh = new ArrayList<HinhAnh>();
		// Điều kiện
		String whereClause = fieldName + " = ?";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(value) };
		Cursor cursor = database.query(HinhAnh.HinhAnh, HinhAnh.HinhAnhColumns,
				whereClause, whereArgs, null, null, HinhAnh.HinhAnhTen
						+ " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			HinhAnh CtHinhAnh = cursorToEntity(cursor);
			lstCtHinhAnh.add(CtHinhAnh);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtHinhAnh;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPK(java.io.Serializable)
	 */
	@Override
	@SuppressWarnings("static-access")
	public HinhAnh findByPK(Integer pk) {
		HinhAnh HinhAnh = null;
		// Điều kiện
		String whereClause = HinhAnh.HinhAnhId + " = ?";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(pk) };
		Cursor cursor = database.query(HinhAnh.HinhAnh, HinhAnh.HinhAnhColumns,
				whereClause, whereArgs, null, null, HinhAnh.HinhAnhTen
						+ " DESC");
		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			HinhAnh = cursorToEntity(cursor);
		}
		// Đóng cursor lại
		cursor.close();
		return HinhAnh;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPKs(java.util.Collection)
	 */
	@Override
	public List<HinhAnh> findByPKs(Collection<?> pks) {
		List<HinhAnh> lstCtHinhAnh = new ArrayList<HinhAnh>();
		// Điều kiện
		String whereClause = HinhAnh.HinhAnhId + " in (?)";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(pks) };
		Cursor cursor = database.query(HinhAnh.HinhAnh, HinhAnh.HinhAnhColumns,
				whereClause, whereArgs, null, null, HinhAnh.HinhAnhTen
						+ " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			HinhAnh HinhAnh = cursorToEntity(cursor);
			lstCtHinhAnh.add(HinhAnh);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtHinhAnh;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findOne(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<HinhAnh> findOne(String fieldName, Object value) {
		List<HinhAnh> lstCtHinhAnh = new ArrayList<HinhAnh>();
		// Điều kiện
		String whereClause = fieldName + " in (?)";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(value) };
		Cursor cursor = database.query(HinhAnh.HinhAnh, HinhAnh.HinhAnhColumns,
				whereClause, whereArgs, null, null, HinhAnh.HinhAnhTen
						+ " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			HinhAnh HinhAnh = cursorToEntity(cursor);
			lstCtHinhAnh.add(HinhAnh);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtHinhAnh;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#getAllPKs()
	 */
	@Override
	public Collection<Integer> getAllPKs() {
		List<Integer> lstPKs = new ArrayList<Integer>();
		Cursor cursor = database.query(HinhAnh.HinhAnh, HinhAnh.HinhAnhColumns,
				null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			lstPKs.add(cursor.getInt(cursor.getColumnIndex(HinhAnh.HinhAnhId)));
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstPKs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insert(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	@SuppressWarnings("static-access")
	public Integer insert(HinhAnh HinhAnh) {
		// Ghi dữ liệu xuống database
		ContentValues values = new ContentValues();
		// values.put(HinhAnh.HinhAnhId,HinhAnh.getHinhAnhId());
		values.put(HinhAnh.HinhAnhTen, HinhAnh.getHinhAnhTen());
		values.put(HinhAnh.HinhAnhTrangThai, HinhAnh.getHinhAnhTrangThai());
		values.put(HinhAnh.HinhAnhGhiChu, HinhAnh.getHinhAnhGhiChu());
		values.put(HinhAnh.HinhAnhNgayTao, HinhAnh.getHinhAnhNgayTao());
		values.put(HinhAnh.HinhAnhNgaySua, HinhAnh.getHinhAnhNgaySua());
		long insertId = database.insert(HinhAnh.HinhAnh, null, values);
		// Trỏ tới database lấy dữ liệu vừa lưu - để kiểm tra xem dữ liệu ghi
		// đúng chưa
		// Truy vấn lấy bản ghi có ID = insertId
		Cursor cursor = database.query(HinhAnh.HinhAnh, null, HinhAnh.HinhAnhId
				+ " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		HinhAnh dm = cursorToEntity(cursor);
		cursor.close();
		// Trả về đối tượng Comment
		return dm.getId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insertOrUpdate(org.oni.webapp.dao.
	 * entity .base.EntityBase)
	 */
	@Override
	public void insertOrUpdate(HinhAnh entity) {
		if (isExisted(entity.getHinhAnhId()))
			update(entity);
		else
			insert(entity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.io.Serializable)
	 */
	@Override
	public boolean isExisted(Integer pk) {
		if (findByPK(pk) != null)
			return true;
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public boolean isExisted(String fieldName, Collection<?> values) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public boolean isExisted(String fieldName, Object value) {
		if (findBy(fieldName, value) != null)
			return true;
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#update(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	@SuppressWarnings("static-access")
	public void update(HinhAnh HinhAnh) {
		// Ghi dữ liệu xuống database
		ContentValues values = new ContentValues();
		// values.put(HinhAnh.HinhAnhId,HinhAnh.getHinhAnhId());
		values.put(HinhAnh.HinhAnhTen, HinhAnh.getHinhAnhTen());
		values.put(HinhAnh.HinhAnhTrangThai, HinhAnh.getHinhAnhTrangThai());
		values.put(HinhAnh.HinhAnhGhiChu, HinhAnh.getHinhAnhGhiChu());
		values.put(HinhAnh.HinhAnhNgayTao, HinhAnh.getHinhAnhNgayTao());
		values.put(HinhAnh.HinhAnhNgaySua, HinhAnh.getHinhAnhNgaySua());
		// Điều kiện
		String whereClause = HinhAnh.HinhAnhId + " = ?";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(HinhAnh
				.getHinhAnhId()) };
		// Thực hiện câu lệnh
		database.update(HinhAnh.HinhAnh, values, whereClause, whereArgs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#updateField(java.io.Serializable,
	 * java.lang.String, java.lang.Object)
	 */
	@Override
	public void updateField(Integer pk, String fieldName, Object value) {
		// Ghi dữ liệu xuống database
		ContentValues values = new ContentValues();
		// values.put(HinhAnh.HinhAnhId,HinhAnh.getHinhAnhId());
		values.put(fieldName, String.valueOf(value));
		// Điều kiện
		String whereClause = HinhAnh.HinhAnhId + " = ?";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(pk) };
		// Thực hiện câu lệnh
		database.update(HinhAnh.HinhAnh, values, whereClause, whereArgs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#updateFields(java.io.Serializable,
	 * java.util.Map)
	 */
	@Override
	public void updateFields(Integer pk, Map<String, Object> valueMap) {
		// Ghi dữ liệu xuống database
		ContentValues values = new ContentValues();
		Set<String> keySet = valueMap.keySet();
		for (String key : keySet) {
			values.put(key, String.valueOf(valueMap.get(key)));
		}
		// Điều kiện
		String whereClause = HinhAnh.HinhAnhId + " = ?";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(pk) };
		// Thực hiện câu lệnh
		database.update(HinhAnh.HinhAnh, values, whereClause, whereArgs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#getEntityClass()
	 */
	@Override
	public HinhAnh getEntityClass() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#cursorToEntity(android.database.Cursor
	 * )
	 */
	@Override
	@SuppressWarnings("static-access")
	public HinhAnh cursorToEntity(Cursor cursor) {
		HinhAnh HinhAnh = new HinhAnh();
		HinhAnh.setHinhAnhId(cursor.getInt(cursor
				.getColumnIndex(HinhAnh.HinhAnhId)));
		HinhAnh.setHinhAnhTen(cursor.getString(cursor
				.getColumnIndex(HinhAnh.HinhAnhTen)));
		HinhAnh.setHinhAnhDuongDan(cursor.getString(cursor
				.getColumnIndex(HinhAnh.HinhAnhDuongDan)));
		HinhAnh.setHinhAnhTrangThai(cursor.getInt(cursor
				.getColumnIndex(HinhAnh.HinhAnhTrangThai)));
		HinhAnh.setHinhAnhGhiChu(cursor.getString(cursor
				.getColumnIndex(HinhAnh.HinhAnhGhiChu)));
		HinhAnh.setHinhAnhNgayTao(cursor.getString(cursor
				.getColumnIndex(HinhAnh.HinhAnhNgayTao)));
		HinhAnh.setHinhAnhNgaySua(cursor.getString(cursor
				.getColumnIndex(HinhAnh.HinhAnhNgaySua)));
		return HinhAnh;
	}
}
