package com.aptech.chitieu2.dao;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.oni.webapp.enumeration.SortDir;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.aptech.chitieu.entity.SuKien;
import com.aptech.chitieu2.dao.GenericDaoI;
/**
 * Title :SuKienDao <br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 10, 2015 2:41:26 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 10, 2015 2:41:26 PM
 */
public class SuKienDao extends DatasoureDao implements
    GenericDaoI<SuKien,Integer> {
	/**
	 * @param context
	 */
	public SuKienDao(Context context) {
		super(context);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#countAll()
	 */
	@Override
	public long countAll() {
		return database.compileStatement(
		    "select * from "+SuKien.SuKien)
		    .simpleQueryForLong();
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#createEntity()
	 */
	@Override
	public SuKien createEntity() {
		return new SuKien(createPK());
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#createPK()
	 */
	@Override
	public Integer createPK() {
		return (int) (database
		    .compileStatement(
		        "SELECT SuKienId  FROM SuKien order by SuKienId DESC LIMIT 1;")
		    .simpleQueryForLong()+1);
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#delete(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	public void delete(SuKien entity) {
		deleteByPK(entity.getId());
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public void deleteBy(String fieldName,
	    Collection<?> values) {
		database.delete(SuKien.SuKien,fieldName+" in ("+values
		    +")",null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public void deleteBy(String fieldName, Object value) {
		database.delete(SuKien.SuKien,fieldName+" = "+value,
		    null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPK(java.io.Serializable)
	 */
	@Override
	public void deleteByPK(Integer pk) {
		database.delete(SuKien.SuKien,SuKien.SuKienId+" = "+pk,
		    null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPKs(java.util.Collection)
	 */
	@Override
	public void deleteByPKs(Collection<Integer> pks) {
		database.delete(SuKien.SuKien,SuKien.SuKienId+" in ("
		    +pks+")",null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#find(int, int, java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<SuKien> find(int offset, int size,
	    String sortKey, SortDir sortDir) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll()
	 */
	@Override
	public List<SuKien> findAll() {
		List<SuKien> lstCtSuKien=new ArrayList<SuKien>();
		Cursor cursor=database.query(SuKien.SuKien,null,null,
		    null,null,null,SuKien.SuKienNgayTao+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			SuKien SuKien=cursorToEntity(cursor);
			lstCtSuKien.add(SuKien);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtSuKien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll(java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<SuKien> findAll(String sortKey,
	    SortDir sortDir) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.util.Map)
	 */
	@Override
	public List<SuKien> findBy(Map<String,Object> map) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public List<SuKien> findBy(String fieldName,
	    Collection<?> values) {
		List<SuKien> lstCtSuKien=new ArrayList<SuKien>();
		// Điều kiện
		String whereClause=fieldName+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(values)};
		Cursor cursor=database.query(SuKien.SuKien,
		    SuKien.SuKienColumns,whereClause,whereArgs,null,
		    null,SuKien.SuKienNgayTao+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			SuKien SuKien=cursorToEntity(cursor);
			lstCtSuKien.add(SuKien);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtSuKien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<SuKien> findBy(String fieldName, Object value) {
		List<SuKien> lstCtSuKien=new ArrayList<SuKien>();
		// Điều kiện
		String whereClause=fieldName+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(value)};
		Cursor cursor=database.query(SuKien.SuKien,
		    SuKien.SuKienColumns,whereClause,whereArgs,null,
		    null,SuKien.SuKienNgayTao+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			SuKien CtSuKien=cursorToEntity(cursor);
			lstCtSuKien.add(CtSuKien);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtSuKien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPK(java.io.Serializable)
	 */
	@Override
	@SuppressWarnings("static-access")
	public SuKien findByPK(Integer pk) {
		SuKien SuKien=null;
		// Điều kiện
		String whereClause=SuKien.SuKienId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		Cursor cursor=database.query(SuKien.SuKien,
		    SuKien.SuKienColumns,whereClause,whereArgs,null,
		    null,SuKien.SuKienNgayTao+" DESC");
		cursor.moveToFirst();
		if(!cursor.isAfterLast()) {
			SuKien=cursorToEntity(cursor);
		}
		// Đóng cursor lại
		cursor.close();
		return SuKien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPKs(java.util.Collection)
	 */
	@Override
	public List<SuKien> findByPKs(Collection<?> pks) {
		List<SuKien> lstCtSuKien=new ArrayList<SuKien>();
		// Điều kiện
		String whereClause=SuKien.SuKienId+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pks)};
		Cursor cursor=database.query(SuKien.SuKien,
		    SuKien.SuKienColumns,whereClause,whereArgs,null,
		    null,SuKien.SuKienNgayTao+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			SuKien SuKien=cursorToEntity(cursor);
			lstCtSuKien.add(SuKien);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtSuKien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findOne(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<SuKien> findOne(String fieldName, Object value) {
		List<SuKien> lstCtSuKien=new ArrayList<SuKien>();
		// Điều kiện
		String whereClause=fieldName+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(value)};
		Cursor cursor=database.query(SuKien.SuKien,
		    SuKien.SuKienColumns,whereClause,whereArgs,null,
		    null,SuKien.SuKienNgayTao+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			SuKien SuKien=cursorToEntity(cursor);
			lstCtSuKien.add(SuKien);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtSuKien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#getAllPKs()
	 */
	@Override
	public Collection<Integer> getAllPKs() {
		List<Integer> lstPKs=new ArrayList<Integer>();
		Cursor cursor=database.query(SuKien.SuKien,
		    SuKien.SuKienColumns,null,null,null,null,null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			lstPKs.add(cursor.getInt(cursor
			    .getColumnIndex(SuKien.SuKienId)));
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstPKs;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insert(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	@SuppressWarnings("static-access")
	public Integer insert(SuKien SuKien) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(SuKien.SuKienId,SuKien.getSuKienId());
		values.put(SuKien.SuKienTen,SuKien.getSuKienTen());
		values.put(SuKien.SuKienTrangThai,
		    SuKien.getSuKienTrangThai());
		values
		    .put(SuKien.SuKienGhiChu,SuKien.getSuKienGhiChu());
		values.put(SuKien.SuKienNgayTao,
		    SuKien.getSuKienNgayTao());
		values.put(SuKien.SuKienNgaySua,
		    SuKien.getSuKienNgaySua());
		long insertId=database
		    .insert(SuKien.SuKien,null,values);
		// Trỏ tới database lấy dữ liệu vừa lưu - để kiểm tra xem dữ liệu ghi
		// đúng chưa
		// Truy vấn lấy bản ghi có ID = insertId
		Cursor cursor=database.query(SuKien.SuKien,null,
		    SuKien.SuKienId+" = "+insertId,null,null,null,null);
		cursor.moveToFirst();
		SuKien dm=cursorToEntity(cursor);
		cursor.close();
		// Trả về đối tượng Comment
		return dm.getId();
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insertOrUpdate(org.oni.webapp.dao.entity
	 * .base.EntityBase)
	 */
	@Override
	public void insertOrUpdate(SuKien entity) {
		if(isExisted(entity.getSuKienId()))
			update(entity);
		else insert(entity);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.io.Serializable)
	 */
	@Override
	public boolean isExisted(Integer pk) {
		if(findByPK(pk)!=null) return true;
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public boolean isExisted(String fieldName,
	    Collection<?> values) {
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public boolean isExisted(String fieldName, Object value) {
		if(findBy(fieldName,value)!=null) return true;
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#update(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	@SuppressWarnings("static-access")
	public void update(SuKien SuKien) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(SuKien.SuKienId,SuKien.getSuKienId());
		values.put(SuKien.SuKienTen,SuKien.getSuKienTen());
		values.put(SuKien.SuKienTrangThai,
		    SuKien.getSuKienTrangThai());
		values
		    .put(SuKien.SuKienGhiChu,SuKien.getSuKienGhiChu());
		values.put(SuKien.SuKienNgayTao,
		    SuKien.getSuKienNgayTao());
		values.put(SuKien.SuKienNgaySua,
		    SuKien.getSuKienNgaySua());
		// Điều kiện
		String whereClause=SuKien.SuKienId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(SuKien
		    .getSuKienId())};
		// Thực hiện câu lệnh
		database.update(SuKien.SuKien,values,whereClause,
		    whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#updateField(java.io.Serializable,
	 * java.lang.String, java.lang.Object)
	 */
	@Override
	public void updateField(Integer pk, String fieldName,
	    Object value) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(SuKien.SuKienId,SuKien.getSuKienId());
		values.put(fieldName,String.valueOf(value));
		// Điều kiện
		String whereClause=SuKien.SuKienId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		// Thực hiện câu lệnh
		database.update(SuKien.SuKien,values,whereClause,
		    whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#updateFields(java.io.Serializable,
	 * java.util.Map)
	 */
	@Override
	public void updateFields(Integer pk,
	    Map<String,Object> valueMap) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		Set<String> keySet=valueMap.keySet();
		for(String key:keySet) {
			values.put(key,String.valueOf(valueMap.get(key)));
		}
		// Điều kiện
		String whereClause=SuKien.SuKienId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		// Thực hiện câu lệnh
		database.update(SuKien.SuKien,values,whereClause,
		    whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#getEntityClass()
	 */
	@Override
	public SuKien getEntityClass() {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#cursorToEntity(android.database.Cursor)
	 */
	@Override
	@SuppressWarnings("static-access")
	public SuKien cursorToEntity(Cursor cursor) {
		SuKien SuKien=new SuKien();
		SuKien.setSuKienId(cursor.getInt(cursor
		    .getColumnIndex(SuKien.SuKienId)));
		SuKien.setSuKienTen(cursor.getString(cursor
		    .getColumnIndex(SuKien.SuKienTen)));
		SuKien.setSuKienTrangThai(cursor.getInt(cursor
		    .getColumnIndex(SuKien.SuKienTrangThai)));
		SuKien.setSuKienGhiChu(cursor.getString(cursor
		    .getColumnIndex(SuKien.SuKienGhiChu)));
		SuKien.setSuKienNgayTao(cursor.getString(cursor
		    .getColumnIndex(SuKien.SuKienNgayTao)));
		SuKien.setSuKienNgaySua(cursor.getString(cursor
		    .getColumnIndex(SuKien.SuKienNgaySua)));
		SuKien.setHinhAnhId(cursor.getInt(cursor
		    .getColumnIndex(SuKien.HinhAnhId)));
		return SuKien;
	}
}
