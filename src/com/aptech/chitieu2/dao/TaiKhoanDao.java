package com.aptech.chitieu2.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.oni.webapp.enumeration.SortDir;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.aptech.chitieu.entity.ChiTieu;
import com.aptech.chitieu.entity.DanhMuc;
import com.aptech.chitieu.entity.TaiKhoan;
import com.aptech.chitieu2.dao.GenericDaoI;

/**
 * Title :TaiKhoanDao <br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 10, 2015 2:41:26 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 10, 2015 2:41:26 PM
 */
public class TaiKhoanDao extends DatasoureDao implements
		GenericDaoI<TaiKhoan, Integer> {

	/**
	 * @param context
	 */
	public TaiKhoanDao(Context context) {

		super(context);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#countAll()
	 */
	@Override
	public long countAll() {
		return database.compileStatement("select * from " + TaiKhoan.TaiKhoan)
				.simpleQueryForLong();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#createEntity()
	 */
	@Override
	public TaiKhoan createEntity() {
		return new TaiKhoan(createPK());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#createPK()
	 */
	@Override
	public Integer createPK() {
		return (int) (database
				.compileStatement(
						"SELECT TaiKhoanId  FROM TaiKhoan order by TaiKhoanId DESC LIMIT 1;")
				.simpleQueryForLong() + 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#delete(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	public void delete(TaiKhoan entity) {
		deleteByPK(entity.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public void deleteBy(String fieldName, Collection<?> values) {
		database.delete(TaiKhoan.TaiKhoan, fieldName + " in (" + values + ")",
				null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public void deleteBy(String fieldName, Object value) {
		database.delete(TaiKhoan.TaiKhoan, fieldName + " = " + value, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPK(java.io.Serializable)
	 */
	@Override
	public void deleteByPK(Integer pk) {
		database.delete(TaiKhoan.TaiKhoan, TaiKhoan.TaiKhoanId + " = " + pk,
				null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPKs(java.util.Collection)
	 */
	@Override
	public void deleteByPKs(Collection<Integer> pks) {
		database.delete(TaiKhoan.TaiKhoan, TaiKhoan.TaiKhoanId + " in (" + pks
				+ ")", null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#find(int, int, java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<TaiKhoan> find(int offset, int size, String sortKey,
			SortDir sortDir) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll()
	 */
	@Override
	public List<TaiKhoan> findAll() {
		List<TaiKhoan> lstCtTaiKhoan = new ArrayList<TaiKhoan>();
		Cursor cursor = database.query(TaiKhoan.TaiKhoan, null, null, null,
				null, null, TaiKhoan.TaiKhoanTen + " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			TaiKhoan TaiKhoan = cursorToEntity(cursor);
			lstCtTaiKhoan.add(TaiKhoan);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtTaiKhoan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll(java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<TaiKhoan> findAll(String sortKey, SortDir sortDir) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.util.Map)
	 */
	@Override
	public List<TaiKhoan> findBy(Map<String, Object> map) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public List<TaiKhoan> findBy(String fieldName, Collection<?> values) {
		List<TaiKhoan> lstCtTaiKhoan = new ArrayList<TaiKhoan>();
		// Điều kiện
		String whereClause = fieldName + " in (?)";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(values) };
		Cursor cursor = database.query(TaiKhoan.TaiKhoan,
				TaiKhoan.TaiKhoanColumns, whereClause, whereArgs, null, null,
				TaiKhoan.TaiKhoanTen + " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			TaiKhoan TaiKhoan = cursorToEntity(cursor);
			lstCtTaiKhoan.add(TaiKhoan);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtTaiKhoan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<TaiKhoan> findBy(String fieldName, Object value) {
		List<TaiKhoan> lstCtTaiKhoan = new ArrayList<TaiKhoan>();
		// Điều kiện
		String whereClause = fieldName + " = ?";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(value) };
		Cursor cursor = database.query(TaiKhoan.TaiKhoan,
				TaiKhoan.TaiKhoanColumns, whereClause, whereArgs, null, null,
				TaiKhoan.TaiKhoanTen + " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			TaiKhoan CtTaiKhoan = cursorToEntity(cursor);
			lstCtTaiKhoan.add(CtTaiKhoan);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtTaiKhoan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPK(java.io.Serializable)
	 */
	@Override
	@SuppressWarnings("static-access")
	public TaiKhoan findByPK(Integer pk) {
		TaiKhoan TaiKhoan = null;
		// Điều kiện
		String whereClause = TaiKhoan.TaiKhoanId + " = ?";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(pk) };
		Cursor cursor = database.query(TaiKhoan.TaiKhoan,
				TaiKhoan.TaiKhoanColumns, whereClause, whereArgs, null, null,
				TaiKhoan.TaiKhoanTen + " DESC");
		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			TaiKhoan = cursorToEntity(cursor);
		}
		// Đóng cursor lại
		cursor.close();
		return TaiKhoan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPKs(java.util.Collection)
	 */
	@Override
	public List<TaiKhoan> findByPKs(Collection<?> pks) {
		List<TaiKhoan> lstCtTaiKhoan = new ArrayList<TaiKhoan>();
		// Điều kiện
		String whereClause = TaiKhoan.TaiKhoanId + " in (?)";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(pks) };
		Cursor cursor = database.query(TaiKhoan.TaiKhoan,
				TaiKhoan.TaiKhoanColumns, whereClause, whereArgs, null, null,
				TaiKhoan.TaiKhoanTen + " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			TaiKhoan TaiKhoan = cursorToEntity(cursor);
			lstCtTaiKhoan.add(TaiKhoan);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtTaiKhoan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#findOne(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<TaiKhoan> findOne(String fieldName, Object value) {
		List<TaiKhoan> lstCtTaiKhoan = new ArrayList<TaiKhoan>();
		// Điều kiện
		String whereClause = fieldName + " in (?)";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(value) };
		Cursor cursor = database.query(TaiKhoan.TaiKhoan,
				TaiKhoan.TaiKhoanColumns, whereClause, whereArgs, null, null,
				TaiKhoan.TaiKhoanTen + " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			TaiKhoan TaiKhoan = cursorToEntity(cursor);
			lstCtTaiKhoan.add(TaiKhoan);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtTaiKhoan;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#getAllPKs()
	 */
	@Override
	public Collection<Integer> getAllPKs() {
		List<Integer> lstPKs = new ArrayList<Integer>();
		Cursor cursor = database.query(TaiKhoan.TaiKhoan,
				TaiKhoan.TaiKhoanColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			lstPKs.add(cursor.getInt(cursor.getColumnIndex(TaiKhoan.TaiKhoanId)));
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstPKs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insert(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	@SuppressWarnings("static-access")
	public Integer insert(TaiKhoan TaiKhoan) {
		// Ghi dữ liệu xuống database
		ContentValues values = new ContentValues();
		// values.put(TaiKhoan.TaiKhoanId,TaiKhoan.getTaiKhoanId());
		values.put(TaiKhoan.TaiKhoanTen, TaiKhoan.getTaiKhoanTen());
		values.put(TaiKhoan.TaiKhoanTrangThai, TaiKhoan.getTaiKhoanTrangThai());
		values.put(TaiKhoan.TaiKhoanGhiChu, TaiKhoan.getTaiKhoanGhiChu());
		values.put(TaiKhoan.TaiKhoanNgayTao, TaiKhoan.getTaiKhoanNgayTao());
		values.put(TaiKhoan.TaiKhoanNgaySua, TaiKhoan.getTaiKhoanNgaySua());
		long insertId = database.insert(TaiKhoan.TaiKhoan, null, values);
		// Trỏ tới database lấy dữ liệu vừa lưu - để kiểm tra xem dữ liệu ghi
		// đúng chưa
		// Truy vấn lấy bản ghi có ID = insertId
		Cursor cursor = database.query(TaiKhoan.TaiKhoan, null,
				TaiKhoan.TaiKhoanId + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		TaiKhoan dm = cursorToEntity(cursor);
		cursor.close();
		// Trả về đối tượng Comment
		return dm.getId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insertOrUpdate(org.oni.webapp.dao.
	 * entity .base.EntityBase)
	 */
	@Override
	public void insertOrUpdate(TaiKhoan entity) {
		if (isExisted(entity.getTaiKhoanId()))
			update(entity);
		else
			insert(entity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.io.Serializable)
	 */
	@Override
	public boolean isExisted(Integer pk) {
		if (findByPK(pk) != null)
			return true;
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public boolean isExisted(String fieldName, Collection<?> values) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public boolean isExisted(String fieldName, Object value) {
		if (findBy(fieldName, value) != null)
			return true;
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#update(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	@SuppressWarnings("static-access")
	public void update(TaiKhoan TaiKhoan) {
		// Ghi dữ liệu xuống database
		ContentValues values = new ContentValues();
		// values.put(TaiKhoan.TaiKhoanId,TaiKhoan.getTaiKhoanId());
		values.put(TaiKhoan.TaiKhoanTen, TaiKhoan.getTaiKhoanTen());
		values.put(TaiKhoan.TaiKhoanTrangThai, TaiKhoan.getTaiKhoanTrangThai());
		values.put(TaiKhoan.TaiKhoanGhiChu, TaiKhoan.getTaiKhoanGhiChu());
		values.put(TaiKhoan.TaiKhoanNgayTao, TaiKhoan.getTaiKhoanNgayTao());
		values.put(TaiKhoan.TaiKhoanNgaySua, TaiKhoan.getTaiKhoanNgaySua());
		// Điều kiện
		String whereClause = TaiKhoan.TaiKhoanId + " = ?";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(TaiKhoan
				.getTaiKhoanId()) };
		// Thực hiện câu lệnh
		database.update(TaiKhoan.TaiKhoan, values, whereClause, whereArgs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#updateField(java.io.Serializable,
	 * java.lang.String, java.lang.Object)
	 */
	@Override
	public void updateField(Integer pk, String fieldName, Object value) {
		// Ghi dữ liệu xuống database
		ContentValues values = new ContentValues();
		// values.put(TaiKhoan.TaiKhoanId,TaiKhoan.getTaiKhoanId());
		values.put(fieldName, String.valueOf(value));
		// Điều kiện
		String whereClause = TaiKhoan.TaiKhoanId + " = ?";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(pk) };
		// Thực hiện câu lệnh
		database.update(TaiKhoan.TaiKhoan, values, whereClause, whereArgs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#updateFields(java.io.Serializable,
	 * java.util.Map)
	 */
	@Override
	public void updateFields(Integer pk, Map<String, Object> valueMap) {
		// Ghi dữ liệu xuống database
		ContentValues values = new ContentValues();
		Set<String> keySet = valueMap.keySet();
		for (String key : keySet) {
			values.put(key, String.valueOf(valueMap.get(key)));
		}
		// Điều kiện
		String whereClause = TaiKhoan.TaiKhoanId + " = ?";
		// Tham số cho điều kiện
		String[] whereArgs = new String[] { String.valueOf(pk) };
		// Thực hiện câu lệnh
		database.update(TaiKhoan.TaiKhoan, values, whereClause, whereArgs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aptech.chitieu.dao2.GenericDao#getEntityClass()
	 */
	@Override
	public TaiKhoan getEntityClass() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#cursorToEntity(android.database.Cursor
	 * )
	 */
	@Override
	@SuppressWarnings("static-access")
	public TaiKhoan cursorToEntity(Cursor cursor) {
		TaiKhoan TaiKhoan = new TaiKhoan();
		TaiKhoan.setTaiKhoanId(cursor.getInt(cursor
				.getColumnIndex(TaiKhoan.TaiKhoanId)));
		TaiKhoan.setTaiKhoanTen(cursor.getString(cursor
				.getColumnIndex(TaiKhoan.TaiKhoanTen)));
		TaiKhoan.setTaiKhoanTrangThai(cursor.getInt(cursor
				.getColumnIndex(TaiKhoan.TaiKhoanTrangThai)));
		TaiKhoan.setTaiKhoanGhiChu(cursor.getString(cursor
				.getColumnIndex(TaiKhoan.TaiKhoanGhiChu)));
		TaiKhoan.setTaiKhoanNgayTao(cursor.getString(cursor
				.getColumnIndex(TaiKhoan.TaiKhoanNgayTao)));
		TaiKhoan.setTaiKhoanNgaySua(cursor.getString(cursor
				.getColumnIndex(TaiKhoan.TaiKhoanNgaySua)));
		TaiKhoan.setHinhAnhId(cursor.getInt(cursor
				.getColumnIndex(TaiKhoan.HinhAnhId)));
		return TaiKhoan;
	}


}
