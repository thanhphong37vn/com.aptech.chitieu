package com.aptech.chitieu2.dao;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.oni.webapp.enumeration.SortDir;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.aptech.chitieu.entity.Tien;
import com.aptech.chitieu2.dao.GenericDaoI;
/**
 * Title :DanhMucDao <br/>
 * Description : <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 10, 2015 2:41:26 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 10, 2015 2:41:26 PM
 */
public class TienDao extends DatasoureDao implements
    GenericDaoI<Tien,Integer> {
	/**
	 * @param context
	 */
	public TienDao(Context context) {
		super(context);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#countAll()
	 */
	@Override
	public long countAll() {
		return database.compileStatement(
		    "select * from "+Tien.Tien).simpleQueryForLong();
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#createEntity()
	 */
	@Override
	public Tien createEntity() {
		return new Tien(createPK());
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#createPK()
	 */
	@Override
	public Integer createPK() {
		return (int) (database
		    .compileStatement(
		        "SELECT TienId  FROM Tien order by TienId DESC LIMIT 1;")
		    .simpleQueryForLong()+1);
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#delete(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@Override
	public void delete(Tien entity) {
		deleteByPK(entity.getId());
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public void deleteBy(String fieldName,
	    Collection<?> values) {
		database.delete(Tien.Tien,fieldName+" in ("+values+")",
		    null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public void deleteBy(String fieldName, Object value) {
		database.delete(Tien.Tien,fieldName+" = "+value,null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPK(java.io.Serializable)
	 */
	@Override
	public void deleteByPK(Integer pk) {
		database.delete(Tien.Tien,Tien.TienId+" = "+pk,null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#deleteByPKs(java.util.Collection)
	 */
	@Override
	public void deleteByPKs(Collection<Integer> pks) {
		database.delete(Tien.Tien,Tien.TienId+" in ("+pks+")",
		    null);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#find(int, int, java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<Tien> find(int offset, int size,
	    String sortKey, SortDir sortDir) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll()
	 */
	@Override
	public List<Tien> findAll() {
		List<Tien> lstCtTien=new ArrayList<Tien>();
		Cursor cursor=database.query(Tien.Tien,null,null,null,
		    null,null,Tien.TienTen+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			Tien Tien=cursorToEntity(cursor);
			lstCtTien.add(Tien);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtTien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findAll(java.lang.String,
	 * org.oni.webapp.enumeration.SortDir)
	 */
	@Override
	public List<Tien> findAll(String sortKey, SortDir sortDir) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.util.Map)
	 */
	@Override
	public List<Tien> findBy(Map<String,Object> map) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public List<Tien> findBy(String fieldName,
	    Collection<?> values) {
		List<Tien> lstCtTien=new ArrayList<Tien>();
		// Điều kiện
		String whereClause=fieldName+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(values)};
		Cursor cursor=database.query(Tien.Tien,
		    Tien.TienColumns,whereClause,whereArgs,null,null,
		    Tien.TienTen+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			Tien Tien=cursorToEntity(cursor);
			lstCtTien.add(Tien);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtTien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findBy(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<Tien> findBy(String fieldName, Object value) {
		List<Tien> lstCtTien=new ArrayList<Tien>();
		// Điều kiện
		String whereClause=fieldName+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(value)};
		Cursor cursor=database.query(Tien.Tien,
		    Tien.TienColumns,whereClause,whereArgs,null,null,
		    Tien.TienTen+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			Tien CtTien=cursorToEntity(cursor);
			lstCtTien.add(CtTien);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtTien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPK(java.io.Serializable)
	 */
	@SuppressWarnings("static-access")
	@Override
	public Tien findByPK(Integer pk) {
		Tien Tien=null;
		// Điều kiện
		String whereClause=Tien.TienId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		Cursor cursor=database.query(Tien.Tien,
		    Tien.TienColumns,whereClause,whereArgs,null,null,
		    Tien.TienTen+" DESC");
		cursor.moveToFirst();
		if(!cursor.isAfterLast()) {
			Tien=cursorToEntity(cursor);
		}
		// Đóng cursor lại
		cursor.close();
		return Tien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findByPKs(java.util.Collection)
	 */
	@Override
	public List<Tien> findByPKs(Collection<?> pks) {
		List<Tien> lstCtTien=new ArrayList<Tien>();
		// Điều kiện
		String whereClause=Tien.TienId+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pks)};
		Cursor cursor=database.query(Tien.Tien,
		    Tien.TienColumns,whereClause,whereArgs,null,null,
		    Tien.TienTen+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			Tien Tien=cursorToEntity(cursor);
			lstCtTien.add(Tien);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtTien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#findOne(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public List<Tien> findOne(String fieldName, Object value) {
		List<Tien> lstCtTien=new ArrayList<Tien>();
		// Điều kiện
		String whereClause=fieldName+" in (?)";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(value)};
		Cursor cursor=database.query(Tien.Tien,
		    Tien.TienColumns,whereClause,whereArgs,null,null,
		    Tien.TienTen+" DESC");
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			Tien Tien=cursorToEntity(cursor);
			lstCtTien.add(Tien);
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstCtTien;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#getAllPKs()
	 */
	@Override
	public Collection<Integer> getAllPKs() {
		List<Integer> lstPKs=new ArrayList<Integer>();
		Cursor cursor=database.query(Tien.Tien,
		    Tien.TienColumns,null,null,null,null,null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			lstPKs.add(cursor.getInt(cursor
			    .getColumnIndex(Tien.TienId)));
			cursor.moveToNext();
		}
		// Đóng cursor lại
		cursor.close();
		return lstPKs;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insert(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@SuppressWarnings("static-access")
	@Override
	public Integer insert(Tien Tien) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(Tien.TienId,Tien.getTienId());
		values.put(Tien.TienTen,Tien.getTienTen());
		values.put(Tien.TienTrangThai,Tien.getTienTrangThai());
		values.put(Tien.TienGhiChu,Tien.getTienGhiChu());
		values.put(Tien.TienNgayTao,Tien.getTienNgayTao());
		values.put(Tien.TienNgaySua,Tien.getTienNgaySua());
		long insertId=database.insert(Tien.Tien,null,values);
		// Trỏ tới database lấy dữ liệu vừa lưu - để kiểm tra xem dữ liệu ghi
		// đúng chưa
		// Truy vấn lấy bản ghi có ID = insertId
		Cursor cursor=database.query(Tien.Tien,null,Tien.TienId
		    +" = "+insertId,null,null,null,null);
		cursor.moveToFirst();
		Tien dm=cursorToEntity(cursor);
		cursor.close();
		// Trả về đối tượng Comment
		return dm.getId();
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#insertOrUpdate(org.oni.webapp.dao.entity
	 * .base.EntityBase)
	 */
	@Override
	public void insertOrUpdate(Tien entity) {
		if(isExisted(entity.getTienId()))
			update(entity);
		else insert(entity);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.io.Serializable)
	 */
	@Override
	public boolean isExisted(Integer pk) {
		if(findByPK(pk)!=null) return true;
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.util.Collection)
	 */
	@Override
	public boolean isExisted(String fieldName,
	    Collection<?> values) {
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#isExisted(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public boolean isExisted(String fieldName, Object value) {
		if(findBy(fieldName,value)!=null) return true;
		return false;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#update(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */@SuppressWarnings("static-access")
	@Override
	public void update(Tien Tien) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(Tien.TienId,Tien.getTienId());
		values.put(Tien.TienTen,Tien.getTienTen());
		values.put(Tien.TienTrangThai,Tien.getTienTrangThai());
		values.put(Tien.TienGhiChu,Tien.getTienGhiChu());
		values.put(Tien.TienNgayTao,Tien.getTienNgayTao());
		values.put(Tien.TienNgaySua,Tien.getTienNgaySua());
		// Điều kiện
		String whereClause=Tien.TienId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(Tien
		    .getTienId())};
		// Thực hiện câu lệnh
		database.update(Tien.Tien,values,whereClause,whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#updateField(java.io.Serializable,
	 * java.lang.String, java.lang.Object)
	 */
	@Override
	public void updateField(Integer pk, String fieldName,
	    Object value) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		// values.put(Tien.TienId,Tien.getTienId());
		values.put(fieldName,String.valueOf(value));
		// Điều kiện
		String whereClause=Tien.TienId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		// Thực hiện câu lệnh
		database.update(Tien.Tien,values,whereClause,whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#updateFields(java.io.Serializable,
	 * java.util.Map)
	 */
	@Override
	public void updateFields(Integer pk,
	    Map<String,Object> valueMap) {
		// Ghi dữ liệu xuống database
		ContentValues values=new ContentValues();
		Set<String> keySet=valueMap.keySet();
		for(String key:keySet) {
			values.put(key,String.valueOf(valueMap.get(key)));
		}
		// Điều kiện
		String whereClause=Tien.TienId+" = ?";
		// Tham số cho điều kiện
		String[] whereArgs=new String[]{String.valueOf(pk)};
		// Thực hiện câu lệnh
		database.update(Tien.Tien,values,whereClause,whereArgs);
	}
	/*
	 * (non-Javadoc)
	 * @see com.aptech.chitieu.dao2.GenericDao#getEntityClass()
	 */
	@Override
	public Tien getEntityClass() {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * com.aptech.chitieu.dao2.GenericDao#cursorToEntity(android.database.Cursor)
	 */@SuppressWarnings("static-access")
	@Override
	public Tien cursorToEntity(Cursor cursor) {
		Tien Tien=new Tien();
		Tien.setTienId(cursor.getInt(cursor
		    .getColumnIndex(Tien.TienId)));
		Tien.setTienQuocGia(cursor.getString(cursor
		    .getColumnIndex(Tien.TienQuocGia)));
		Tien.setTienTen(cursor.getString(cursor
		    .getColumnIndex(Tien.TienTen)));
		Tien.setTienMaTien(cursor.getString(cursor
		    .getColumnIndex(Tien.TienMaTien)));
		Tien.setTienTrangThai(cursor.getInt(cursor
		    .getColumnIndex(Tien.TienTrangThai)));
		Tien.setTienGhiChu(cursor.getString(cursor
		    .getColumnIndex(Tien.TienGhiChu)));
		Tien.setTienNgayTao(cursor.getString(cursor
		    .getColumnIndex(Tien.TienNgayTao)));
		Tien.setTienNgaySua(cursor.getString(cursor
		    .getColumnIndex(Tien.TienNgaySua)));
		Tien.setHinhAnhId(cursor.getInt(cursor
		    .getColumnIndex(Tien.HinhAnhId)));
		return Tien;
	}
}
