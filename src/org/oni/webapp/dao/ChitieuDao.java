package org.oni.webapp.dao;
import java.util.List;
import org.oni.webapp.dao.base.GenericDao;
import org.oni.webapp.utils.PaginationBean;
import com.aptech.chitieu.entity.ChiTieu;
/**
 * This is the interface of the Account DAO.
 * 
 * @author DungTV
 */
public interface ChitieuDao extends
    GenericDao<ChiTieu,Integer> {
	public List<ChiTieu> searchByKey(String key);
	public List<ChiTieu> getListAccount(
	    PaginationBean paginationBean, Integer cate);
}