package org.oni.webapp.dao;
import org.oni.webapp.dao.base.GenericDao;
import com.aptech.chitieu.entity.DanhMuc;
/**
 * This is the interface of the DanhMuc DAO.
 * 
 * @author DungTV
 */
public interface DanhMucDao extends
    GenericDao<DanhMuc,Integer> {
}