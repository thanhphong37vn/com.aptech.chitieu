package org.oni.webapp.dao.base.impl;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.oni.webapp.dao.base.GenericDao;
import org.oni.webapp.dao.entity.base.EntityBase;
import org.oni.webapp.enumeration.SortDir;
import android.util.Log;
import com.aptech.chitieu.dao.ConnectionUtil;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
/**
 * Title : DAO Layer <br/>
 * Description : This is the implementation class of the base DAO. <br/>
 * Copyright : Copyright (c) 2015<br/>
 * Company : Aptech Aprotrain <br/>
 * Create on Aug 7, 2015 12:15:55 PM
 * 
 * @author <a href="mailto:hoanpmp@gmail.com">HoanPham</a>
 * @version Aug 7, 2015 12:15:55 PM</br>
 * @param <E>
 *          Class of Entity
 * @param <PK>
 *          Class of Primary Key
 */
public abstract class GenericDaoImpl02<E extends EntityBase<PK>,PK extends Serializable>
    implements GenericDao<E,PK> {
	/** Class of Entity */
	// private final E entityClass;
	// /** Class of Primary Key */
	// private final PK pkClass;
	/** Class of Entity */
	private final Class<? extends E>	entityClass;
	/** Class of Primary Key */
	private final Class<? extends PK>	pkClass;
	ConnectionSource	                connectionSource	=null;
	Dao<E,PK>	                        dao	             =null;
	Connection	                      conSQL	         =null;
	/**
	 * Constructor
	 * 
	 * @param entityClass
	 *          Class of Entity
	 * @param pkClass
	 *          Class of Primary Key
	 */
	public GenericDaoImpl02(Class<? extends E> entityClass,
	    Class<? extends PK> pkClass) {
		this.entityClass=entityClass;
		this.pkClass=pkClass;
		// verify entity class
		// this uses SQLlite by default but change to match your database
		// create a connection source to our database
		try {
			connectionSource=ConnectionUtil
			    .getConnectionSQLLite();
			Log.e("Is close connection?",connectionSource
			    .getReadWriteConnection().isClosed()+"");
			dao=DaoManager.createDao(connectionSource,
			    EntityBase.class);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	// /**
	// * @param class1
	// * @param class2
	// */
	// public GenericDaoImpl(EntityBase<PK> entityClass,
	// Class<PK> pkClass) {
	// // this.entityClass=entityClass;
	// // this.pkClass=pkClass;
	// // verify entity class
	// // this uses SQLlite by default but change to match your database
	// // create a connection source to our database
	// try {
	// connectionSource=ConnectionUtil
	// .getConnectionSQLLite();
	// dao=DaoManager.createDao(connectionSource,
	// EntityBase.class);
	// } catch(SQLException e) {
	// e.printStackTrace();
	// }
	// }
	/*
	 * (non-Javadoc)
	 * @see
	 * org.oni.webapp.dao.base.GenericDao#insert(org.oni.webapp.dao.entity.base
	 * .EntityBase)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public PK insert(E entity) {
		try {
			getDaoOject().create(entity);
			return (PK) Integer.valueOf(getDaoOject().create(
			    entity));
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public void update(E entity) {
		try {
			getDaoOject().update(entity);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void insertOrUpdate(E entity) {
		try {
			getDaoOject().update(entity);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void updateField(PK pk, String fieldName,
	    Object value) {
		Map<String,Object> valueMap=new HashMap<String,Object>(
		    1);
		valueMap.put(fieldName,value);
		updateFields(pk,valueMap);
	}
	@Override
	public void updateFields(PK pk,
	    Map<String,Object> valueMap) {
		PreparedStatement prep=null;
		try {
			StringBuilder sb=new StringBuilder();
			sb.append("UPDATE ").append(getEntityClassName())
			    .append(" SET ");
			for(Map.Entry<String,?> entry:valueMap.entrySet()) {
				sb.append(entry.getKey()).append(
				    " = :"+entry.getKey()+", ");
			}
			sb.deleteCharAt(sb.length()-2);
			sb.append("WHERE id = "+pk);
			prep=conSQL.prepareStatement(sb.toString());
			for(Map.Entry<String,?> entry:valueMap.entrySet()) {
				entry.getKey();
				Object value=entry.getValue();
				if(value instanceof Integer) {
					Integer new_name=(Integer) value;
					prep.setInt(Integer.parseInt(entry.getKey()),
					    new_name);
				} else if(value instanceof String) {
					String new_name=(String) value;
					prep.setString(Integer.parseInt(entry.getKey()),
					    new_name);
				}
			}
			prep.executeUpdate();
		} catch(NumberFormatException e) {
			e.printStackTrace();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(prep!=null) try {
				prep.close();
			} catch(SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	@Override
	public void delete(E entity) {
		try {
			getDaoOject().delete(entity);
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void deleteByPK(PK pk) {
		StringBuilder sb=new StringBuilder();
		sb.append("DELETE FROM "+getEntityClassName()
		    +" WHERE id = "+pk);
		PreparedStatement prep=null;
		try {
			prep.execute(sb.toString());
		} catch(NumberFormatException e) {
			e.printStackTrace();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(prep!=null) try {
				prep.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
	@Override
	public void deleteByPKs(Collection<PK> pks) {
		deleteBy("id",pks);
	}
	@Override
	public void deleteBy(String fieldName, Object value) {
		StringBuilder sb=new StringBuilder();
		sb.append("DELETE FROM "+getEntityClassName()+" WHERE "
		    +fieldName+" = "+value);
		PreparedStatement prep=null;
		try {
			prep.execute(sb.toString());
		} catch(NumberFormatException e) {
			e.printStackTrace();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(prep!=null) try {
				prep.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
	@Override
	public void deleteBy(String fieldName,
	    Collection<?> values) {
		if(values.isEmpty()) return;
		StringBuilder sb=new StringBuilder();
		sb.append("DELETE FROM "+getEntityClassName()+" WHERE "
		    +fieldName+" = in ("+values+")");
		PreparedStatement prep=null;
		try {
			prep.execute(sb.toString());
		} catch(NumberFormatException e) {
			e.printStackTrace();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(prep!=null) try {
				prep.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
	@Override
	public E findByPK(PK pk) {
		try {
			E queryForId=getDaoOject().queryForId(pk);
			return queryForId;
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<E> findAll() {
		try {
			return getDaoOject().queryForAll();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<E> findAll(String sortKey, SortDir sortDir) {
		try {
			return getDaoOject().queryForAll();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<E> find(int offset, int size, String sortKey,
	    SortDir sortDir) {
		try {
			return getDaoOject().queryForAll();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public long countAll() {
		try {
			return (long) getDaoOject().queryForAll().size();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return (Long) null;
	}
	@Override
	public List<E> findOne(String fieldName, Object value) {
		try {
			return getDaoOject().queryForAll();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<E> findBy(String fieldName, Object value) {
		//
		// List<E> list=createCriteria().add(
		// Restrictions.eq(fieldName,value)).list();
		return findAll();
	}
	@Override
	public List<E> findBy(Map<String,Object> map) {
		// find all entities if the map is empty
		// if(map==null||map.isEmpty()) { return findAll(); }
		// Criteria criteria=createCriteria();
		// for(Map.Entry<String,Object> entry:map.entrySet()) {
		// String fieldName=entry.getKey();
		// Object value=entry.getValue();
		// criteria.add(Restrictions.eq(fieldName,value));
		// }
		//
		// List<E> list=criteria.list();
		// return list;
		return findAll();
	}
	@Override
	public List<E> findByPKs(Collection<?> values) {
		return findAll();
	}
	@Override
	public List<E> findBy(String fieldName,
	    Collection<?> values) {
		List<E> list=null;
		// if(CollectionUtils.isNotEmpty(values)) {
		// list=createCriteria().add(
		// Restrictions.in(fieldName,values)).list();
		// } else {
		// list=new ArrayList<E>(0);
		// }
		return list;
	}
	@Override
	public Collection<PK> getAllPKs() {
		List<PK> pks=null;
		// createCriteria().setProjection(Projections.id()).list();
		return new HashSet<PK>(pks);
	}
	@Override
	public boolean isExisted(PK pk) {
		E object=findByPK(pk);
		return object!=null;
	}
	@Override
	public boolean isExisted(String fieldName, Object value) {
		// E object=findOne(fieldName,value);
		return true;
	}
	@Override
	public boolean isExisted(String fieldName,
	    Collection<?> values) {
		Long count=0l;
		if(!values.isEmpty()) {
			// count=(Long) createCriteria()
			// .add(Restrictions.in(fieldName,values))
			// .setMaxResults(1)
			// .setProjection(Projections.rowCount())
			// .uniqueResult();
			// return count.longValue()>0;
			return true;
		} else {
			return false;
		}
	}
	/*
	 * Set the list parameters to the Criteria.
	 */
	// private void setParamsToCriteria(Criteria criteria,
	// int offset, int size, String sortKey, SortDir sortDir) {
	// // range specification
	// criteria.setFirstResult(offset);
	// criteria.setMaxResults(size);
	// // order specified
	// setSortParamsToCriteria(criteria,sortKey,sortDir);
	// }
	/*
	 * Set the list sort parameters to the Criteria.
	 */
	// private void setSortParamsToCriteria(Criteria criteria,
	// String sortKey, SortDir sortDir) {
	// // order specified
	// if(StringUtils.isNotEmpty(sortKey)) {
	// Order order;
	// if(sortDir==SortDir.DESC) {
	// order=Order.desc(sortKey);
	// } else {
	// order=Order.asc(sortKey);
	// }
	// criteria.addOrder(order);
	// }
	// }
	public Class<? extends E> getEntityClass() {
		return this.entityClass;
	}
	/**
	 * Get the class name of the entity.
	 * 
	 * @return エンティティのクラス名
	 */
	protected String getEntityClassName() {
		return getEntityClass().getClass().getName();
	}
	/**
	 * create Criteria
	 * 
	 * @return Criteria
	 */
	// protected Criteria createCriteria() {
	// // return getCurrentSession().createCriteria(
	// // this.entityClass);
	// }
	/**
	 * Get the dao object.
	 * 
	 * @return Dao object
	 */
	protected Dao<E,PK> getDaoOject() {
		try {
			if(dao!=null)
			  dao=DaoManager.createDao(connectionSource,
			      EntityBase.class);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return dao;
	}
	public E createEntity() {
		return newInstanceWithConvertRuntimeException(entityClass);
	}
	public PK createPK() {
		return (PK) newInstanceWithConvertRuntimeException(this.pkClass);
	}
	private static <T>T newInstanceWithConvertRuntimeException(
	    Class<T> cls) {
		try {
			return cls.newInstance();
		} catch(InstantiationException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch(IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
