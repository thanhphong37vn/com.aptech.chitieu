package org.oni.webapp.dao.entity.base;

import java.io.Serializable;

/**
 * abstract implement of class Entitybase
 * 
 * @author DungTV11
 */
public abstract class EntityBaseImpl<PK extends Serializable> implements
		EntityBase<PK>, Serializable {
	private static final long serialVersionUID = 1L;
}
