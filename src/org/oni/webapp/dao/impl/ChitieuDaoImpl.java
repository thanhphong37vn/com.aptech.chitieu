package org.oni.webapp.dao.impl;
import java.util.List;
import org.oni.webapp.dao.ChitieuDao;
import org.oni.webapp.dao.base.impl.GenericDaoImpl;
import org.oni.webapp.utils.PaginationBean;
import com.aptech.chitieu.entity.ChiTieu;
/**
 * This is the implementation class of the Account DAO.
 * 
 * @author DungTV
 */
public class ChitieuDaoImpl extends
    GenericDaoImpl<ChiTieu,Integer> implements ChitieuDao {
	public ChitieuDaoImpl() {
		super(ChiTieu.class,Integer.class);
	}
	/**
	 * @param entityClass
	 * @param pkClass
	 */
	// public ChitieuDaoImpl(ChiTieu entityClass, Integer pkClass) {
	// super(entityClass,pkClass);
	// }
	public List<ChiTieu> searchByKey(Integer key) {
		findByPK(key);
		return null;
	}
	public List<ChiTieu> getListAccount(
	    PaginationBean paginationBean, Integer cate) {
		return null;
	}
	/*
	 * (non-Javadoc)
	 * @see org.oni.webapp.dao.ChitieuDao#searchByKey(java.lang.String)
	 */
	@Override
	public List<ChiTieu> searchByKey(String key) {
		// TODO Auto-generated method stub
		return null;
	}
}