package org.oni.webapp.dao.impl;
import org.oni.webapp.dao.DanhMucDao;
import org.oni.webapp.dao.base.impl.GenericDaoImpl;
import com.aptech.chitieu.entity.DanhMuc;
/**
 * This is the implementation class of the Account DAO.
 * 
 * @author DungTV
 */
public class DanhMucDaoImpl extends
    GenericDaoImpl<DanhMuc,Integer> implements DanhMucDao {
	public DanhMucDaoImpl() {
		super(DanhMuc.class,Integer.class);
	}
}